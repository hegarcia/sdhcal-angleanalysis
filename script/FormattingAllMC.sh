#!/bin/bash

if [ -z "$ILCSOFT" ]; then
   echo " You should source an init_ilcsoft.sh script"
   exit
fi

particleTypes=( "Electrons" ) #"Cosmics" "Muons" "Electrons" "Pions" ) 
particleName=( "e-" ) #"cosmic" "mu-" "e-" "pi+" )
particleTypeID=( 3 )
energies=( 5 10 50 ) #5 10 20 30 40 50 60 70 80 90 )
angles=( 30 )
nEntries=10000

simulationFolder="/pool/calice/hectorgc/SDHCALSim/Production"
resultsFolder="/pool/calice/hectorgc/sdhcal-angleanalysis/Results/MC"

for iParticle in $(seq 0 $(( ${#particleTypes[@]} - 1 ))); do
    for energy in "${energies[@]}"; do
	for angle in "${angles[@]}"; do

	    inputName="${simulationFolder}/${particleTypes[${iParticle}]}/${particleName[${iParticle}]}_${nEntries}_${energy}GeV_${angle}Deg_QGSP_Digi.slcio"
	    
	    if ! [ -f "${inputName}" ]; then
		echo "#### ${inputName} file not found. ####"
		continue
	    fi
	    
	    if [ ${angle} == 0 ]; then
		particleTypeNumber=$(( $(( ${energy} * 10 )) + ${particleTypeID[${iParticle}]} ))
	    else
		particleTypeNumber=$(( $(( ${angle} * 100 )) + $(( ${energy} * 10 )) + ${particleTypeID[${iParticle}]} ))
	    fi
	   
	    Marlin steer/MCFormatting.xml --global.LCIOInputFiles=${inputName} --MyMCFormatting.ParticleType=${particleTypeNumber}

	    mv LogROOT* ${resultsFolder}/${particleTypes[${iParticle}]}
	    mv MCProto* ${resultsFolder}/${particleTypes[${iParticle}]}

	done
   done
done
