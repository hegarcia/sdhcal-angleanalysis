#!/bin/bash

if [ -z "$ILCSOFT" ]; then
   echo " You should source an init_ilcsoft.sh script"
   exit
fi

resultsFolder="/pool/calice/hectorgc/sdhcal-angleanalysis/Results"

# $1 is run type: TB or MC
# $2 is angle: 0Deg, 20Deg or 22.46Deg
# $3 is the final cut. This parameter is optional. Usualy 'FinalCleanupSelection'

folder="${resultsFolder}/$1/$2"

runs22Deg=( 728456 728448 728449 728450 728451 728452 728453 728454 728455 )
runs20Deg=( 728427 728428 728429 728430 728431 728433 728434 )
runs0Deg=( 728401 728403 728405 728406 728408 728410 728412 728415 )

if [ $2 == "0Deg" ]; then
    runs=("${runs0Deg[@]}")
elif [ $2 == "20Deg" ]; then
    runs=("${runs20Deg[@]}")
elif [ $2 == "22.46Deg" ]; then
    runs=("${runs22Deg[@]}")
fi

for iRun in "${runs[@]}"; do
    root -l ./Macros/ComputeSystematics.cpp\(\"${folder}/Systematics_BeamParticleSelector_${iRun}.txt\ ${folder}/Systematics_MuonSelector_Showers_${iRun}.txt\ ${folder}/Systematics_ElectronSelector_Pions_${iRun}.txt\"\,\"$3\"\)
    mv SystematicErrors.root SystematicErrors_${iRun}.root
    mv SystematicErrors.txt SystematicErrors_${iRun}.txt
done

#${folder}/Systematics_CosmicsAngularSelector_${iRun}.txt\
