#!/bin/bash

nTypes=3   #NTypes - 1
particleTypes=( "Muons" "Cosmics" "Pions" "Electrons" )
energies=( 10 50 10 10)

densityValues=( 1.2 1.4 1.6 1.8 2.0 2.2 2.4 2.6 2.8 3.0 3.2 3.4 3.6 3.8 4.0 4.2 4.4 4.6 4.8 5.0 )

if [ -f "DensityScanResults.txt" ]; then
    rm DensityScanResults.txt
fi

for iType in $(seq 0 ${nTypes}); do
    for density in "${densityValues[@]}"; do

	#Marlin steer/MuonsSelection.xml \
	#    --constant.DensityCuts="${density}" \
	#    --constant.SecondMaxCuts=10000 \
	#    --global.LCIOInputFiles="../SDHCALSim/Production/${particleTypes[${iType}]}/0Deg/${particleTypes[${iType}]}_${energies[${iType}]}GeV_QGSP_Digi.slcio"
        

	#mv MuonsSelection_-1.root ./tmp/MuonsSelection_${particleTypes[${iType}]}_${energies[${iType}]}GeV_${density}Cut.root
        
	root -l ./Macros/DensityScan.cpp\(\"${particleTypes[${iType}]}\"\,\"${energies[${iType}]}\"\,\"${density}\"\)

    done
done

root -l ./Macros/DensityScanResults.cpp


