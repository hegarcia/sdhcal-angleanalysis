#!/bin/bash

nTypes=3   #NTypes - 1
particleTypes=( "Muons" "Cosmics" "Pions" "Electrons" ) #"Muons"
energies=( 10 50 10 10 ) 

densityValues=( 2.6 2.7 2.8 2.9 3.0 3.2 3.4 3.6 3.8 4.0 4.2 4.4 4.6 4.8 5.0 5.4 5.8 6.2 6.6 7.0 7.4 7.6 8.2 )
secondMaxValues=( 5 6 7 8 9 10 11 12 13 14 15 )

runMarlin=false

if [ -f "SecondMaxScanResults.txt" ]; then
    rm SecondMaxScanResults.txt
fi

for iType in $(seq 0 ${nTypes}); do
    for density in "${densityValues[@]}"; do
	for secondMax in "${secondMaxValues[@]}"; do

	    if [ ${runMarlin} = true ]; then

		Marlin steer/MuonsSelection.xml \
	    	    --constant.DensityCuts="${density}" \
		    --constant.SecondMaxCuts="${secondMax}" \
	    	    --global.LCIOInputFiles="../SDHCALSim/Production/${particleTypes[${iType}]}/0Deg/${particleTypes[${iType}]}_${energies[${iType}]}GeV_QGSP_Digi.slcio"
	        
		mv MuonsSelection_-1.root ./tmp/MuonsSelection_${particleTypes[${iType}]}_${energies[${iType}]}GeV_${density}Cut_${secondMax}SMCut.root
        
	    fi
	    
	root -l ./Macros/SecondMaxScan.cpp\(\"${particleTypes[${iType}]}\"\,\"${energies[${iType}]}\"\,\"${density}\"\,\"${secondMax}\"\)
	
	done
    done
done

root -l ./Macros/SecondMaxScanResults.cpp


