#ifndef _TriventProc_h_
#define _TriventProc_h_

#include "Mapping.h"

#include <marlin/Processor.h>

#include <IO/LCWriter.h>
#include <EVENT/RawCalorimeterHit.h>
#include <EVENT/LCEvent.h>
#include <EVENT/LCRunHeader.h>
#include <IMPL/LCCollectionVec.h>
#include <IMPL/CalorimeterHitImpl.h>
#include <UTIL/CellIDEncoder.h>

#include <TFile.h> //Any ROOT header includes the ROOT types definitions
#include <TH1F.h>
#include <TH2F.h>

#include <utility> //It has the definition of std::pair
#include <vector>
#include <map>
#include <string>
  
  
class TriventProc  : public marlin::Processor
{
public:
  
  Processor*  newProcessor() { return new TriventProc ; }
  
  TriventProc();
  
  ~TriventProc() {};
  
  void init();

  void processGeometry(std::string jsonFile);
  
  void processEvent( LCEvent * evtP );
  void processRunHeader( LCRunHeader * runH);
  
  void end();
  
protected:

  void writeHistograms();

  void processHit(RawCalorimeterHit* a_DHcalhit, CellIDEncoder<CalorimeterHitImpl>* cd);
  void displayBadMap();
  
  //---- Input variables ----

  std::vector<std::string> _hcalCollections;
  std::string _outFileName;
  std::string _mappingfile;
  std::string _logrootName;

  int _elec_noise_cut,_noiseCut, _timeWin;
  double _layer_gap;

  // -----------------------
  
  TFile *_outputFile;

  // Readout related histograms
  
  TH1F *maxTimeHist; //Histogram of the maxTime used in the histograms
  TH1F *nHitsReadout;
  TH1F *readoutTimeGap;
  TH1F *DIFTimePedestal;
    
  TH1F *firstTimeA;
  TH1F *firstTimeB;
  TH2F *firstA_nHit;
  TH2F *firstA_spillTime;
  TH2F *firstA_spillNr;
  TH2F *firstA_Gap;
  TH2F *firstB_nHit;
  TH2F *firstB_spillTime;
  TH2F *firstB_spillNr;
  TH2F *firstB_Gap;
  TH2F *firstB_prevA;
  TH2F *firstB_nHitsPrevReadout;

  TH2F *readoutTG_nHitsPrevReadout;

  TH1F *noisyReadout = nullptr;
  TH1F *fullReadout = nullptr; //Histograms of number of entries per time beam from a readout
  TH1F *physicalReadout = nullptr; //Histogram of number of entries per time beam only for bins selected as physical events

  TH1F *noisePerTimeBin;
  TH2F *noisePTB_runTime;

  // Event related histograms

  TH1F *nHitHist;
  TH1F* IHist;
  TH1F* JHist;
  TH1F* KHist;
  
  TH2F *nHit_spillTime;
  LCWriter* _lcWriter;

  int evtnum, spillNr, maxtime, runNumber = -1;
  int nInitialHits, nOverlappedHits, nNoisyReadouts;
  
  // --- Mapping ---
  std::map<int, LayerID  > _mapping;
  std::map<std::pair<int, int>, int> _badMapping;

  // --- Time variables ---
  unsigned long long int prevBCID; 
  unsigned long long int firstBCID;
  unsigned long long int refBCID;
  unsigned long long int evtTimeInSpill;
  float timeInSpillInSec;

  // --- Event variables ---

  std::map<int,std::map<int, std::map<int,CalorimeterHitImpl*>>> nHitsMap;
  
  int firstLayer;
  Int_t _Nhit;

  int nHit_Prev;
  int prevA;

};

#endif


