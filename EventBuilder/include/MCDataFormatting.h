#ifndef __MCDATAFORMATTING__
#define __MCDATAFORMATTING__

#include <marlin/Processor.h>

#include <IO/LCWriter.h>
#include <EVENT/LCEvent.h>
#include <EVENT/LCRunHeader.h>

#include "TFile.h"
#include "TH1F.h"

#include <vector>
#include <string>
  
class MCDataFormatting  : public marlin::Processor
{
public:
  
  Processor*  newProcessor() { return new MCDataFormatting ; }
  
  MCDataFormatting();
  ~MCDataFormatting() {};
  
  void init();
  
  void processEvent( LCEvent * evtP );
  void processRunHeader( LCRunHeader * runH) {};
  
  void end();
  
protected:

  //---- Input variables ----

  std::vector<std::string> _inputCollections;
  std::string _outFileName;
  int _particleType; // 0 Unknown; 1 Cosmics; 2 Muons; 3 Electrons; 4 Pions
  
  // -----------------------
  
  LCWriter* _lcWriter;

  TFile* rootFile;

  TH1F* IHist;
  TH1F* JHist;
  TH1F* KHist;
  TH1F* THist;

  TH1F* ASICs;
  TH1F* DIFs;

  TH1F* nHitsASICHist;
  TH1F* nHitsDIFHist;
  TH1F* nHitsHist;
  
  int evtnum = 0;
  
};

#endif


