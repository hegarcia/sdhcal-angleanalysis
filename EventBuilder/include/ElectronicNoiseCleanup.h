#ifndef __ELNOISECLEANUP__
#define __ELNOISECLEANUP__

#include "Systematics.h"

#include <marlin/Processor.h>

#include <IO/LCWriter.h>

#include <EVENT/LCRunHeader.h>
#include <EVENT/LCEvent.h>
#include <EVENT/CalorimeterHit.h>

#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>

#include <map>
#include <vector>
#include <string>

  
class ElNoiseCleanup  : public marlin::Processor
{
public:
  
  Processor*  newProcessor() { return new ElNoiseCleanup ; }
  
  ElNoiseCleanup();
  ~ElNoiseCleanup() {};
  
  void init();
  
  void processEvent( LCEvent * evtP );
  void processRunHeader( LCRunHeader * runH);
  
  void end();
  
protected:

  bool scanIsolated(int testLayer, int testDIF, int testASIC, int searchWindow);
  bool searchForASIC(int layer, int dif, int asic);
  bool searchForDIF(int layer, int dif);
  void writeHistograms();
  
  // ---- Input variables ----

  std::vector<std::string> _hcalCollections;
  std::string _outFileName;
  std::string _outColName;
  std::string _logRootName;
  int _noisyCut, _preCut, _postCut;
  int _noisyDIFCut;
  
  // ---- Output Variables ----
  
  LCWriter* _lcWriter;  

  TFile *_outputFile;

  std::map<std::string, TH1F*> histograms1D;
  std::map<std::string, TH2F*> histograms2D;

  // ---- Counters ----
  
  int evtnum, nASICsTotal, nDIFsTotal, runNumber = -1; 
  int nNoisyASICs, nIsolatedASICs, nNoisyDIFs, nNoisyLayers;

  // --- Event variables ---

  std::map<int,std::map<int,std::map<int,std::vector<CalorimeterHit*>>>> nHitsMap;

  // ---- Tools ----

  SystematicsTool* sysTool; 
};

#endif


