#include "MCDataFormatting.h"

#include <UTIL/CellIDDecoder.h>
#include <UTIL/CellIDEncoder.h>
#include <EVENT/LCCollection.h>
#include <EVENT/CalorimeterHit.h>

#include <marlin/Global.h>

#include <IMPL/LCEventImpl.h>
#include <IMPL/LCRunHeaderImpl.h>
#include <IMPL/LCCollectionVec.h>
#include <IMPL/CalorimeterHitImpl.h>

MCDataFormatting a_MCDataFormatting_instance;

//=========================================================
MCDataFormatting::MCDataFormatting()
  :Processor("MCDataFormatting")
{
  
  //Input Collections    
  _inputCollections.push_back(std::string("HCALEndcap"));
  registerInputCollections(LCIO::CALORIMETERHIT, 
			   "InputCollection",  
			   "Input Calorimeter hit to format",  
			   _inputCollections, 
			   _inputCollections); 
  
  _particleType = 0;
  registerProcessorParameter("ParticleType", 
			     "Type of simulated particle",
			     _particleType,
			     _particleType);

} 

void MCDataFormatting::init() {
  
  streamlog_out(DEBUG2) << "MC formatting begin" << std::endl;
  
  _lcWriter = LCFactory::getInstance()->createLCWriter() ;
  _lcWriter->setCompressionLevel( 0 );

  switch(_particleType%10){
  case 1:
_outFileName = "MCProto_Cosmics_" + std::to_string((_particleType%1000)/10) + "GeV_" + std::to_string((_particleType/1000)*10) + "Deg" ; break;
  case 2:
_outFileName = "MCProto_Muons_" + std::to_string((_particleType%1000)/10) + "GeV_" + std::to_string((_particleType/1000)*10) + "Deg"; break;
  case 3:
_outFileName = "MCProto_Electrons_" + std::to_string((_particleType%1000)/10) + "GeV_" + std::to_string((_particleType/1000)*10) + "Deg"; break;
  case 4:
_outFileName = "MCProto_Pions_" + std::to_string((_particleType%1000)/10) + "GeV_" + std::to_string((_particleType/1000)*10) + "Deg"; break;
  default:
_outFileName = "MCProto_Unknown_" + std::to_string((_particleType%1000)/10) + "GeV_" + std::to_string((_particleType/1000)*10) + "Deg";
  };
  
  _lcWriter->open((_outFileName +  ".slcio").c_str(),LCIO::WRITE_NEW);

  LCRunHeaderImpl* runHd = new LCRunHeaderImpl();
  runHd->setRunNumber(_particleType);
  runHd->setDetectorName("MCProto");
  
  _lcWriter->writeRunHeader(runHd);

  rootFile = new TFile(("LogROOT_" + _outFileName + ".root").c_str(),"RECREATE");

  IHist = new TH1F("IHist", ";Pad;Entries", 101, 0., 101.);
  JHist = new TH1F("JHist", ";Pad;Entries", 101, 0., 101.);
  KHist = new TH1F("KHist", ";Pad;Entries", 101, 0., 101.);
  THist = new TH1F("THist", ";Threshold;Entries", 5, 0., 5.);

  ASICs = new TH1F("ASICs", ";ASIC;Entries", 151, 0., 151.);
  DIFs = new TH1F("DIFs", ";DIF;Entries", 151, 0., 151.);

  nHitsASICHist = new TH1F("NHitASIC", ";nHit;Entries", 71, 0., 71.);
  nHitsDIFHist = new TH1F("NHitDIF", ";nHit;Entries", 3101, 0., 3101.);
  nHitsHist = new TH1F("NHits", ";nHit;Entries", 250, 0., 2000.);
  
  printParameters();
  
}

 
void MCDataFormatting::processEvent( LCEvent * evtP ) 
{ 
  streamlog_out(DEBUG2) << "Start MCDataFormatting process" << std::endl;
  
  if(evtP){ //Check evtP
    for(unsigned int i=0; i < _inputCollections.size(); i++) { //loop over collection
      try{
	
	LCCollection* col = evtP->getCollection(_inputCollections[i].c_str());
	int nHits = col->getNumberOfElements();
	nHitsHist->Fill(nHits);

	CellIDDecoder<CalorimeterHit> cd(col);

	LCCollectionVec* outCol = new LCCollectionVec(LCIO::CALORIMETERHIT);
	outCol->setFlag(col->getFlag());
	  
	CellIDEncoder<CalorimeterHitImpl> encoder("I:7,J:7,K:10,Dif_id:8,Asic_id:6,Chan_id:7",outCol);

	std::map<int, std::map<int,std::map<int,CalorimeterHitImpl*>>> hitMap = {};
	std::map<int, std::map<int,int>> hitCounter = {};
	
	for(int iHit = 0; iHit < nHits; iHit++) {

	  CalorimeterHit* hit = dynamic_cast<CalorimeterHit*>(col->getElementAt(iHit));

	  int I = (int)cd(hit)["I"];
	  int J = (int)cd(hit)["J"];
	  int K = (int)cd(hit)["K-1"];
	  
	  if(I < 0 || I > 95) {
	    streamlog_out(DEBUG) << " I = " << I << " in Event: " << evtnum + 1 << std::endl;
	    continue;
	  }
	  if(J < 0 || J > 95) {
	    streamlog_out(DEBUG) << " J = " << J << " in Event: " << evtnum + 1 << std::endl;
	    continue;
	  }
	  if(K < 0 || K > 48) {
	    streamlog_out(DEBUG) << " K = " << K << " in Event: " << evtnum + 1 << std::endl;
	    continue;
	  }
	  
	  CalorimeterHitImpl* copyHit = new CalorimeterHitImpl();
	  copyHit->setTime(hit->getTime());
	  copyHit->setEnergy(hit->getEnergy());
	  copyHit->setPosition(hit->getPosition());

	  encoder["I"] = I;
	  encoder["J"] = J;
	  encoder["K"] = K;
	  
	    
	  encoder["Dif_id"] = 0;
	  encoder["Asic_id"] = 0;
	  encoder["Chan_id"] = 0;

	  encoder.setCellID(copyHit);

	  int geomASIC = I/8 + J/8*12;
	  int geomDIF = I/32 + K*3;

	  ASICs->Fill(geomASIC);
	  DIFs->Fill(geomDIF);
	  
	  if(hitMap.find(I) != hitMap.end() && hitMap[I].find(J) != hitMap[I].end() && hitMap[I][J].find(K) != hitMap[I][J].end()) {
	    if((int)copyHit->getEnergy() > (int)hitMap[I][J][K]->getEnergy()) {
	      hitMap[I][J][K] = copyHit;
	    }
	  }
	  else hitMap[I][J][K] = copyHit;

	  hitCounter[geomDIF][geomASIC]++;
	  
	}

	for(auto DIFIt = hitCounter.begin(); DIFIt != hitCounter.end(); DIFIt++) {
	  int nHitsDIF = 0;
	  for(auto ASICIt = DIFIt->second.begin(); ASICIt != DIFIt->second.end(); ASICIt++) {
	    nHitsASICHist->Fill(ASICIt->second);
	    nHitsDIF += ASICIt->second;
	  }
	  nHitsDIFHist->Fill(nHitsDIF);
	}
	
	CellIDDecoder<CalorimeterHit> outcd(outCol);
	for(auto IIt = hitMap.begin(); IIt != hitMap.end(); IIt++) {
	  for(auto JIt = IIt->second.begin(); JIt != IIt->second.end(); JIt++) {
	    for(auto KIt = JIt->second.begin(); KIt != JIt->second.end(); KIt++) {
	      
	      outCol->addElement(KIt->second);
	    
	      IHist->Fill(outcd(KIt->second)["I"]);
	      JHist->Fill(outcd(KIt->second)["J"]);
	      KHist->Fill(outcd(KIt->second)["K"]);
	      THist->Fill(KIt->second->getEnergy());
	    }
	  }
	}
	
	LCEventImpl* evt = new LCEventImpl();
	evt->setEventNumber(++evtnum);
	evt->addCollection(outCol, "MCProtoEvent");

	_lcWriter->writeEvent(evt);
	delete evt;
	
      }catch (lcio::DataNotAvailableException zero) { std::cout << "ERROR READING COLLECTION: " << _inputCollections[i] << std::endl;} 
    }//end loop over collection      
  }//if (evtP)
}

//==============================================================
void MCDataFormatting::end()
{

  IHist->Write();
  JHist->Write();
  KHist->Write();
  THist->Write();

  ASICs->Write();
  DIFs->Write();

  nHitsASICHist->Write();
  nHitsDIFHist->Write();
  nHitsHist->Write();

  delete IHist;
  delete JHist;
  delete KHist;
  delete THist;

  delete ASICs;
  delete DIFs;

  delete nHitsASICHist;
  delete nHitsDIFHist;
  delete nHitsHist;

  rootFile->Close();
  delete rootFile;
  
  _lcWriter->close();  
  delete _lcWriter;
  
}
 
