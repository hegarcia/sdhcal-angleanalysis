#include "ElectronicNoiseCleanup.h"

#include <UTIL/CellIDDecoder.h>

#include <EVENT/LCCollection.h>

#include <IMPL/LCEventImpl.h>
#include <IMPL/LCCollectionVec.h>
#include <IMPL/CalorimeterHitImpl.h>

#include <TPaveText.h>

#include <iostream>

ElNoiseCleanup a_ElNoiseCleanup_instance;

//=========================================================
ElNoiseCleanup::ElNoiseCleanup()
  :Processor("ElNoiseCleanup")
{
  
  //Input Collections    
  _hcalCollections.push_back(std::string("SDHCAL_RawEvents"));
  registerInputCollections(LCIO::RAWCALORIMETERHIT, 
			   "HCALCollections",  
			   "HCAL Collection Names",  
			   _hcalCollections, 
			   _hcalCollections); 
  
  //Output file base name
  _outFileName="TB_SDHCALEvents_";
  registerProcessorParameter("LCIOOutputFile", 
			     "Base LCIO file name",
			     _outFileName,
			     _outFileName);

  //Output collection name
  _outColName="SDHCAL_Events";
  registerProcessorParameter("OutputCollectionName", 
			     "Name of the produced collection in this processor",
			     _outColName,
			     _outColName);

  
  //Output ROOT name
  _logRootName = "LogROOT_ElNoiseCleanup";
  registerProcessorParameter("logRoot_Name" ,
                             "LogRoot name",
                             _logRootName,
			     _logRootName);

  //Noisy ASIC Cut
  _noisyCut = 64;
  registerProcessorParameter("NoisyCut" ,
                             "Noisy ASIC Cut",
                             _noisyCut,
			     _noisyCut);

  //Pre asic cut
  _preCut = 10;
  registerProcessorParameter("PreCut" ,
                             "Pre noisy ASIC Cut",
                             _preCut,
			     _preCut);


  //Pre asic cut
  _postCut = 10;
  registerProcessorParameter("PostCut" ,
                             "Post noisy ASIC Cut",
                             _postCut,
			     _postCut);

  //Noisy DIF Cut
  _noisyDIFCut = 20;
  registerProcessorParameter("NoisyDIFCut" ,
                             "Noisy DIF Cut",
                             _noisyDIFCut,
			     _noisyDIFCut);
  
} 

void ElNoiseCleanup::init() {
  
  streamlog_out(DEBUG2) << "ElNoise cleanup begin" << std::endl;
  
  _lcWriter = LCFactory::getInstance()->createLCWriter() ;
  _lcWriter->setCompressionLevel( 0 );

  _outputFile = new TFile((_logRootName + ".root").c_str(),"RECREATE");	  
  
  printParameters();
  
}

//==================================================================================
void ElNoiseCleanup::processRunHeader( LCRunHeader * runHd ) {

  streamlog_out(MESSAGE) << "ElNoiseCleanup processing run: " << runHd->getRunNumber() << std::endl;

     if(runNumber != -1) {
       _lcWriter->close();
       writeHistograms();
       sysTool->WriteSystematics();
       delete sysTool;
     }
     runNumber = runHd->getRunNumber();

     streamlog_out(DEBUG2) << "New lcio output" << std::endl;

     _lcWriter->open((_outFileName + std::to_string(runNumber) + ".slcio").c_str(),LCIO::WRITE_NEW);
     _lcWriter->writeRunHeader(runHd);
	 
     streamlog_out(DEBUG2) << "Creating new local histograms" << std::endl;

     _outputFile->mkdir(std::to_string(runNumber).c_str());
     _outputFile->cd(std::to_string(runNumber).c_str());

     // NHits Histograms
     
     histograms1D["NHits"] = new TH1F("NHits", "Number of hits;NHits;Entries", 500, 0., 5000.);
     histograms1D["NHitsASIC"] = new TH1F("NHitsASIC", "Number of Hits in ASICs;nHit;Entries", 100, 0., 100.);
     histograms1D["NHitsDIF"] = new TH1F("NHitsDIF", "Number of Hits in DIFs;nHit;Entries", 3100, 0., 3100.);
     histograms1D["NHitsLayer"] = new TH1F("NHitsLayer", "Number of Hits in Layers;nHit;Entries", 9300, 0., 9300);

     histograms1D["NHitsCleanup"] = new TH1F("NHitsCleanup", "Number of hits;NHits;Entries", 500, 0., 5000.);
     histograms1D["NHitsASICCleanup"] = new TH1F("NHitsASICCleanup", "Number of Hits in ASICs;nHit;Entries", 100, 0., 100.);
     histograms1D["NHitsDIFCleanup"] = new TH1F("NHitsDIFCleanup", "Number of Hits in DIFs;nHit;Entries", 3100, 0., 3100.);
     histograms1D["NHitsLayerCleanup"] = new TH1F("NHitsLayerCleanup", "Number of Hits in Layers;nHit;Entries", 9300, 0., 9300);
     
     histograms1D["NHitsPostASIC"] = new TH1F("NHitsPostASIC", "Number of Hits in the ASIC behind a noisy one;nHit;Entries", 100, 0., 100.);
     histograms1D["NHitsPreASIC"] = new TH1F("NHitsPreASIC", "Number of Hits in ASIC in fron a noisy one;nHit;Entries", 100, 0., 100.);
     histograms2D["NHitsPreVsPost"] = new TH2F("NHitsPreVsPost", ";NHitsPre;NHitsPost", 100, 0., 100., 100, 0., 100.);

     // NASICs Histograms

     histograms1D["ASICsRemoved"] = new TH1F("ASICsRemoved", ";ASICID;Entries", 7100, 0., 7100.);
     histograms1D["IsolatedASICs"] = new TH1F("IsolatedASICs", ";ASICID;Entries", 7100, 0., 7100.);

     histograms1D["NASICs"] = new TH1F("NASICs", ";NASICs;Entries", 200, 0., 2000.);
     histograms1D["NASICsInDIF"] = new TH1F("NASICsInDIF", ";NASICs;nEntries", 51, 0., 51.);
     histograms1D["NASICsInLayer"] = new TH1F("NASICsInLayer", ";NASICs;Entries", 151, 0., 151.);

     histograms1D["NASICsCleanup"] = new TH1F("NASICsCleanup", ";NASICs;Entries", 200, 0., 2000.);
     histograms1D["NASICsInDIFCleanup"] = new TH1F("NASICsInDIFCleanup", ";NASICs;nEntries", 51, 0., 51.);
     histograms1D["NASICsInLayerCleanup"] = new TH1F("NASICsInLayerCleanup", ";NASICs;Entries", 151, 0., 151.);
     
     histograms1D["NASICsPostDIF"] = new TH1F("NASICsPostDIF", ";NASIcs;Entries", 51, 0., 51.);
     histograms1D["NASICsPreDIF"] = new TH1F("NASICsPreDIF", ";NASIcs;Entries", 51, 0., 51.);
     histograms2D["NASICsPreVsPost"] = new TH2F("NASICsPreVsPost", ";NASICsPre;NASICsPost", 51, 0., 51., 51, 0., 51.);
     
     // NDIFs Histograms

     histograms1D["DIFsRemoved"] = new TH1F("DIFsRemoved", ";DIFID;Entries", 151, 0., 151.);

     histograms1D["NDIFs"] = new TH1F("NDIFs",";nDIFs;Entries", 151, 0., 151.);
     histograms1D["NDIFsInLayer"] = new TH1F("NDIFsInLayer",";nDIFs;Entries", 5, 0., 5.);

     histograms1D["NDIFsCleanup"] = new TH1F("NDIFsCleanup",";nDIFs;Entries", 151, 0., 151.);
     histograms1D["NDIFsInLayerCleanup"] = new TH1F("NDIFsInLayerCleanup",";nDIFs;Entries", 5, 0., 5.);
     
     // NLayers Histograms

     histograms1D["NLayers"] = new TH1F("NLayers", ";NLayers;Entries", 51, 0., 51.);
     histograms1D["NLayersCleanup"] = new TH1F("NLayersCleanup", ";NLayers;Entries", 51, 0., 51.);
     
     evtnum = nASICsTotal = nDIFsTotal = 0;
     nNoisyASICs = nIsolatedASICs = nNoisyDIFs = nNoisyLayers = 0;
     
     streamlog_out(DEBUG2) << "Creating systematics tool" << std::endl;

     sysTool = new SystematicsTool(("Systematics_ElNoiseCleanup_" + std::to_string(runNumber) + ".txt").c_str());
     
     sysTool->AddCut("NoisyASICCut");
     //sysTool->AddCut("IsolatedASICCut");
     sysTool->AddCut("NoisyDIFCut");
    
}

 
void ElNoiseCleanup::processEvent( LCEvent * evtP ) 
{ 
  streamlog_out(DEBUG2) << "Start ElNoiseCleanup process" << std::endl;
  
  if(evtP){ //Check evtP
    for(unsigned int i=0; i < _hcalCollections.size(); i++) { //loop over collection
      try{
	
	LCCollection* col = evtP->getCollection(_hcalCollections[i].c_str());
	int initialNHits = col->getNumberOfElements();

	CellIDDecoder<CalorimeterHit> cd(col);
	
	streamlog_out(DEBUG2) << "Initial number of hits: " << initialNHits << std::endl;

	nHitsMap.clear();

	int nHitsCutNoisyASIC, nHitsCutNoisyASICUp, nHitsCutNoisyASICDown;
	nHitsCutNoisyASIC = nHitsCutNoisyASICUp = nHitsCutNoisyASICDown = 0; 
	int nHitsCutIsolatedASIC, nHitsCutIsolatedASICUp, nHitsCutIsolatedASICDown;
	nHitsCutIsolatedASIC = nHitsCutIsolatedASICUp = nHitsCutIsolatedASICDown = 0;

	bool flagToRemove, flagToRemoveUp, flagToRemoveDown;
	flagToRemove = flagToRemoveUp = flagToRemoveDown = false;
	
	for(int iHit = 0; iHit < initialNHits; iHit++) {

	  CalorimeterHit* hit = dynamic_cast<CalorimeterHit*>(col->getElementAt(iHit));

	  int I = (int)cd(hit)["I"];
	  int J = (int)cd(hit)["J"];
	  int K = (int)cd(hit)["K"];

	  //This assumes the dif_id in vertical position
	  int geomDIF = I/32;
	  int geomASIC = I/8 + J/8*12;

	  nHitsMap[K][geomDIF][geomASIC].push_back(hit);
	  
	}

	histograms1D.at("NHits")->Fill(initialNHits);

	int nASICs = 0, nDIFs = 0, nLayers = 0;

	// ASIC Cleanup and preCleanup variables
	
	for(auto layerIt = nHitsMap.begin(); layerIt != nHitsMap.end(); layerIt++) {
	  int nHitsInLayer = 0; //
	  int nDIFsInLayer = 0; //
        
	  nLayers++;
	  for(auto difIt = layerIt->second.begin(); difIt != layerIt->second.end(); difIt++) {
	    int nHitsInDIF = 0;//
	    int nASICsInDIF = 0;//
	    
	    nDIFs++;
	    nDIFsInLayer++;
	    //std::map<int, std::map<int,int>> nASICsMap;

	    auto asicIt = difIt->second.begin();
	    while(asicIt != difIt->second.end()) {
	      
	      nHitsInDIF += asicIt->second.size();
	      histograms1D.at("NHitsASIC")->Fill(asicIt->second.size());	      

	      nASICs++;
	      nASICsInDIF++;

	      int nHitsPreASIC = 0;
	      if(searchForASIC( layerIt->first - 1, difIt->first, asicIt->first)) nHitsPreASIC = nHitsMap.at(layerIt->first - 1).at(difIt->first).at(asicIt->first).size();

	      int nHitsPostASIC = 0;
	      if(searchForASIC( layerIt->first + 1, difIt->first, asicIt->first)) nHitsPostASIC = nHitsMap.at(layerIt->first + 1).at(difIt->first).at(asicIt->first).size();

	      // Selection 10% Up
	      
	      if(asicIt->second.size() < _noisyCut*1.1) nHitsCutNoisyASICUp += asicIt->second.size();
	      else {

		if(nHitsPostASIC > _postCut*0.9 && nHitsPreASIC > _preCut*0.9) nHitsCutNoisyASICUp += asicIt->second.size();
		else if((nHitsPostASIC <= _postCut*0.9 && nHitsPreASIC > _preCut*0.9) || (nHitsPostASIC > _postCut*0.9 && nHitsPreASIC <= _preCut*0.9)) flagToRemoveUp = true;

	      }

	      // Selection 10% Down
	      
	      if(asicIt->second.size() < _noisyCut*0.9)  nHitsCutNoisyASICDown += asicIt->second.size();
	      else {
		
		if(nHitsPostASIC > _postCut*1.1 && nHitsPreASIC > _preCut*1.1) nHitsCutNoisyASICDown += asicIt->second.size();
		else if((nHitsPostASIC <= _postCut*1.1 && nHitsPreASIC > _preCut*1.1) || (nHitsPostASIC > _postCut*1.1 && nHitsPreASIC <= _preCut*1.1)) flagToRemoveDown = true;

	      }

	      // Standard selection
	      
	      if(asicIt->second.size() < _noisyCut) nHitsCutNoisyASIC += asicIt->second.size();
	      else {
	
	        histograms1D.at("NHitsPostASIC")->Fill(nHitsPostASIC);
	        histograms1D.at("NHitsPreASIC")->Fill(nHitsPreASIC);
	        histograms2D.at("NHitsPreVsPost")->Fill(nHitsPreASIC,nHitsPostASIC);
		
		if(nHitsPostASIC > _postCut && nHitsPreASIC > _preCut) nHitsCutNoisyASIC += asicIt->second.size();
		else if(nHitsPostASIC <= _postCut && nHitsPreASIC <= _preCut) {
		  streamlog_out(DEBUG2) << "Removing noisy ASIC: " << asicIt->first << std::endl;
		  auto tmpIt = asicIt;
		  tmpIt++;
        	  nHitsMap.at(layerIt->first).at(difIt->first).erase(asicIt);
		  asicIt = tmpIt;
		  nNoisyASICs++;
		  histograms1D.at("ASICsRemoved")->Fill(asicIt->first + layerIt->first*144);
		  //streamlog_out(DEBUG7) << "Removing noisy ASIC in event: " << evtnum << std::endl;
		  continue;
		}
		else {
		  streamlog_out(DEBUG3) << "Electronic issues gap in event: " << evtP->getEventNumber() << std::endl;
		  flagToRemove = true;
		}
	      }

	      // Isolated ASIC test
	      
	      /*if(!scanIsolated(layerIt->first, difIt->first, asicIt->first, 3)) nHitsCutIsolatedASICUp += asicIt->second.size();
	      if(!scanIsolated(layerIt->first, difIt->first, asicIt->first, 1)) nHitsCutIsolatedASICDown += asicIt->second.size();	      

	      if(!scanIsolated(layerIt->first, difIt->first, asicIt->first, 2)) nHitsCutIsolatedASIC += asicIt->second.size();
	      else {
		streamlog_out(DEBUG2) << "Removing isolated ASIC: " << asicIt->first << std::endl;
		auto tmpIt = asicIt;
		tmpIt++;
		nHitsMap.at(layerIt->first).at(difIt->first).erase(asicIt);
		asicIt = tmpIt;
		nIsolatedASICs++;
		histograms1D.at("IsolatedASICs")->Fill(asicIt->first + layerIt->first*144);
        	continue;
		}*/

	      asicIt++;
	      
	    } // End of ASICs loop
	    
	    histograms1D.at("NHitsDIF")->Fill(nHitsInDIF);
	    histograms1D.at("NASICsInDIF")->Fill(nASICsInDIF);
	    nHitsInLayer += nHitsInDIF;

	  } // End loop over DIFs
	  
	  histograms1D.at("NHitsLayer")->Fill(nHitsInLayer);
	  histograms1D.at("NDIFsInLayer")->Fill(nDIFsInLayer);
	  
	} // End loop over Layers

	if(flagToRemoveUp) nHitsCutNoisyASICUp = -999; 
	if(flagToRemoveDown) nHitsCutNoisyASICDown = -999;	

	if(flagToRemove) {
	  sysTool->FillCut("NoisyASICCut", -1, nHitsCutNoisyASICUp, nHitsCutNoisyASICDown);
	  return;
	}

	sysTool->FillCut("NoisyASICCut", nHitsCutNoisyASIC, nHitsCutNoisyASICUp, nHitsCutNoisyASICDown);

	//sysTool->FillCut("IsolatedASICCut", nHitsCutIsolatedASIC, nHitsCutIsolatedASICUp, nHitsCutIsolatedASICDown);
	        
        histograms1D.at("NASICs")->Fill(nASICs);
        histograms1D.at("NDIFs")->Fill(nDIFs);
        histograms1D.at("NLayers")->Fill(nLayers);
	
	nASICsTotal += nASICs;
	nDIFsTotal += nDIFs;
        
	// Noisy DIFs cleanup

	int nHitsCutNoisyDIF, nHitsCutNoisyDIFUp, nHitsCutNoisyDIFDown;
	nHitsCutNoisyDIF = nHitsCutNoisyDIFUp = nHitsCutNoisyDIFDown = 0;
	
	auto layerIt = nHitsMap.begin();
	while(layerIt != nHitsMap.end()) {

	  if(layerIt->second.size() == 0) {
	    auto tmpIt = layerIt;
	    tmpIt++;
	    nHitsMap.erase(layerIt);
	    layerIt = tmpIt;
	    continue;
	  }
	  
	  auto difIt = layerIt->second.begin();
	  while(difIt != layerIt->second.end()) {
	    
	    if(difIt->second.size() == 0) {
	      auto tmpIt = difIt;
	      tmpIt++;
	      layerIt->second.erase(difIt);
	      difIt = tmpIt;
	      continue;
	    }

	    int nASICsInDIF = difIt->second.size();

	    int nHitsInDIF = 0;
	    for(auto asicIt = difIt->second.begin(); asicIt != difIt->second.end(); asicIt++) {
	      nHitsInDIF += asicIt->second.size();
	    }

	    if(nASICsInDIF < _noisyDIFCut*1.1) nHitsCutNoisyDIFUp += nHitsInDIF;
	    if(nASICsInDIF < _noisyDIFCut*0.9) nHitsCutNoisyDIFDown += nHitsInDIF;
	    
	    if(nASICsInDIF < _noisyDIFCut) nHitsCutNoisyDIF += nHitsInDIF;
	    else {
		 //streamlog_out(DEBUG2) << "Removing noisy DIF: " << difIt->first << std::endl;

	      int nASICsPost = 0;
	      if(searchForDIF(layerIt->first + 1,difIt->first)) {
		nASICsPost = nHitsMap.at(layerIt->first + 1).at(difIt->first).size();
	      }
	      histograms1D.at("NASICsPostDIF")->Fill(nASICsPost);

	      int nASICsPre = 0;
	      if(searchForDIF(layerIt->first - 1,difIt->first)) {
		for(auto asicIt = nHitsMap.at(layerIt->first - 1).at(difIt->first).begin(); asicIt != nHitsMap.at(layerIt->first - 1).at(difIt->first).end(); asicIt++) {
		  nASICsPre++;
		}
	      }
	      histograms1D.at("NASICsPreDIF")->Fill(nASICsPre);
	      
	      histograms2D.at("NASICsPreVsPost")->Fill(nASICsPre,nASICsPost);

	      auto tmpIt = difIt;
	      tmpIt++;
	      layerIt->second.erase(difIt);
	      difIt = tmpIt;
	      nNoisyDIFs++;
	      histograms1D.at("DIFsRemoved")->Fill(difIt->first + layerIt->first*3);
	      //streamlog_out(DEBUG7) << "Removing noisy DIF in event: " << evtnum << std::endl;
	      continue;
	    }

	    difIt++;
	    
	  } // End of DIF loop

          
	  layerIt++;
	  
	} // End of Layer loop


	sysTool->FillCut("NoisyDIFCut", nHitsCutNoisyDIF, nHitsCutNoisyDIFUp, nHitsCutNoisyDIFDown);

	LCEventImpl* evt = new LCEventImpl();
	evt->setRunNumber(evtP->getRunNumber());
	evt->setTimeStamp(evtP->getTimeStamp());
	evt->setEventNumber(++evtnum);
	
	IMPL::LCCollectionVec* outcolnew = new IMPL::LCCollectionVec(LCIO::CALORIMETERHIT);        
	outcolnew->setDefault();
	outcolnew->setFlag(outcolnew->getFlag() | (1 << LCIO::RCHBIT_LONG));

	IMPL::LCCollectionVec* eventcolnew = new IMPL::LCCollectionVec(LCIO::CALORIMETERHIT);
	eventcolnew->setFlag(col->getFlag());
	eventcolnew->setSubset(true);
	
	std::vector<int> timeParameters = {};
	col->parameters().getIntVals("TimeParameters",timeParameters);

	outcolnew->parameters().setValues("TimeParameters", timeParameters);
	outcolnew->parameters().setValue("TimeInSpill", col->parameters().getFloatVal("TimeInSpill"));

	eventcolnew->parameters().setValues("TimeParameters", timeParameters);
	eventcolnew->parameters().setValue("TimeInSpill", col->parameters().getFloatVal("TimeInSpill"));
	
	int nCleanHits = 0; // 
	int nCleanDIFs = 0; //
	int nCleanASICs = 0; //
	int nCleanLayers = 0; //
	
	for(auto clLayerIt = nHitsMap.begin(); clLayerIt != nHitsMap.end(); clLayerIt++) {
	  
	  int nCleanHitsInLayer = 0; //
	  int nCleanDIFsInLayer = 0; //
	  int nCleanASICsInLayer = 0; //
	  
	  nCleanLayers++;
	  for(auto clDIFIt = clLayerIt->second.begin(); clDIFIt != clLayerIt->second.end(); clDIFIt++) {
	    int nCleanHitsInDIF = 0; //
	    int nCleanASICsInDIF = 0; //
	    
	    nCleanDIFsInLayer++;
	    nCleanDIFs++;

	    for(auto clASICIt = clDIFIt->second.begin(); clASICIt != clDIFIt->second.end(); clASICIt++) {
	      histograms1D.at("NHitsASICCleanup")->Fill(clASICIt->second.size());
	      nCleanHitsInDIF += clASICIt->second.size();
	      
	      nCleanASICs++;
	      nCleanASICsInDIF++;

	      for(auto clHitIt = clASICIt->second.begin(); clHitIt != clASICIt->second.end(); clHitIt++) {
	        CalorimeterHitImpl* copyHit = new CalorimeterHitImpl(*(static_cast<CalorimeterHitImpl*>(*clHitIt)));
		outcolnew->addElement(copyHit);
		eventcolnew->addElement(*clHitIt);
	      }

	    } // End loop over ASICs
	    histograms1D.at("NHitsDIFCleanup")->Fill(nCleanHitsInDIF);
	    nCleanHitsInLayer += nCleanHitsInDIF;
	    histograms1D.at("NASICsInDIFCleanup")->Fill(nCleanASICsInDIF);
	    nCleanASICsInLayer += nCleanASICsInDIF;
	  } //End loop over DIFs
	  histograms1D.at("NHitsLayerCleanup")->Fill(nCleanHitsInLayer);
	  nCleanHits += nCleanHitsInLayer;
	  histograms1D.at("NASICsInLayerCleanup")->Fill(nCleanASICsInLayer);
	  histograms1D.at("NDIFsInLayerCleanup")->Fill(nCleanDIFsInLayer);
	} // End loop over Layers

        evt->addCollection(outcolnew,_outColName);
	evtP->addCollection(eventcolnew,_outColName);
	
	_lcWriter->writeEvent(evt);
        delete evt; 
        
        histograms1D.at("NHitsCleanup")->Fill(nCleanHits);
        histograms1D.at("NASICsCleanup")->Fill(nCleanASICs);
        histograms1D.at("NDIFsCleanup")->Fill(nCleanDIFs);
	histograms1D.at("NLayersCleanup")->Fill(nCleanLayers);

      }catch (lcio::DataNotAvailableException zero) { std::cout << "ERROR READING COLLECTION: " << _hcalCollections[i] << std::endl;}
    }//end loop over collection      
  }//if (evtP)
}

//==============================================================
void ElNoiseCleanup::end()
{

  _lcWriter->close();  
  delete _lcWriter;
  writeHistograms();

  sysTool->WriteSystematics();
  delete sysTool;
  
  _outputFile->Close();
  delete _outputFile;

  streamlog_out(DEBUG2) << "Ending ElectronicNoiseCleanup Processor." << std::endl;
  
}
 

void ElNoiseCleanup::writeHistograms() {

  _outputFile->cd(std::to_string(runNumber).c_str());
  
  TPaveText stats(0.05, 0.1, 0.95, 0.9);
  stats.AddText(("Percentage of noisy ASICs:" + std::to_string(nNoisyASICs*100/(float)nASICsTotal) + "%").c_str());
  //stats.AddText(("Percentage of isolated ASICs:" + std::to_string(nIsolatedASICs*100/(float)nASICsTotal) + "%").c_str());
  stats.AddText(("Percentage of noisy DIFs:" + std::to_string(nNoisyDIFs*100/(float)nDIFsTotal) + "%").c_str());
  stats.Write();

  for(auto It1D = histograms1D.begin(); It1D != histograms1D.end(); It1D++) {
    It1D->second->Write();
    delete It1D->second;
  }

  for(auto It2D = histograms2D.begin(); It2D != histograms2D.end(); It2D++) {
    It2D->second->Write();
    delete It2D->second;
  }
    
}


bool ElNoiseCleanup::searchForASIC(int layer, int dif, int asic) {
  
  if(nHitsMap.find(layer) != nHitsMap.end() && nHitsMap.at(layer).find(dif) != nHitsMap.at(layer).end() && nHitsMap.at(layer).at(dif).find(asic) != nHitsMap.at(layer).at(dif).end()) return true;
  return false;

}

bool ElNoiseCleanup::searchForDIF(int layer, int dif) {
  
  if(nHitsMap.find(layer) != nHitsMap.end() && nHitsMap.at(layer).find(dif) != nHitsMap.at(layer).end()) return true;
  return false;

}


bool ElNoiseCleanup::scanIsolated(int testLayer, int testDIF, int testASIC, int searchWindow) {

  for(int iScan = -searchWindow; iScan <= searchWindow; iScan++) {
    streamlog_out(DEBUG3) << "IScan: " << iScan << std::endl;
    for(int jScan = -searchWindow; jScan <= searchWindow; jScan++) {
      streamlog_out(DEBUG3) << "JScan: " << jScan << std::endl;
		  
      int asicIScanned = testASIC/12 + iScan;
      int asicJScanned = testASIC/4 + jScan;
		  
      int difScanned = testDIF;
      if(asicIScanned == -1) {
	asicIScanned = 3;
	difScanned--;
      }

      if(asicIScanned == 4) {
	asicIScanned = 0;
	difScanned++;
      }
      
      int asicScanned = asicJScanned*4 + asicIScanned;
		  
      for(int kScan = -searchWindow; kScan <= searchWindow; kScan++) {
	streamlog_out(DEBUG3) << "KScan: " << kScan << std::endl;
		  
	if(iScan == 0 && jScan == 0 && kScan == 0) continue;
	if(searchForASIC(testLayer + kScan,difScanned,asicScanned)) {
	  return false;
	}
      }
    }
  }
  
  return true;

}
