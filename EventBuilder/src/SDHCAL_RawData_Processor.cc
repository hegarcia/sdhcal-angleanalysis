#include "SDHCAL_RawData_Processor.h"
#include "DIFUnpacker.h"

#include <EVENT/LCIO.h>
#include <EVENT/LCCollection.h>
#include <UTIL/LCTOOLS.h>
#include "IMPL/LCCollectionVec.h"
#include "IMPL/LCFlagImpl.h"
#include "IMPL/RawCalorimeterHitImpl.h"

#include <TPaveText.h>

#include <iostream>
#include <assert.h>
#include <algorithm>

void SDHCAL_buffer::printBuffer(unsigned int start, unsigned int stop,std::ostream& flux)
{
  flux << std::hex;
  for (unsigned int k=start; k<stop; k++)
    flux << (unsigned int)(first[k]) << " - ";
  flux << std::dec <<  std::endl;
}



SDHCAL_RawBuffer_Navigator::SDHCAL_RawBuffer_Navigator(SDHCAL_buffer b) :
  _buffer(b),_SCbuffer(0,0)
{
  _DIFstartIndex=DIFUnpacker::getStartOfDIF(_buffer.buffer(),_buffer.getsize(),24);
  _theDIFPtr=NULL;
  _badSCdata=false;
}

DIFPtr* SDHCAL_RawBuffer_Navigator::getDIFPtr()
{
  if (NULL==_theDIFPtr) _theDIFPtr=new DIFPtr(getDIFBufferStart(),getDIFBufferSize());
  return _theDIFPtr;
}

uint32_t SDHCAL_RawBuffer_Navigator::getDIF_CRC()
{
  uint32_t i=getEndOfDIFData();
  uint32_t ret=0;
  ret |= ( (_buffer.buffer()[i-2])<<8 );
  ret |= _buffer.buffer()[i-1];
  return ret;
}

void SDHCAL_RawBuffer_Navigator::setSCBuffer()
{
  if (! hasSlowControlData() ) return;
  if (0 != _SCbuffer.getsize() ) return; //deja fait
  if (_badSCdata) return;
  _SCbuffer.first=&(getDIFBufferStart()[getEndOfDIFData()]);
  //compute Slow Control size
  uint32_t maxsize=_buffer.getsize()-_DIFstartIndex-getEndOfDIFData()+1; // should I +1 here ?
  uint32_t k=1; //SC Header
  uint32_t dif_ID=_SCbuffer.first[1];
  uint32_t chipSize=_SCbuffer.first[3];
    while ( (dif_ID != 0xa1 && _SCbuffer.first[k] != 0xa1 && k <maxsize) ||
	    (dif_ID == 0xa1 && _SCbuffer.first[k+2]==chipSize && k<maxsize) )
    {
      k+=2; //DIF ID + ASIC Header
      uint32_t scsize=_SCbuffer.first[k];
      if (scsize != 74 && scsize != 109)
	{
	  //std::cout << "PROBLEM WITH SC SIZE " << scsize << std::endl;
	  k=0;
	  _badSCdata=true;
	  break;
	}
      k++; //skip size bit
      k+=scsize; // skip the data
    }
  if (_SCbuffer.first[k] == 0xa1 && !_badSCdata ) _SCbuffer.second=k+1; //add the trailer
  else
    {
      _badSCdata=true;
      //std::cout << "PROBLEM SC TRAILER NOT FOUND " << std::endl;
    }
}


SDHCAL_buffer SDHCAL_RawBuffer_Navigator::getEndOfAllData()
{
  setSCBuffer();
  if (hasSlowControlData() && !_badSCdata)
    {
      return SDHCAL_buffer( &(_SCbuffer.buffer()[_SCbuffer.getsize()]), getSizeAfterDIFPtr()-3-_SCbuffer.getsize() );
    }
  else
    return SDHCAL_buffer( &(getDIFBufferStart()[getEndOfDIFData()]), getSizeAfterDIFPtr()-3 ); //remove the 2 bytes for CRC and the DIF trailer
}



// --------------------- Processor function definitions -----------------------------


SDHCAL_RawData_Processor aSDHCAL_RawData_Processor ;

SDHCAL_RawData_Processor::SDHCAL_RawData_Processor() : Processor("SDHCAL_RawData_Processor") {

    // Modify processor description
    _description = "SDHCAL_RawData_Processor converts SDHCAL Raw Data into collection of RawCalorimeterHit" ;


    // Register steering parameters: name, description, class-variable, default value

    registerInputCollection( LCIO::LCGENERICOBJECT,
			     "XDAQCollectionName" ,
			     "XDAQ produced collection name" ,
			     _XDAQCollectionNames,
			     std::string("RU_XDAQ"));


    registerOutputCollection( LCIO::RAWCALORIMETERHIT,
			      "OutputRawCaloHitCollectionName" ,
			      "Name of output collection containing raw calorimeter hits"  ,
			      _RawHitCollectionName ,
			      std::string("SDHCALRawHits"));

    // Do not read this DIFs 
    _skipDIFs = {};
    registerProcessorParameter("SkipDIFs",
			       "Do not read this DIFs. Reasons: DIFs are malfunctioning, they belong to other electronics, etc.",
			       _skipDIFs,
			       _skipDIFs);

    
    //ROOT Log file base name 
    _outFileBaseName = "LogROOT_RawDataProc";
    registerProcessorParameter("OutBaseName",
			       "Base name for the ROOT log file. The run number will we appended.",
			       _outFileBaseName,
			       _outFileBaseName);

    _debugMode=false;
    registerProcessorParameter( "DebugMode",
				"Turn ON/OFF debug mode : Warning Debug mode uses assert and may crash the application",
				_debugMode,
			        _debugMode);
    

    
    _nrun = 0;
}



void SDHCAL_RawData_Processor::init() {

  streamlog_out(DEBUG) << "Init RawData Processor" << std::endl;

  logFile = new TFile((_outFileBaseName + ".root").c_str(), "RECREATE");
  
  printParameters() ;
  
}


void SDHCAL_RawData_Processor::processRunHeader( LCRunHeader* run) {

  if(_runNumber != -1) writeHistograms();

  streamlog_out(MESSAGE) << "Processing run: " << run->getRunNumber() << std::endl;  

  _runNumber = run->getRunNumber();
  _nrun++;
  
  logFile->mkdir(std::to_string(_runNumber).c_str());
  logFile->cd(std::to_string(_runNumber).c_str());

  collSize = new TH1F("CollSize", "NLCGenericObjects;NObects;Entries", 1000, 0, 10000);
  DIFStart = new TH1F("DIFStart", "Position of the start of data after header;DIFStart;Entries", 50, 0, 50);
  DIFEndValue = new TH1F("DIFEndValue", "Value after DIF end;EndValue;Entries", 256, 0, 256);
  sizeAfterDIF = new TH1F("SizeAfterDIF", "Size remaining in buffer after DIF;Size;Entries", 21, 0, 21);
  sizeAfterData = new TH1F("SizeAfterData", "Size remaining after all data;Size;Entries", 21, 0, 21);
  
  afterDataCounters = new TH1F("AfterDataCunters", "Number of non zero values after data;Counter;Entries", 11, 0, 11);
  afterDataValues = new TH1F("AfterDataValues", "Non zero values after data;Values;Entries", 256, 0, 256);
  DIFWithEndValues = new TH1F("DIFWithEndValues", "DIFs with nonzero end values",200, 0, 200);
  
  LCTOOLS::dumpRunHeader(run);

  _nevt=_nWrongBuffers=_nWrongObj=_nProcessedObject=_hasSlowControl=_hasBadSlowControl=nskips=0;

}


void SDHCAL_RawData_Processor::processEvent( LCEvent * evt ) {

  streamlog_out(DEBUG) << "Process RawData Processor" << std::endl;  
  _nevt++;

  IMPL::LCCollectionVec *RawVec=new IMPL::LCCollectionVec(LCIO::RAWCALORIMETERHIT) ;

  IMPL::LCFlagImpl chFlag(0);
  EVENT::LCIO bitinfo;
  chFlag.setBit(bitinfo.RCHBIT_LONG ) ;                    // raw calorimeter data
  chFlag.setBit(bitinfo.RCHBIT_BARREL ) ;                  // barrel
  chFlag.setBit(bitinfo.RCHBIT_ID1 ) ;                     // cell ID
  chFlag.setBit(bitinfo.RCHBIT_TIME ) ;                    // timestamp
  RawVec->setFlag( chFlag.getFlag()  ) ;

  try{

    LCCollection* col = NULL;
    col = evt->getCollection( _XDAQCollectionNames );
    int nElement=col->getNumberOfElements();

    _CollectionSizeCounter[nElement]++;
    collSize->Fill(nElement);
    int totalHits = 0;
    for (int iel=0; iel<nElement; iel++)
      {
	LCGenericObject* obj=dynamic_cast<LCGenericObject*>(col->getElementAt(iel));
	if (NULL==obj)
	  {
	    _nWrongObj++;
	    continue;
	  }
	_nProcessedObject++;
	LMGeneric *lmobj=(LMGeneric *) obj;
	unsigned char* debug_variable_1=lmobj->getSDHCALBuffer().endOfBuffer();
	SDHCAL_RawBuffer_Navigator bufferNavigator(lmobj->getSDHCALBuffer());
	unsigned char* debug_variable_2=bufferNavigator.getDIFBuffer().endOfBuffer();
	if (_debugMode) assert(bufferNavigator.getDIFPtr()!=NULL);
	streamlog_out(DEBUG) << "Processing DIF: " << bufferNavigator.getDIFPtr()->getID() << std::endl;
	streamlog_out(DEBUG) << "DIF BUFFER END " << (unsigned int *) debug_variable_1 << " " << (unsigned int *) debug_variable_2 << std::endl;
	if (_debugMode) assert (debug_variable_1 == debug_variable_2);

	uint32_t idstart=bufferNavigator.getStartOfDIF();
	streamlog_out(DEBUG) << "DIF header starts at " << idstart
			     << " and is equal to " << std::hex << (unsigned int) lmobj->getCharBuffer()[idstart] << std::dec << std::endl;
	if(idstart==0 && streamlog::out.write< streamlog::DEBUG >() ) lmobj->getSDHCALBuffer().printBuffer( 0 , streamlog::out() );
	_DIFStarter[idstart]++;
	DIFStart->Fill(idstart);
        
	if (!  bufferNavigator.validBuffer() ) {
	  _nWrongBuffers++;
	  continue;
	}

        DIFPtr *d=bufferNavigator.getDIFPtr();
	if (d!=NULL)
	  {
	    if(std::find(_skipDIFs.begin(), _skipDIFs.end(), (int)d->getID()) != _skipDIFs.end()) {

	      nskips++;
	      streamlog_out(DEBUG) << "SKIPPING  DIF: " << (int)d->getID() << std::endl;
	      continue;

	    }
	    
	    _DIFPtrValueAtReturnedPos[bufferNavigator.getDIFBufferStart()[d->getGetFramePtrReturn()] ]++;
	    
	    DIFEndValue->Fill(bufferNavigator.getDIFBufferStart()[d->getGetFramePtrReturn()]);
	    if (_debugMode) assert( bufferNavigator.getDIFBufferStart()[d->getGetFramePtrReturn()] == 0xa0);
	  
	
	    _SizeAfterDIFPtr[bufferNavigator.getSizeAfterDIFPtr()]++;
	    sizeAfterDIF->Fill(bufferNavigator.getSizeAfterDIFPtr());
	    
	    for (uint32_t i=0;i<d->getNumberOfFrames();i++)
	      {
		for (uint32_t j=0;j<64;j++)
		  {
		    if (!(d->getFrameLevel(i,j,0) || d->getFrameLevel(i,j,1))) continue; // skip empty pads	
		    unsigned long int ID0;
		    ID0=(unsigned long int)(((unsigned short)d->getID())&0xFF);			//8 firsts bits: DIF Id
		    
		    ID0+=(unsigned long int)(((unsigned short)d->getFrameAsicHeader(i)<<8)&0xFF00);	//8 next bits:   Asic Id
		    bitset<6> Channel(j);
		    ID0+=(unsigned long int)((Channel.to_ulong()<<16)&0x3F0000);			//6 next bits:   Asic's Channel
		    unsigned long BarrelEndcapModule=0;  //(40 barrel + 24 endcap) modules to be coded here  0 for testbeam (over 6 bits)
		ID0+=(unsigned long int)((BarrelEndcapModule<<22)&0xFC00000);
		unsigned long int ID1 = (unsigned long int)(d->getFrameBCID(i));
		//std::cout << d->getFrameBCID(i) << std::endl;
		bitset<2> ThStatus;
		ThStatus[0]=d->getFrameLevel(i,j,0);
		ThStatus[1]=d->getFrameLevel(i,j,1);
		//ThStatus[2]=isSynchronised; //I'm not computing the synchronisation here.

		IMPL::RawCalorimeterHitImpl *hit=new IMPL::RawCalorimeterHitImpl() ;
		hit->setCellID0((unsigned long int)ID0);
		hit->setCellID1(ID1);
		hit->setAmplitude(ThStatus.to_ulong());
		unsigned long int TTT = (unsigned long int)(d->getFrameTimeToTrigger(i));

		hit->setTimeStamp(TTT);			      		//Time stamp of this event from Run Begining
		RawVec->addElement(hit);
	      }//for (uint32_t j=0;j<64;j++)
	    
	  }//for (uint32_t i=0;i<d->getNumberOfFrames();i++)

	//register Triggers'time : lots of values here ?
	
	lcio::IntVec trig(8);
	trig[0] = d->getDTC();
	trig[1] = d->getGTC();
	trig[2] = d->getBCID();
	trig[3] = d->getAbsoluteBCID()&0xFFFFFF;
	trig[4] = (d->getAbsoluteBCID()/(0xFFFFFF+1))&0xFFFFFF;
	trig[5] = d->getTASU1(); //what happen if no temperature ?
	trig[6] = d->getTASU2();
	trig[7] = d->getTDIF();

	std::stringstream ss("");
	ss<<"DIF"<<d->getID()<<"_Triggers";
	RawVec->parameters().setValues(ss.str(),trig);

	if (bufferNavigator.hasSlowControlData()) _hasSlowControl++;
	if (bufferNavigator.badSCData())  _hasBadSlowControl++;

	SDHCAL_buffer eod=bufferNavigator.getEndOfAllData();
	_SizeAfterAllData[eod.getsize()]++;
	sizeAfterData->Fill(eod.getsize());
	unsigned char* debug_variable_3=eod.endOfBuffer();
	streamlog_out(DEBUG) << "END DATA BUFFER END " << (unsigned int *) debug_variable_1 << " " << (unsigned int *) debug_variable_3 << std::endl;
	if (_debugMode) assert (debug_variable_1 == debug_variable_3);
	streamlog_out(DEBUG) << "End of Data remaining stuff : ";
	if(streamlog::out.write< streamlog::DEBUG >() ) eod.printBuffer( 0 , streamlog::out() );

	int nonzeroCount=0;
	for (unsigned char* it=eod.buffer(); it != eod.endOfBuffer(); it++) {
	  if (int(*it) !=0) {
	    afterDataValues->Fill((int)*it);
	    nonzeroCount++;
	  }
	}

	if(nonzeroCount > 0) DIFWithEndValues->Fill(d->getID());
	
	_NonZeroValusAtEndOfData[nonzeroCount]++;
	afterDataCounters->Fill(nonzeroCount);

	  }
      } //for (int iel=0; iel<nElement; iel++)
    
  }
  catch(DataNotAvailableException &e){
       streamlog_out(WARNING) << _XDAQCollectionNames << " collection not available" << std::endl;
       
  }

  evt->addCollection(RawVec,_RawHitCollectionName);
}



void SDHCAL_RawData_Processor::check( LCEvent * evt ) {
    // nothing to check here - could be used to fill checkplots in reconstruction processor
}


void SDHCAL_RawData_Processor::printCounter(std::string description, std::map<int,int> &m)
{
  streamlog_out(MESSAGE) << " statistics for " << description << " : ";
  for (std::map<int,int>::iterator it=m.begin(); it != m.end(); it++)
    {
      if (it != m.begin() ) streamlog_out(MESSAGE) << ",";
      streamlog_out(MESSAGE) << " [" << it->first << "]=" << it->second;
    }
  streamlog_out(MESSAGE) << std::endl;
}


void SDHCAL_RawData_Processor::end(){

  writeHistograms();
  
  logFile->Close();
  delete logFile;
  
}


void SDHCAL_RawData_Processor::writeHistograms() {

  streamlog_out(MESSAGE) << "RUN STATISTICS : " << _runNumber << std::endl;
  streamlog_out(MESSAGE) << _nrun << " runs for " << _nevt << " events." << std::endl;
  printCounter("Size of GenericObject collections",_CollectionSizeCounter);
  streamlog_out(MESSAGE) << "Number of processed LCGenericObject : " <<  _nProcessedObject << std::endl;
  streamlog_out(MESSAGE) << "Number of badly extracted LCGenericObject : " <<  _nWrongObj << std::endl;
  streamlog_out(MESSAGE) << "Number of wrong buffers: " << _nWrongBuffers << std::endl;
  printCounter("Start of DIF header",_DIFStarter);
  std::cout << "nskips = " << nskips << std::endl;
  printCounter("Value after DIF data are processed",_DIFPtrValueAtReturnedPos);
  printCounter("Size remaining in buffer after end of DIF data",  _SizeAfterDIFPtr );
  streamlog_out(MESSAGE) << "Number of Slow Control found " << _hasSlowControl << " out of which " << _hasBadSlowControl << " are bad" << std::endl;
  printCounter("Size remaining after all of data have been processed",_SizeAfterAllData );
  printCounter("Number on non zero values in end of data buffer",_NonZeroValusAtEndOfData);

  logFile->cd(std::to_string(_runNumber).c_str());
  
  TPaveText stats(0.05, 0.1, 0.95, 0.9);
  stats.AddText(("NEvents: " + std::to_string(_nevt)).c_str());
  stats.AddText(("NProcessed LCGenericObject: " + std::to_string(_nProcessedObject)).c_str());
  stats.AddText(("NBadly extracted LCGenericObject: " + std::to_string(_nWrongObj)).c_str());
  stats.AddText(("NWrong buffers: " + std::to_string(_nWrongBuffers)).c_str());
  stats.AddText(("NSlowControl found: " + std::to_string(_hasSlowControl)).c_str());
  stats.AddText(("NBadSlowControl: " + std::to_string(_hasBadSlowControl)).c_str());
  stats.Write();
  
  collSize->Write();
  DIFStart->Write();
  DIFEndValue->Write();
  sizeAfterDIF->Write();
  sizeAfterData->Write();
  afterDataCounters->Write();
  afterDataValues->Write();
  DIFWithEndValues->Write();
  
  delete collSize;
  delete DIFStart;
  delete DIFEndValue;
  delete sizeAfterDIF;
  delete sizeAfterData;
  delete afterDataCounters;
  delete afterDataValues;
  delete DIFWithEndValues;
 
}
