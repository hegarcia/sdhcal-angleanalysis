#include "TriventProc.h"

#include "json.hpp"

#include <UTIL/CellIDDecoder.h>
#include <EVENT/LCCollection.h>
#include <EVENT/MCParticle.h>
#include <EVENT/CalorimeterHit.h>
#include <IMPL/LCEventImpl.h>

#include <TCanvas.h>
#include <TLine.h>
#include <TPaveText.h>

#include <iostream>
#include <fstream>
#include <assert.h>

TriventProc a_TriventProc_instance;

//=========================================================
TriventProc::TriventProc()
  :Processor("TriventProc")
{
  
  //Input Collections    
  _hcalCollections.push_back(std::string("SDHCALRawHits"));
  registerInputCollections(LCIO::RAWCALORIMETERHIT, 
			   "HCALCollections",  
			   "HCAL Collection Names",  
			   _hcalCollections, 
			   _hcalCollections); 
  
  //Output file base name
  _outFileName="TB_RawEvents_";
  registerProcessorParameter("LCIOOutputFile", 
			     "Base LCIO file name",
			     _outFileName,
			     _outFileName);

  //mapping 
  _mappingfile = "mapping_ps.txt";
  registerProcessorParameter("DIFMapping" ,
                             "DIF's mapping file ",
                             _mappingfile,
                             _mappingfile);

  // electronic noise cut
  _elec_noise_cut = 5000;
  registerProcessorParameter("electronic_noise_cut" ,
                             "number of hit max on time stamp",
                             _elec_noise_cut,
                             _elec_noise_cut);

  //inter layer gap size
  _layer_gap = 2.8;
  registerProcessorParameter("layerGap" ,
                             "Layers Gap in (cm)",
                             _layer_gap,
                             _layer_gap);
   
  // noise cut
  _noiseCut = 10;
  registerProcessorParameter("noiseCut" ,
                             "noise cut in time spectrum 10 in default",
                             _noiseCut ,
                             _noiseCut);


  // time windows
  _timeWin = 2;
  registerProcessorParameter("timeWin" ,
                             "time window = 2 in default",
                             _timeWin ,
                             _timeWin);

  //Output ROOT name
  _logrootName = "LogROOT";
  registerProcessorParameter("logroot_Name" ,
                             "Logroot name",
                             _logrootName,
			     _logrootName);

} 

void TriventProc::init() {
  
  streamlog_out(DEBUG1) << "Trivent begin " << std::endl;

  TriventProc::processGeometry(_mappingfile.c_str());
  
  for(std::map<int,LayerID>::iterator itt = _mapping.begin();itt!=_mapping.end();itt++)     {
    std::cout << itt->first << "\t" << itt->second.K 
	      <<"\t"<<itt->second.DifX 
	      <<"\t"<<itt->second.DifY
	      <<"\t"<<itt->second.IncX
      	      <<"\t"<<itt->second.IncY
	      << std::endl;
  }


  _lcWriter = LCFactory::getInstance()->createLCWriter() ;
  _lcWriter->setCompressionLevel( 0 );

  _outputFile = new TFile((_logrootName + ".root").c_str(),"RECREATE");	  
    
  printParameters();
  
}

void TriventProc::processGeometry(std::string jsonFile)
{
	_mapping.clear() ;

	std::ifstream file(jsonFile) ;
	auto json = nlohmann::json::parse(file) ;

	auto chambersList = json.at("chambers") ;

	for ( const auto& i : chambersList )
	{
		int slot = i.at("slot") ;
		LayerID temp;
		temp.K = slot;
		temp.IncX = 1.;
		temp.IncY = 1.;
		temp.DifX = 0.;

		int difID = i.at("left") ;
		temp.DifY = 0.;

	        if(_mapping.find(difID) != _mapping.end())
		{
			std::cout << "ERROR in geometry : dif " << difID << " of layer " << slot << " already exists" << std::endl ;
			std::terminate() ;
		}
		_mapping[difID] = temp;
        
		difID = i.at("center") ;
		temp.DifY = 32.;

	        if(_mapping.find(difID) != _mapping.end())
		{
			std::cout << "ERROR in geometry : dif " << difID << " of layer " << slot << " already exists" << std::endl ;
			std::terminate() ;
		}
		_mapping[difID] = temp;

		difID = i.at("right") ;
		temp.DifY = 64.;
		
	        if(_mapping.find(difID) != _mapping.end())
		{
			std::cout << "ERROR in geometry : dif " << difID << " of layer " << slot << " already exists" << std::endl ;
			std::terminate() ;
		}
		_mapping[difID] = temp;
}

	file.close() ;
}


//==================================================================================
void TriventProc::processRunHeader( LCRunHeader * runHd ) {

     if(runNumber != -1) {
       _lcWriter->close();
       writeHistograms();
     }
     
     streamlog_out(MESSAGE) << "Trivent processing run: " << runHd->getRunNumber() << std::endl;
     runNumber = runHd->getRunNumber();

     streamlog_out(DEBUG) << "New lcio output" << std::endl;

     _lcWriter->open((_outFileName + std::to_string(runNumber) + ".slcio").c_str(),LCIO::WRITE_NEW);
     _lcWriter->writeRunHeader(runHd);
	 
     streamlog_out(DEBUG) << "Creating new local histograms" << std::endl;

     _outputFile->mkdir(std::to_string(runNumber).c_str());
     _outputFile->cd(std::to_string(runNumber).c_str());

     maxTimeHist = new TH1F("MaxTimeHist", ";MaxTime(BCID);Entries", 10000, 0., 1000000.);
     nHitsReadout = new TH1F("NHitsReadout", ";NHit;Entries", 10000, 0., 50000000.);
     readoutTimeGap = new TH1F("ReadoutTimeGap", ";Time(s);Entries", 10000, 0, 100);
     DIFTimePedestal = new TH1F("DIFTimePedestal", ";Time(BCID);Entries", 1000, 0., 1000.);

     readoutTG_nHitsPrevReadout = new TH2F("ReadoutTG_nHitsPrevReadout", ";Time(s);Time(s)", 100, 0., 10., 1000, 0., 500000.);

     noisePerTimeBin = new TH1F("NoisePerTimeBin", ";MeanNoise(NHit);Entries", 200, 0., 1.2);
     noisePTB_runTime = new TH2F("NoisePTB_RunTime", ";MeanNoise(NHit);Time(s)", 100, 0., 1.2, 3600, 0, 36000);
					   

     firstTimeA = new TH1F("FirstTimeA", ";Time(BCID);Entries", 1000, 0, 10000);
     firstA_nHit = new TH2F("FirstA_nHit", ";Time(BCID);nHit", 1000, 0., 10000., 500, 0., 2000);
     firstA_spillTime = new TH2F("FirstA_spillTime", ";Time(BCID);Time(s)", 1000, 0., 10000., 1000, 0., 10.);
     firstA_spillNr = new TH2F("FirstA_spillNr", ";Time(BCID);Spill", 1000, 0., 10000., 10000, 0., 10000.);
     firstA_Gap = new TH2F("FirstA_Gap", ";Time(BCID);Gap(s)", 1000, 0., 10000., 10000, 0., 100);
     
     firstTimeB = new TH1F("FirstTimeB", ";Time(BCID);Entries", 1000, 0, 10000);
     firstB_nHit = new TH2F("FirstB_nHit", ";Time(BCID);nHit", 1000, 0., 10000., 500, 0., 2000);
     firstB_spillTime = new TH2F("FirstB_spillTime", ";Time(BCID);Time(s)", 1000, 0., 10000., 1000, 0., 10.);
     firstB_Gap = new TH2F("FirstB_Gap", ";Time(BCID);Gap(s)", 1000, 0., 10000., 10000, 0., 100);
     firstB_spillNr = new TH2F("FirstB_spillNr", ";Time(BCID);Spill", 1000, 0., 10000., 10000, 0., 10000.);
     firstB_prevA = new TH2F("FirstB_prevA", ";Time(BCID);Time(BCID)", 1000, 0., 10000., 1000, 0., 10000.);
     firstB_nHitsPrevReadout = new TH2F("FirstB_nHitsPrevReadout", ";Time(BCID);nHits", 1000, 0., 10000., 1000, 0., 500000.);
     
     nHitHist = new TH1F("Nhit",";NHit;Entries",500,0.,2000.);
     nHit_spillTime = new TH2F("NHit_spillTime", ";NHit;Time(s)", 500, 0., 2000., 1000, 0., 10.);
     IHist = new TH1F("I", ";I(pad);Entries", 98, 0., 98.);
     JHist = new TH1F("J", ";J(pad);Entries", 98, 0., 98.);
     KHist = new TH1F("K", ";K(Layer);Entries", 50, 0., 50.);

     evtnum = prevBCID = firstBCID = refBCID = spillNr = 0;
     nInitialHits = nOverlappedHits = nNoisyReadouts = 0;	  
     
     prevA = -1;
     
}

 
void TriventProc::processEvent( LCEvent * evtP ) 
{ 
  streamlog_out(DEBUG) << "Start Trivent process" << std::endl;
  
  if(evtP){ //Check evtP
    for(unsigned int i=0; i < _hcalCollections.size(); i++) { //loop over collection
      try{

	LCCollection* col = evtP->getCollection(_hcalCollections[i].c_str());
	int numElements = col->getNumberOfElements();

	streamlog_out(DEBUG1) << "Number of hits in trigger: " << numElements << std::endl;
	nHitsReadout->Fill(numElements);
	 
	streamlog_out(DEBUG1) << "Trigger " << evtP->getEventNumber() << " will be processed" << std::endl;

	std::vector<int> timeParameters = std::vector<int>(4,0);
	  
	//Loop the hits and find all the DIFs involved, their abs bcid and frame
	std::map<int, std::pair<int,int>> allDIFParameters = {};
	std::map<int, unsigned long long int> linkDIF_BCID = {};
	std::map<int, int> DIF_Frame = {};

	_badMapping.clear();
	for(int j = 0; j < numElements; j++) {

	  RawCalorimeterHit* raw_find_tmax = dynamic_cast<RawCalorimeterHit*>( col->getElementAt(j) );
	  int ID0 = raw_find_tmax->getCellID0();	  
	  int Dif_id = ID0 & 0xFF;
	  int Asic_id = (ID0 & 0xFF00)>>8;
	  int Chan_id = (ID0 & 0x3F0000)>>16;

	  //Dif_id must be in the mapping file, ASIC_ID should be in the range [1,48] and chan_id in [0,63]
	  if(_mapping.find(Dif_id) == _mapping.end() || Asic_id >= 49 || Asic_id <= 0 || Chan_id >= 64 || Chan_id < 0) {
	    _badMapping[std::make_pair(Dif_id,Asic_id)]++; 
	    break;
	  }
	  
	  std::stringstream ss("");
	  ss << "DIF" << Dif_id << "_Triggers";

	  std::vector<int> DIFParameters;
	  col->getParameters().getIntVals(ss.str(), DIFParameters);
	  unsigned long long int NewAbsBCID = DIFParameters[3] + DIFParameters[4]*16777216ULL;
	  
	  DIF_Frame[Dif_id] = DIFParameters[2];

	  allDIFParameters[Dif_id].first = DIFParameters[3];
	  allDIFParameters[Dif_id].second = DIFParameters[4];
	  linkDIF_BCID[Dif_id] = NewAbsBCID;

	}

	displayBadMap();		   

	//Compute the reference values
	int reference_DIF = 0;
	unsigned long long int reference_Max = 0;
	unsigned long long int reference_Min = 0;
	for(auto mapIt = linkDIF_BCID.begin(); mapIt != linkDIF_BCID.end(); mapIt++) {
	  if(reference_DIF == 0) {
	    reference_DIF = mapIt->first;
	    reference_Max = mapIt->second;
	    reference_Min = mapIt->second - DIF_Frame.at(mapIt->first);
	    continue;
	  }
		      
	  if(mapIt->second > reference_Max) {
	    reference_DIF = mapIt->first;
	    reference_Max = mapIt->second;
	  }
		      
	  if(mapIt->second - DIF_Frame.at(mapIt->first) < reference_Min) reference_Min = mapIt->second - DIF_Frame.at(mapIt->first);
	}
		 
	int maxtime = reference_Max - reference_Min;

	if(firstBCID == 0) {
	  firstBCID = linkDIF_BCID[reference_DIF] - maxtime; 
	  refBCID = linkDIF_BCID[reference_DIF] - (unsigned long long int)maxtime;
	}
	else {
	  if((linkDIF_BCID[reference_DIF] - prevBCID)*200*0.000000001 > 5.) {
	    refBCID = linkDIF_BCID[reference_DIF] - (unsigned long long int)maxtime;
	    spillNr++;
	  }
	  readoutTimeGap->Fill((linkDIF_BCID[reference_DIF] - maxtime - prevBCID)*200*0.000000001);
	  readoutTG_nHitsPrevReadout->Fill((linkDIF_BCID[reference_DIF] - maxtime - prevBCID)*200*0.000000001, nHit_Prev);
	}

	
	timeParameters.at(0) = allDIFParameters[reference_DIF].first;
	timeParameters.at(1) = allDIFParameters[reference_DIF].second;
	timeParameters.at(2) = DIF_Frame[reference_DIF];
	
	streamlog_out(DEBUG1) << "\t maxtime == "<< maxtime << std::endl;
	maxTimeHist->Fill(maxtime);

	streamlog_out(DEBUG1) << "Computing time spectrum" << std::endl;

	//Compute the Time Spectrum
	std::map<int,std::vector<RawCalorimeterHit*>> H = {};
		 
	for(int j = 0; j < numElements; j++) {// loop over the hits
	  RawCalorimeterHit *raw_time = dynamic_cast<RawCalorimeterHit*>(col->getElementAt(j));
	  int time = raw_time->getTimeStamp(); 
	  int ID0  =  raw_time->getCellID0();
	  int Dif_id = ID0 & 0xFF;
	  
	  int correctedTime = linkDIF_BCID[Dif_id] - DIF_Frame[Dif_id] - reference_Min + time;
	  
	  H[correctedTime].push_back(raw_time);
	  
	}//end loop over the hits
	
	//Test if it is a noisy readout
	if(numElements > _elec_noise_cut)  {
	  streamlog_out(DEBUG1) << "TRIGGER SKIPED ..." << std::endl;

	  nNoisyReadouts++;
	  
	  if(prevBCID == 0 || (linkDIF_BCID[reference_DIF] - prevBCID)*200*0.000000001 > 5.) {
	    refBCID = linkDIF_BCID[reference_DIF] - (unsigned long long int)maxtime;
	    spillNr++;
	  }
	  prevBCID = linkDIF_BCID[reference_DIF];
	  
	  if(noisyReadout == nullptr) {

	    noisyReadout = new TH1F("NoisyReadout", ";Time(BCID);Entries", 100000, 0., maxtime);
	    
	    for(auto itTime = H.begin(); itTime != H.end(); itTime++) {
	      
	      int bin = (itTime->first/(double)maxtime)*100000;
	      noisyReadout->SetBinContent(bin, noisyReadout->GetBinContent(bin) + itTime->second.size());
	    }
	  }
	  
	  nHit_Prev = numElements;
	  return;
	}
	
	if(fullReadout == nullptr) {
	  fullReadout = new TH1F("FullReadout", ";Time(BCID);Entries", 100000, 0., maxtime);
	  physicalReadout = new TH1F("PhysicalReadout", ";Time(BCID);Entries", 100000, 0., maxtime);

	  for(auto itTime = H.begin(); itTime != H.end(); itTime++) {
			
	    int bin = (itTime->first/(double)maxtime)*100000;
	    
	    fullReadout->SetBinContent(bin, fullReadout->GetBinContent(bin) + itTime->second.size());
	    if(itTime->second.size() >= _noiseCut) physicalReadout->SetBinContent(bin, physicalReadout->GetBinContent(bin) + itTime->second.size());
	  }		      
	}
	
	for(auto difIt = linkDIF_BCID.begin(); difIt != linkDIF_BCID.end(); difIt++) {
	  DIFTimePedestal->Fill(difIt->second - DIF_Frame[difIt->first] - reference_Min);
	}

	streamlog_out(DEBUG1) << "Starting loop" << std::endl;

	std::vector<int> eventBCIDs = {};

	int firstTime_A = 9999;
	int firstTime_B = 9999;
	 
	int bin_c_prev = -2*_timeWin; //maxtime + 2*_timeWin;
	for(auto it = H.begin(); it != H.end(); it++) {
	       
	  bool av_valid = false, ap_valid = false;
	  auto it_av = it;	      
	  auto it_ap = it;
	  it_ap++;
		      
	  if(it_av != H.begin()) {
	    it_av--;
	    if(it->first - it_av->first <= _timeWin) av_valid = true;
	  }	       

	  if(it_ap != H.end() && it_ap->first - it->first <= _timeWin) ap_valid = true;
		      
	  if(it->second.size() >= _noiseCut && (!av_valid || it->second.size() >= it_av->second.size()) && (!ap_valid || it->second.size() > it_ap->second.size()) && it->first > bin_c_prev + _timeWin ) {

	    timeParameters.at(3) = it->first;
	    eventBCIDs.push_back(it->first);
			   
	    int time_A = it->second.at(0)->getCellID1();
	    int time_B = it->first;

	    LCEventImpl*  evt = new LCEventImpl();     // create the event 
	    evt->setRunNumber(evtP->getRunNumber());
	    evt->setTimeStamp(it->first);

	    IMPL::LCCollectionVec* outcolnew = new IMPL::LCCollectionVec(LCIO::CALORIMETERHIT);
	    outcolnew->setFlag(outcolnew->getFlag()|( 1 << LCIO::RCHBIT_LONG));
			   
	    CellIDEncoder<CalorimeterHitImpl> cd( "I:7,J:7,K:10,Dif_id:8,Asic_id:6,Chan_id:7" ,outcolnew);
	    CellIDDecoder<CalorimeterHitImpl> decoder(outcolnew);

	    nHitsMap.clear();

	    for(auto hitIt = it->second.begin(); hitIt != it->second.end(); hitIt++) {
		 processHit((*hitIt),&cd);
	    }

	    auto scanIt_av = it;
	    auto scanIt_ap = it;
	    scanIt_ap++;

	    for(int iScan = 0; iScan < _timeWin; iScan++) {
	      if(scanIt_ap != H.end()) {
		if(scanIt_ap->first - it->first <= _timeWin) {
					  
		  eventBCIDs.push_back(scanIt_ap->first);

		  for(auto hitIt = scanIt_ap->second.begin(); hitIt != scanIt_ap->second.end(); hitIt++) {
		    processHit((*hitIt),&cd);
		  }
					  
		}
		scanIt_ap++;
	      }
				
	      if(scanIt_av != H.begin()) {
		scanIt_av--;
		if(it->first - scanIt_av->first <= _timeWin && scanIt_av->first > bin_c_prev + _timeWin) {
		  eventBCIDs.push_back(scanIt_av->first);

		  for(auto hitIt = scanIt_av->second.begin(); hitIt != scanIt_av->second.end(); hitIt++) {
		    processHit((*hitIt),&cd);
		  }	  
		}
	      }	
	    }

	    _Nhit = 0;
	    for(auto IIt = nHitsMap.begin(); IIt != nHitsMap.end(); IIt++) {
	      for(auto JIt = IIt->second.begin(); JIt != IIt->second.end(); JIt++) {
		for(auto KIt = JIt->second.begin(); KIt != JIt->second.end(); KIt++) {
		  _Nhit++;
		  int I = (int)decoder(KIt->second)["I"];
		  int J = (int)decoder(KIt->second)["J"];
		  int K = (int)decoder(KIt->second)["K"];
		 
		  IHist->Fill(I);
		  JHist->Fill(J);	  
		  KHist->Fill(K);

		  outcolnew->addElement(KIt->second);
		}
	      }
	    }

	    if(time_A < firstTime_A) firstTime_A = time_A; 
	    if(time_B < firstTime_B) firstTime_B = time_B;
        
	    evtTimeInSpill = (int)(linkDIF_BCID[reference_DIF] - refBCID) - maxtime + timeParameters.at(3);
	    timeInSpillInSec = evtTimeInSpill*200*0.000000001;
	    
	    nHitHist->Fill(_Nhit);
	    nHit_spillTime->Fill(_Nhit, timeInSpillInSec);
	    
	    outcolnew->parameters().setValues("TimeParameters",timeParameters);
	    outcolnew->parameters().setValue("TimeInSpill",timeInSpillInSec);	      			   
	    evt->setEventNumber(++evtnum);
	    evt->addCollection(outcolnew, "SDHCAL_RawEvents");
	    _lcWriter->writeEvent(evt);
	    
	    delete evt; evt=NULL;
	    bin_c_prev = it->first;
	  }
	}
		 

	streamlog_out(DEBUG1) << "Filling first time A histograms" << std::endl;
		      
	firstTimeA->Fill(firstTime_A);
	firstA_nHit->Fill(firstTime_A, _Nhit);
	firstA_spillTime->Fill(firstTime_A, (prevBCID - refBCID - firstTime_A)*200*0.000000001);
	firstA_spillNr->Fill(firstTime_A, spillNr);

	streamlog_out(DEBUG1) << "Filling first time B histograms" << std::endl;     
         
	firstTimeB->Fill(firstTime_B);
	firstB_nHit->Fill(firstTime_B, _Nhit);
	firstB_spillTime->Fill(firstTime_B, (prevBCID - refBCID - maxtime + firstTime_B)*200*0.000000001);
	firstB_spillNr->Fill(firstTime_B, spillNr);
		 
	if(prevBCID != 0) {
	  firstA_Gap->Fill(firstTime_A, (linkDIF_BCID[reference_DIF] - maxtime - prevBCID)*200*0.000000001);
	  firstB_Gap->Fill(firstTime_B, (linkDIF_BCID[reference_DIF] - maxtime - prevBCID)*200*0.000000001);
		      
	  firstB_prevA->Fill(firstTime_B, prevA);
	  firstB_nHitsPrevReadout->Fill(firstTime_B, nHit_Prev);
	  
	}
     	 
	streamlog_out(DEBUG1) << "Filling noise histograms" << std::endl;
		      
	int nNoiseBins = 0;
	int nNoiseHits = 0;
	for(int iBCID = 0; iBCID < (H.end()--)->first; iBCID++) {
	  
	  if(std::find(eventBCIDs.begin(), eventBCIDs.end(), iBCID) != eventBCIDs.end()) continue;
	  nNoiseBins++;
	  auto itTime = H.find(iBCID);
	  if(itTime != H.end()) nNoiseHits += itTime->second.size();
	}
	
	noisePerTimeBin->Fill(nNoiseHits/(double)nNoiseBins);
	noisePTB_runTime->Fill(nNoiseHits/(double)nNoiseBins, (refBCID - firstBCID)*200*0.000000001);
		 
	prevBCID = linkDIF_BCID[reference_DIF];	   
	prevA = firstTime_A;
	nHit_Prev = numElements;
		 
      }catch (lcio::DataNotAvailableException zero) { std::cout << "ERROR READING COLLECTION: " << _hcalCollections[i] << std::endl;} 
    }//end loop over collection      
  }//if (evtP)
}

void TriventProc::displayBadMap()
{
  if(_badMapping.size() > 0)
    {
      std::cout << "####-------------------------BAD MAPPING--------------------------####" << std::endl;
      for(auto it = _badMapping.begin(); it != _badMapping.end(); it++)
	{
	  std::cout << "DIF: " << (*it).first.first << " ASIC: " << (*it).first.second << " nHits: " << (*it).second << std::endl;
	}

      assert(false);
    }
}

//==============================================================
void TriventProc::end()
{

     _lcWriter->close();  
     delete _lcWriter;
     
     writeHistograms();
   
     _outputFile->Close();
     delete _outputFile;


}

void TriventProc::processHit(RawCalorimeterHit* a_DHcalhit, CellIDEncoder<CalorimeterHitImpl>* cd) {

     nInitialHits++;
  
     int time = a_DHcalhit->getTimeStamp();
     
     int ID0  = a_DHcalhit->getCellID0();
     int Dif_id = ID0 & 0xFF;

     float pos[3]; 
     int Asic_id;
     int Chan_id;
        	  
     Int_t I,J,K;
	
     Asic_id=(ID0 & 0xFF00)>>8; // Asic id
     Chan_id=(ID0 & 0x3F0000)>>16; // Channel id
     
     // ============= mapping by hand ========
		  
     double DifY,DifZ;

     DifY = _mapping.at(Dif_id).DifY;
     DifZ = _mapping.at(Dif_id).K;

     I = (32-(MapJLargeHR2[Chan_id]+AsicShiftJ[Asic_id])) + int(DifY) - 1;
     J = (1+MapILargeHR2[Chan_id]+AsicShiftI[Asic_id]) - 1;
     K = int(DifZ);

     int tmpThreshold = int(a_DHcalhit->getAmplitude()&3);
     if(tmpThreshold == 1) tmpThreshold = 2;
     else if(tmpThreshold == 2) tmpThreshold = 1;
        
     if(nHitsMap.find(I) != nHitsMap.end() && nHitsMap[I].find(J) != nHitsMap[I].end() && nHitsMap[I][J].find(K) != nHitsMap[I][J].end()) {
       nOverlappedHits++;
       if((int)nHitsMap.at(I).at(J).at(K)->getEnergy() >= tmpThreshold) return;
     }
     
     //Position is the center of the pad in mm
     
     pos[0] = (I+0.5)*10.421;
     pos[1] = (J+0.5)*10.421;
     pos[2] = K*_layer_gap*10;		  

     CalorimeterHitImpl* caloHit = new CalorimeterHitImpl();		  
     caloHit->setTime(float(time));
     caloHit->setEnergy(float(tmpThreshold));
     caloHit->setPosition(pos);

     (*cd)["I"] = I ;
     (*cd)["J"] = J ;
     (*cd)["K"] = K ;
	
     (*cd)["Dif_id"]  =  Dif_id ;
     (*cd)["Asic_id"] =  Asic_id;	
     (*cd)["Chan_id"] =  Chan_id;
		  
     (*cd).setCellID(caloHit);
       
     nHitsMap[I][J][K] = caloHit;
}
 

void TriventProc::writeHistograms() {

  _outputFile->cd(std::to_string(runNumber).c_str());

  if(evtnum > 0) {
       TPaveText stats(0.05, 0.1, 0.95, 0.9);
       stats.AddText(("Percentage of noisy readouts:" + std::to_string(nNoisyReadouts*100/(float)evtnum) + "%").c_str());
       stats.AddText(("Percentage of overlapped hits: " + std::to_string(nNoisyReadouts*100/(float)nInitialHits) + "%").c_str());
       stats.Write();
  }
  
  if(noisyReadout != nullptr) {
       noisyReadout->Write();
       delete noisyReadout;
       noisyReadout = nullptr;
  }

  nHitsReadout->Write();
  maxTimeHist->Write();
  
  noisePerTimeBin->Write();
  noisePTB_runTime->Write();

  DIFTimePedestal->Write();

  readoutTimeGap->Write();
  readoutTG_nHitsPrevReadout->Write();
  
  firstTimeA->Write();
  firstA_nHit->Write();
  firstA_spillTime->Write();
  firstA_spillNr->Write();
  firstA_Gap->Write();
  
  firstTimeB->Write();
  firstB_nHit->Write();
  firstB_spillTime->Write();
  firstB_spillNr->Write();
  firstB_Gap->Write();
  firstB_prevA->Write();
  firstB_nHitsPrevReadout->Write();
  
  nHitHist->Write();
  nHit_spillTime->Write();
  
  IHist->Write();
  JHist->Write();
  KHist->Write();
  
  if(fullReadout != nullptr) {
       
       TCanvas* canvas = new TCanvas("SingleReadoutCanvas");
       fullReadout->Draw();
       physicalReadout->SetLineColor(3);
       physicalReadout->Draw("SAME");
       TLine* hLine = new TLine(fullReadout->GetXaxis()->GetXmin(), 7, fullReadout->GetXaxis()->GetXmax(), 7);
       hLine->SetLineStyle(9);
       hLine->SetLineColor(2);
       hLine->Draw();
       
       canvas->Write();
       delete canvas;

       physicalReadout = nullptr;
       fullReadout = nullptr;
  }

  delete nHitHist;
  delete nHit_spillTime;
  delete noisePerTimeBin;
  delete noisePTB_runTime;
  delete DIFTimePedestal;
  delete nHitsReadout;
  delete maxTimeHist; 
  delete readoutTimeGap;
  delete readoutTG_nHitsPrevReadout;
  delete firstTimeA;
  delete firstA_nHit;
  delete firstA_spillTime;
  delete firstA_spillNr;
  delete firstA_Gap;
  delete firstTimeB;
  delete firstB_nHit;
  delete firstB_spillTime;
  delete firstB_spillNr;
  delete firstB_Gap;
  delete firstB_prevA;
  delete firstB_nHitsPrevReadout;
  delete IHist;
  delete JHist;
  delete KHist;
  
}
