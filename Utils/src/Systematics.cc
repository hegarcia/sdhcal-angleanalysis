#include "Systematics.h"

#include <iostream>


SystematicsTool::SystematicsTool(std::string systematicsFileName) {

  systematicsFile.open(systematicsFileName.c_str(), std::ofstream::trunc);

}


bool SystematicsTool::AddCut(std::string cutName) {

  if(cuts.find(cutName) != cuts.end()) {
    std::cout << "Cut: " << cutName << " already exists in the sytematics cut list." << std::endl;
    return false;
  }

  std::cout << "Adding cut: " << cutName  << std::endl;
  cuts.emplace(cutName,Cut());  
  return true;
  
}


void SystematicsTool::FillCut(std::string cutName, int result, int upResult, int downResult) {

  if(cuts.find(cutName) == cuts.end()) {
    std::cout << "No cut: " << cutName << " in systematics." << std::endl;
    return;
  }

  cuts.at(cutName).cutResult.push_back(result);
  cuts.at(cutName).cutResultUp.push_back(upResult);
  cuts.at(cutName).cutResultDown.push_back(downResult);

  cuts.at(cutName).nElements++;
  
}

void SystematicsTool::WriteSystematics() {

  for(auto cutIt = cuts.begin(); cutIt != cuts.end(); cutIt++) {

    systematicsFile << "NEW Cut " << cutIt->first << std::endl;
    systematicsFile << "CutResult CutResultUp CutResultDown" << std::endl;

    for(int i = 0; i < cutIt->second.nElements; i++) {
      systematicsFile << cutIt->second.cutResult.at(i) << " " << cutIt->second.cutResultUp.at(i) << " " << cutIt->second.cutResultDown.at(i) << std::endl; 
    }
     
  }


}
