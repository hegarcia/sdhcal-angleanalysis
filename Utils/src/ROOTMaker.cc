#include "ROOTMaker.h"

#include <EVENT/LCCollection.h>
#include <EVENT/Cluster.h>
#include <IMPL/CalorimeterHitImpl.h>
#include <UTIL/CellIDDecoder.h>

#include <marlin/Global.h>

#include <TStyle.h>
#include <TCanvas.h>
#include <TLine.h>
#include <TPaveText.h>

#include <variant>
#include <cstring>
#include <cassert>

ROOTMakerProc a_ROOTMakerProc;

//=========================================================
ROOTMakerProc::ROOTMakerProc()
  :Processor("ROOTMakerProc")
{
  
  std::cout << "ROOTMaker Begin" << std::endl;
  
  //Collections to read 
  registerInputCollections( LCIO::CALORIMETERHIT, 
			    "HCAL_Collections",  
			    "HCAL Hit Collections Names",  
			    _hcalCollections, 
			    _hcalCollections); 
  
  //Output Tree name
  _treeName = "SDHCAL";
  registerProcessorParameter("Out_Tree_Name" ,
                             "Output Tree name",
                             _treeName,
			     _treeName);

  //Output ROOT File name
  _rootName = "ROOT";
  registerProcessorParameter("Out_ROOT_Name",
                             "Output ROOT File name",
                             _rootName,
			     _rootName);


  //Collection for display
  registerProcessorParameter("CollToDisplay",
                             "Collection to search events to display",
                             _collectionToDisplay,
			     _collectionToDisplay);


  //Do you want clusters?
  registerProcessorParameter("IncludeClusters",
                             "Include the cluster collection if it exists",
                             _includeClusters,
			     _includeClusters);


  //Clusters collection name.
  registerProcessorParameter("ClustersCollName",
                             "Name of the cluster collections associated the hcalCollection",
                             _clustersCollections,
			     _clustersCollections);

  
  //Display events
  registerProcessorParameter("EvtsToDisplay",
                             "Events to add to the viewer",
                             evtsToDisplay,
			     evtsToDisplay);
  

  //Variables for selection
  registerProcessorParameter("SelectVars",
                             "Variables for selections for events to display",
                             variableForSelection,
			     variableForSelection);


  //Skip events
  registerProcessorParameter("Skip",
                             "Skip the first events",
                             skip,
			     0);


  //Selections
  registerProcessorParameter("Select",
                             "Selections for events to display",
                             selections,
			     selections);

  //Selection Cuts
  registerProcessorParameter("SelectCuts",
                             "Selection cuts for events to display",
                             cuts,
			     cuts);


  //Stop the processing if found an event to display
  registerProcessorParameter("StopToDisplay",
                             "If true, when an event for the display is found all processors are stopped",
                             stopToDisplay,
			     stopToDisplay);

  _kEncoding = "K";
  registerProcessorParameter("K_Encoding",
                             "Encoding of K in the hits",
                             _kEncoding,
			     _kEncoding);

  //Noisy ASIC Cut
  _noisyASICCut = 65;
  registerProcessorParameter("ASIC_Cut",
                             "NHits in one ASIC to be noisy",
                             _noisyASICCut,
			     _noisyASICCut);
  
  //Noisy DIF Cut
  _noisyDIFCut = 3073;
  registerProcessorParameter("DIF_Cut",
                             "NHits in one DIF to be noisy",
                             _noisyDIFCut,
			     _noisyDIFCut);


  //The input file does not have header
  registerProcessorParameter("Is_Headerless",
                             "Does the file have header?",
                             _headerless,
			     false);
  
} 

void ROOTMakerProc::init() {

  std::cout << "Creating the output file, tree and intializing the branches."<< std::endl;

  gStyle->SetOptStat(0);

  marlin::Global::parameters->getFloatVals("Energies",runEnergies);
  
  evtCount = std::vector<int>(_hcalCollections.size(),0);

  printParameters();
  
}


void ROOTMakerProc::Clear()
{

  streamlog_out(DEBUG) << "Clearing variables" << std::endl;

  nLayers = 0;
  N1 = 0;
  N2 = 0;
  N3 = 0;
  vI.clear();
  vJ.clear();
  vLayer.clear();
  nHitsDIF.clear();
  nHitsASIC.clear();
  vX.clear();
  vY.clear();
  vZ.clear();
  vThreshold.clear();
  vBCID.clear();
  nClusters = 0;
  vCenter_I.clear();
  vCenter_J.clear();
  vCenter_K.clear();
  nHitsMap.clear();

  noisyASIC = false;
  noisyDIF = false;

}


void ROOTMakerProc::DisplayEvt(int collIndex)
{

     streamlog_out(DEBUG) << "Displaying event" << std::endl;

     XY->Reset();
     XZ->Reset();
     ZY->Reset();

     streamlog_out(DEBUG) << "Creating graphs and histograms" << std::endl;

     XZT1 = new TGraph(); 
     XZT1->SetName("Hits T1");
     XZT1->GetXaxis()->SetTitle("X(cm)");
     XZT1->GetYaxis()->SetTitle("Z(cm)");
     XZT1->GetXaxis()->SetLabelSize(0.04);
     XZT1->GetYaxis()->SetLabelSize(0.04);
     XZT1->GetXaxis()->SetTitleSize(0.05);
     XZT1->GetYaxis()->SetTitleSize(0.05);
     XZT1->GetYaxis()->SetTitleOffset(1.);
     XZT1->GetXaxis()->SetLabelFont(12);
     XZT1->GetXaxis()->SetTitleFont(22);
     XZT1->GetYaxis()->SetLabelFont(12);
     XZT1->GetYaxis()->SetTitleFont(22);
     XZT1->SetMaximum(140);
     XZT1->SetMinimum(0.);
     XZT1->SetMarkerStyle(20);
     XZT1->SetMarkerColor(3);
     XZT2 = new TGraph(); 
     XZT2->SetName("Hits T2");
     XZT2->SetMarkerStyle(21);
     XZT2->SetMarkerColor(4);
     XZT3 = new TGraph();
     XZT3->SetName("Hits T3");
     XZT3->SetMarkerColor(2);
     XZT3->SetMarkerStyle(22);     

     YZT1 = new TGraph(); 
     YZT1->SetName("Hits T1");
     YZT1->GetXaxis()->SetTitle("Z(cm)");
     YZT1->GetYaxis()->SetTitle("Y(cm)");
     YZT1->GetXaxis()->SetLabelSize(0.04);
     YZT1->GetYaxis()->SetLabelSize(0.04);
     YZT1->GetXaxis()->SetTitleSize(0.05);
     YZT1->GetYaxis()->SetTitleSize(0.05);
     YZT1->GetYaxis()->SetTitleOffset(1.);
     YZT1->GetXaxis()->SetLabelFont(12);
     YZT1->GetXaxis()->SetTitleFont(22);
     YZT1->GetYaxis()->SetLabelFont(12);
     YZT1->GetYaxis()->SetTitleFont(22);
     YZT1->SetMaximum(100);
     YZT1->SetMinimum(0.);
     YZT1->SetMarkerStyle(20);
     YZT1->SetMarkerColor(3);
     YZT2 = new TGraph(); 
     YZT2->SetName("Hits T2");
     YZT2->SetMarkerColor(4);
     YZT2->SetMarkerStyle(21);
     YZT3 = new TGraph();
     YZT3->SetName("Hits T3");
     YZT3->SetMarkerColor(2);
     YZT3->SetMarkerStyle(22);


     XYT1 = new TGraph(); 
     XYT1->SetName("Hits T1");
     XYT1->GetXaxis()->SetTitle("X(cm)");
     XYT1->GetYaxis()->SetTitle("Y(cm)");
     XYT1->GetXaxis()->SetLabelSize(0.04);
     XYT1->GetYaxis()->SetLabelSize(0.04);
     XYT1->GetXaxis()->SetTitleSize(0.05);
     XYT1->GetYaxis()->SetTitleSize(0.05);
     XYT1->GetYaxis()->SetTitleOffset(1.);
     XYT1->GetXaxis()->SetLabelFont(12);
     XYT1->GetXaxis()->SetTitleFont(22);
     XYT1->GetYaxis()->SetLabelFont(12);
     XYT1->GetYaxis()->SetTitleFont(22);
     XYT1->SetMaximum(100);
     XYT1->SetMinimum(0.);
     XYT1->SetMarkerStyle(20);
     XYT1->SetMarkerColor(3);
     XYT2 = new TGraph(); 
     XZT2->SetName("Hits T2");
     XYT2->SetMarkerColor(4);
     XYT2->SetMarkerStyle(21);
     XYT3 = new TGraph();
     XZT3->SetName("Hits T3");
     XYT3->SetMarkerColor(2);
     XYT3->SetMarkerStyle(22);


     int counterT1 = 0;
     int counterT2 = 0;
     int counterT3 = 0;

     streamlog_out(DEBUG) << "Filling graphs" << std::endl;

  for(int iHit = 0; iHit < vI.size(); iHit++)
    {
	 switch(vThreshold.at(iHit)) {
	 case 1: XZT1->SetPoint(counterT1, (double)vI.at(iHit)*1.04, (double)vLayer.at(iHit)*2.8); YZT1->SetPoint(counterT1, (double)vLayer.at(iHit)*2.8, (double)vJ.at(iHit)*1.04); XYT1->SetPoint(counterT1, (double)vI.at(iHit)*1.04, (double)vJ.at(iHit)*1.04); counterT1++; break;
	 case 2: XZT2->SetPoint(counterT2, (double)vI.at(iHit)*1.04, (double)vLayer.at(iHit)*2.8); YZT2->SetPoint(counterT2, (double)vLayer.at(iHit)*2.8, (double)vJ.at(iHit)*1.04); XYT2->SetPoint(counterT2, (double)vI.at(iHit)*1.04, (double)vJ.at(iHit)*1.04); counterT2++; break;
	 case 3: XZT3->SetPoint(counterT3, (double)vI.at(iHit)*1.04, (double)vLayer.at(iHit)*2.8); YZT3->SetPoint(counterT3, (double)vLayer.at(iHit)*2.8, (double)vJ.at(iHit)*1.04); XYT3->SetPoint(counterT3, (double)vI.at(iHit)*1.04, (double)vJ.at(iHit)*1.04); counterT3++; break;
	 }

      XY->Fill(vI.at(iHit),vJ.at(iHit));
      XZ->Fill(vI.at(iHit),vLayer.at(iHit));
      ZY->Fill(vLayer.at(iHit),vJ.at(iHit));

    }

  _outputFile->cd();

  streamlog_out(DEBUG) << "Creating Pave Text" << std::endl;

  double hasTrack = false;
  double hasRMS = false;

  if(trkPars.size() > 0) hasTrack = true;
  if(rms.size() > 0) hasRMS = true;

  TPaveText* info = new TPaveText(0.05,0.1,0.95,0.8);
  info->AddText(("NLayers: " + std::to_string(nLayers)).c_str());
  info->AddText(("NHits: " + std::to_string(nhit)).c_str());
  info->AddText(("N1: " + std::to_string(N1)).c_str());
  info->AddText(("N2: " + std::to_string(N2)).c_str());
  info->AddText(("N3: " + std::to_string(N3)).c_str());
  info->AddText(("Density: " + std::to_string(nhit/(double)nLayers)).c_str());
  info->AddText(("SecondMax: " + std::to_string(secondMax)).c_str());
  if(hasTrack) {
       info->AddText(("Slope X: " + std::to_string((double)trkPars.at(1))).c_str());
       info->AddText(("Slope Y: " + std::to_string((double)trkPars.at(3))).c_str());
  }
  info->AddText(("ShowerStart: " + std::to_string(showerStart)).c_str());
  if(hasRMS) {
       info->AddText(("RMS_X: " + std::to_string(rms.at(0))).c_str());
       info->AddText(("RMS_Y: " + std::to_string(rms.at(1))).c_str()); 
  }
  
  nDisplays++;

  streamlog_out(DEBUG) << "Drawing display canvas" << std::endl;

  TCanvas* canvas = new TCanvas(("Display_" + std::to_string(E) + "_" + std::to_string(nDisplays)).c_str(), "", 1920, 1080);
  canvas->Divide(2,2);
  
  canvas->cd(2);
  info->Draw();

  canvas->cd(3);
  XY->DrawCopy("COLZ");
  
  canvas->cd(4);
  ZY->DrawCopy("COLZ");
  
  canvas->cd(1);
  XZ->DrawCopy("COLZ");
  
  canvas->Write();
  
  delete canvas;

  TLine* XZFit;
  TLine* YZFit;
  TPaveText* xSlopeText;
  TPaveText* ySlopeText;

  if(hasTrack) {

       XZFit = new TLine(0, trkPars[0]/10.0, 48*2.8, trkPars[1]*48*2.8 + trkPars[0]/10.0);
       XZFit->SetLineWidth(2.0);
       XZFit->SetLineColor(2);
       
       YZFit = new TLine(0, trkPars[2]/10.0, 48*2.8, trkPars[3]*48*2.8 + trkPars[2]/10.0);
       YZFit->SetLineWidth(2.0);
       YZFit->SetLineColor(2);

       xSlopeText = new TPaveText(85, 85, 125, 95);
       xSlopeText->AddText(("SlopeX: " + std::to_string(trkPars[1])).c_str());

       ySlopeText = new TPaveText(85, 85, 125, 95);
       ySlopeText->AddText(("SlopeY: " + std::to_string(trkPars[3])).c_str());

  }

  TCanvas* canvasGraph = new TCanvas(("GraphDisplay_" + std::to_string(E) + "_" + std::to_string(nDisplays)).c_str(), "", 1920, 1080);
  canvasGraph->Divide(2,2);

  canvasGraph->cd(1);
  XZT1->Draw("AP");
  XZT1->GetXaxis()->SetLimits(0,100);
  XZT2->Draw("Psame");
  XZT3->Draw("Psame");
  if(hasTrack) {
       XZFit->Draw("same");
       xSlopeText->Draw("same");
  }

  canvasGraph->cd(3);
  XYT1->Draw("AP");
  XYT1->GetXaxis()->SetLimits(0,100);
  XYT2->Draw("Psame");
  XYT3->Draw("Psame");
  if(hasTrack) {
       YZFit->Draw("same");
       ySlopeText->Draw("same");
  }

  canvasGraph->cd(4);
  YZT1->Draw("AP");
  YZT1->GetXaxis()->SetLimits(0,140);
  YZT2->Draw("Psame");
  YZT3->Draw("Psame");
  if(hasTrack) {
       YZFit->Draw("same");
       ySlopeText->Draw("same");
  }

  canvasGraph->Write();

  streamlog_out(DEBUG) << "Deleting and ending event display" << std::endl;


  delete XZT1;
  delete XZT2;
  delete XZT3;
  
  delete YZT1;
  delete YZT2;
  delete YZT3;

  delete XYT1;
  delete XYT2;
  delete XYT3;

  delete canvasGraph;

}


bool ROOTMakerProc::Select(int variableID, int selectionRule, std::vector<float> cuts)
{

     streamlog_out(DEBUG3) << "VariableID: " << variableID << std::endl;
     streamlog_out(DEBUG3) << "selectionRule: " << selectionRule << std::endl;
     streamlog_out(DEBUG3) << "Cuts: " << cuts.at(0) << " " << cuts.at(1) << std::endl;
     
     long long int variableLLI;
     double variableD;
     bool variableB;
     int type;
     
     switch(variableID)
	  {
	  case 0: variableLLI = absBCID; type = 0; break;
	  case 1: variableLLI = BCID; type = 0; break;
	  case 2: variableLLI = nhit; type = 0; break;
	  case 3: variableLLI = nLayers; type = 0; break;
	  case 4: variableLLI = N1; type = 0; break;
	  case 5: variableLLI = N2; type = 0; break;
	  case 6: variableLLI = N3; type = 0; break;
	  case 7: variableLLI = secondMax; type = 0; break;
	  case 8: variableD = nhit/(double)nLayers; type = 1;  break;
	  case 9: variableD = (double)trkPars.at(1); type = 1; break;
	  case 10: variableD = (double)trkPars.at(3); type = 1; break;
	  case 11: variableLLI = nLayers - showerStart; type = 0; break;
	  case 12: variableB = noisyASIC; type = 2; break;
	  case 13: variableB = noisyDIF; type = 2; break;     
    default: std::cout << "Wrong variable ID for the selection." << std::endl; return false;
    }

     switch(type) {
     case 0: streamlog_out(DEBUG3) << "Variable value: " << variableLLI  << std::endl; break;
     case 1: streamlog_out(DEBUG3) << "Variable value: " << variableD  << std::endl; break;
     case 2: streamlog_out(DEBUG3) << "Variable value: " << variableB  << std::endl; break;
     }

  if(cuts.at(1) == -1)
    {
      switch(selectionRule)
	{
	case 0:
	     if(type == 0) {
		  if(variableLLI >= cuts.at(0)) return true;
	     }
	     else if(type == 1) { if(variableD >= cuts.at(0)) return true; }
	     else if(type == 2) { if(variableB) return true; }
	     return false;
	case 1:
	     if(type == 0) {
		  if(variableLLI > cuts.at(0)) return true;
	     }
	     else if(type == 1) { if(variableD > cuts.at(0)) return true; }
	     else if(type == 2) { if(!variableB) return true; }
	     return false;
	case 2:
	     if(type == 0) {
		  if(variableLLI == cuts.at(0)) return true;
	     }
	     else { if(variableD == cuts.at(0)) return true; }
	     return false;
	case 3:
	     if(type == 0) {
		  if(variableLLI != cuts.at(0)) return true;
	     }
	     else { if(variableD != cuts.at(0)) return true; }
	     return false;
	case 4:
	     if(type == 0) {
		  if(variableLLI < cuts.at(0)) return true;
	     }
	     else { if(variableD < cuts.at(0)) return true; }
	     return false;
	case 5:
	     if(type == 0) {
		  if(variableLLI <= cuts.at(0)) return true;
	     }
	     else { if(variableD <= cuts.at(0)) return true; }
	     return false;
	default:
	     std::cout << "Wrong operation ID for the selection." << std::endl; return false; 
	}
    }

  switch(selectionRule)
    {
    case 0:
	 if(type == 0) { if(cuts.at(0) <= variableLLI && variableLLI <= cuts.at(1)) return true; }
	 else { if(cuts.at(0) <= variableD && variableD <= cuts.at(1)) return true; }
	 return false;
    case 1:
	 if(type == 0) { if(cuts.at(0) < variableLLI && variableLLI <= cuts.at(1)) return true; }
	 else { if(cuts.at(0) < variableD && variableD <= cuts.at(1)) return true; }
	 return false;
    case 2:
	 if(type == 0) { if(cuts.at(0) < variableLLI && variableLLI < cuts.at(1)) return true; }
	 else { if(cuts.at(0) < variableD && variableD < cuts.at(1)) return true; }
	 return false;
    case 3:
	 if(type == 0) { if(cuts.at(0) <= variableLLI && variableLLI < cuts.at(1)) return true; }
	 else { if(cuts.at(0) <= variableD && variableD < cuts.at(1)) return true; }
	 return false;
    default:
	 std::cout << "Wrong operation ID for the selection." << std::endl; return false; 
    }
  
}


void ROOTMakerProc::processRunHeader(LCRunHeader* runP) {

  streamlog_out(MESSAGE) << "ROOTMaker Run Header" << std::endl;

  int headerRun;
  if(runP != 0) { headerRun = runP->getRunNumber(); }
  else headerRun = -1;

  if(runIndex != 0 && runEnergies.at(runIndex - 1) == runEnergies.at(runIndex)) { runIndex++; return; }

  if(_outputFile != nullptr) {
      
       E = 0; //int(runEnergies.at(runIndex - 1));

       _outputFile->cd();
       _outputTree->Write();

       delete _outputTree;
 
       nHit_H->Write();
       delete nHit_H;

       TCanvas* thresholdsCanvas = new TCanvas(("Thresholds_" + std::to_string(E)).c_str());
       nHit_N3->Draw();
       nHit_N2->Draw("same");
       nHit_N1->Draw("same");

       thresholdsCanvas->Write();
       delete thresholdsCanvas;

       _outputFile->Close();
       delete _outputFile;
  }

  E = 0;//int(runEnergies.at(runIndex));

  char tmp[50];
  corrFile.open(("Results/0Deg/SatCorr_" + std::to_string(headerRun) + "GeV_TimeA.corr").c_str());

  if(corrFile.is_open()) { corrFile >> nStats >> tmp >> tmp >> tmp >> tmp >> tmp >> tmp >> corrN1 >> errN1 >> tmp >> tmp >> tmp >> corrN2 >> errN2 >> tmp >> tmp >> tmp >> corrN3 >> errN3 >> tmp >> tmp; 
  corrFile.close(); }
  else {
       nStats = 0;
       corrN1 = 0;
       corrN2 = 0; 
       corrN3 = 0;
  }

  //Output File 
  _outputFile = new TFile((_rootName + std::to_string(E) + "GeV.root").c_str(),"RECREATE");
  
  //Output 
  _outputTree = new TTree(_treeName.c_str(),_treeName.c_str());
  _outputTree->SetAutoSave(32*1024*1024);
      
  //Initializing branches
  _outputTree->Branch("EvtNbr",&evtnum,"EvtNr/I");
  _outputTree->Branch("ParentName", "TObjString",&parent);
  _outputTree->Branch("AbsoluteBCID", &absBCID, "AbsBCID/l"); 
  _outputTree->Branch("BCID", &BCID, "BCID/I"); 
  _outputTree->Branch("E",&E, "E/I");
  _outputTree->Branch("NHit",&nhit_corr, "nHit/I");
  _outputTree->Branch("NLayer",&nLayers, "NLayer/I"); 
  _outputTree->Branch("N1",&N1, "N1/I"); 
  _outputTree->Branch("N2",&N2, "N2/I"); 
  _outputTree->Branch("N3",&N3, "N3/I");
  _outputTree->Branch("I","std::vector<int>",&vI);
  _outputTree->Branch("J","std::vector<int>",&vJ);
  _outputTree->Branch("Layer","std::vector<int>",&vLayer);
  _outputTree->Branch("nHitASIC","std::vector<int>",&nHitsASIC);
  _outputTree->Branch("nHitDIF","std::vector<int>",&nHitsDIF);
  _outputTree->Branch("X","std::vector<float>",&vX);
  _outputTree->Branch("Y","std::vector<float>",&vY);
  _outputTree->Branch("Z","std::vector<float>",&vZ);
  _outputTree->Branch("Thr","std::vector<int>",&vThreshold);  
  _outputTree->Branch("BCID","std::vector<unsigned int>", &vBCID); 
  
  if(_includeClusters)
       {
	    _outputTree->Branch("NClusters",&nClusters,"nClusters/I");
	    _outputTree->Branch("ClusterI","std::vector<int>", &vCenter_I); 
	    _outputTree->Branch("ClusterJ","std::vector<int>", &vCenter_J); 
	    _outputTree->Branch("ClusterK","std::vector<int>", &vCenter_K); 
       }

  nHit_H = new TH1F("NHit", "NHit", 400, 0., 2000.);
  nHit_H->SetTitle(";NHit;Entries");
  nHit_H->SetLabelSize(0.05, "X");
  nHit_H->SetLabelSize(0.05, "Y");
  nHit_H->SetTitleSize(0.05, "X");
  nHit_H->SetTitleSize(0.05, "Y");
  nHit_H->SetTitleOffset(0.75, "Y");

  nHit_N1 = new TH1F("NHit1", "NHit1", 400, 0., 2000.);
  nHit_N1->SetTitle(";NHit;Entries");
  nHit_N1->SetLineColor(4);
  nHit_N1->SetLabelSize(0.05, "X");
  nHit_N1->SetLabelSize(0.05, "Y");
  nHit_N1->SetTitleSize(0.05, "X");
  nHit_N1->SetTitleSize(0.05, "Y");
  nHit_N1->SetTitleOffset(0.75, "Y");

  nHit_N2 = new TH1F("NHit2", "NHit2", 400, 0., 2000.);
  nHit_N2->SetTitle(";NHit;Entries");
  nHit_N2->SetLineColor(3);

  nHit_N3 = new TH1F("NHit3", "NHit3", 400, 0., 2000.);
  nHit_N3->SetTitle(";NHit;Entries");
  nHit_N3->SetLineColor(2);
  
  XY = new TH2I("XY", "", 96, 1., 97., 96, 1., 97.);
  XY->SetTitle(";I(pad);J(pad)"); 
  ZY = new TH2I("ZY", "", 49, 0., 49., 96, 1., 97.);
  ZY->SetTitle(";K(layer);J(pad)");
  XZ = new TH2I("XZ", "", 96, 1., 97., 49, 0., 49.);
  XZ->SetTitle(";I(pad);K(layer)");

  runIndex++;

  histsCreated = true;
  
}


void ROOTMakerProc::processGeneralCollection(LCCollection* col, int collIndex, int startIndex, LCEvent* evtPt)
{

  streamlog_out(DEBUG) << "Processing General Collection" << std::endl;
     
  timeInSpill = col->getParameters().getFloatVal("TimeInSpill");

  trkPars.clear();
  col->getParameters().getFloatVals("TrackParameters", trkPars);
  col->getParameters().getFloatVals("RMS",rms);

  secondMax = col->getParameters().getIntVal("SecondMax");
  showerStart = col->getParameters().getIntVal("ShowerStart");

  CellIDDecoder<CalorimeterHitImpl> decoder("I:7,J:7,K:10,Dif_id:8,Asic_id:6,Chan_id:7");

  std::vector<int> kDistribution(49, 0);

  streamlog_out(DEBUG5) << "Starting hits loop" << std::endl;
  for(int iHit = startIndex; iHit < startIndex + nhit; iHit++) { //loop over hits
		    
    CalorimeterHitImpl* hit = dynamic_cast<CalorimeterHitImpl*>(col->getElementAt(iHit));

    streamlog_out(DEBUG1) << "Gettting indexes" << std::endl;
    vI.push_back(decoder(hit)["I"]);
    vJ.push_back(decoder(hit)["J"]);
    vLayer.push_back(decoder(hit)[_kEncoding]);
    kDistribution.at(decoder(hit)[_kEncoding])++;

    int ASIC = ((int)decoder(hit)["I"])/8 + ((int)decoder(hit)["J"])/8*12;
    int DIF = ((int)decoder(hit)["I"])/32 + ((int)decoder(hit)[_kEncoding])*3;

    /*if(nhit == 1231) {
    std::cout << "I = " << decoder(hit)["I"] << " J = " <<  decoder(hit)["J"] << " K = " << decoder(hit)[_kEncoding] << std::endl;
    std::cout << "ASIC = " << ASIC << " DIF = " << DIF << std::endl;
    }*/

    nHitsMap[DIF][ASIC]++;

    streamlog_out(DEBUG1) << "Gettting position" << std::endl;

    const float* position = hit->getPosition();
    vX.push_back(position[0]);
    vY.push_back(position[1]);
    vZ.push_back(position[2]);
    
    streamlog_out(DEBUG1) << "Gettting threshold" << std::endl;
    int threshold = hit->getEnergy();
    if(threshold == 1) N1++; 
    else if(threshold == 2) N2++;
    else N3++;
    		  
    vThreshold.push_back(threshold);

    streamlog_out(DEBUG1) << "Gettting BCID" << std::endl;
    vBCID.push_back(hit->getTime());
    
  } //end loop over hits

  streamlog_out(DEBUG) << "Asserting number of hits" << std::endl;
  assert(N1 + N2 + N3 == nhit);

  for(auto itDIF = nHitsMap.begin(); itDIF != nHitsMap.end(); itDIF++) {
       int nHDIF = 0;
       for(auto itASIC = itDIF->second.begin(); itASIC != itDIF->second.end(); itASIC++) {
	     streamlog_out(DEBUG1) << "NHitASIC = " << itASIC->second << std::endl;
	     nHitsASIC.push_back(itASIC->second);
	     if(itASIC->second >= _noisyASICCut) noisyASIC = true;
	     nHDIF += itASIC->second;
       }
       streamlog_out(DEBUG1) << "NHitDIF = " << nHDIF << std::endl;
       nHitsDIF.push_back(nHDIF);
       if(nHDIF >= _noisyDIFCut) noisyDIF = true;
  }

  //if(corrN1 + errN1 < 0) N1 -= corrN1*timeInSpill;
  //if(corrN2 + errN2 < 0) N2 -= corrN2*timeInSpill;
  //if(corrN3 + errN3 < 0) N3 -= corrN3*timeInSpill;

  //N1 *= 0.88;
  //N2 *= 0.88;
  //N3 *= 0.88;

  nhit_corr = N1 + N2 + N3;

  nHit_H->Fill(nhit);
  nHit_N1->Fill(N1);
  nHit_N2->Fill(N2);
  nHit_N3->Fill(N3);

  streamlog_out(DEBUG) << "Displaying eventss" << std::endl;

  bool eventsDisplayed = false;
  for(int iLayer = 0; iLayer < kDistribution.size(); iLayer++) { if(kDistribution.at(iLayer) > 0) nLayers++; }
    
  if(_collectionToDisplay.size() == 0 || std::find(_collectionToDisplay.begin(), _collectionToDisplay.end(), _hcalCollections[collIndex]) != _collectionToDisplay.end())
    {
      
	 bool selected = true;
	 int selectIndex = 0;
	 streamlog_out(DEBUG) << "Selection rules" << std::endl;
	 for(auto varIt = variableForSelection.begin(); varIt != variableForSelection.end(); varIt++) {
	      if(Select(*varIt, selections.at(selectIndex), { cuts.at(selectIndex*2), cuts.at(selectIndex*2 + 1) })) { selectIndex++; continue; }
	      selected = false;
	      break;
	 }

	 streamlog_out(DEBUG1) << "Event accepted: " << selected << std::endl;

      if(evtsToDisplay.size() > 0)
	{

	  //if(selected) { evtCount.at(collIndex)++; }

	  streamlog_out(DEBUG) << evtCount.at(collIndex) << std::endl;
	  if(std::find(evtsToDisplay.begin(), evtsToDisplay.end(), evtCount.at(collIndex)) != evtsToDisplay.end()){
	       DisplayEvt(collIndex);
	       if(evtCount.at(collIndex) == evtsToDisplay.back()) eventsDisplayed = true;
	  } 
	}
      else if(selected) {
	   streamlog_out(DEBUG2) << "nSkipped: " << nSkipped  << " skip: " << skip << std::endl;

	   if(nSkipped >= skip) {
		if(nDisplays == 10) { eventsDisplayed = true; }
		else { DisplayEvt(collIndex); }
	   }
	   else { 
		nSkipped++; 
		streamlog_out(DEBUG2) << "Skipping event, nSkipped: " << nSkipped << std::endl;
	   }
      }
    }
  
  
  if(_includeClusters) {
       //try { processClustersCollection(evtPt, collIndex); }
       //catch (lcio::DataNotAvailableException zero) { streamlog_out(MESSAGE) << "Error finding the cluster collection associated to "<< _hcalCollections[collIndex] << std::endl; }
  }


  streamlog_out(DEBUG) << "Filling tree" << std::endl;
  _outputTree->Fill();

  if(eventsDisplayed && stopToDisplay) throw(marlin::StopProcessingException(&a_ROOTMakerProc));
}


void ROOTMakerProc::processEventByClusters(LCCollection* col)
{

     timeInSpill = col->getParameters().getFloatVal("TimeInSpill");

     trkPars.clear();
     col->getParameters().getFloatVals("TrackParameters", trkPars);

     secondMax = col->getParameters().getIntVal("SecondMax");

     //ClusterFactory* clusters = col->getParameters().

     /*
  std::string clusCollName = _clustersCollections[collIndex];
  
  LCCollection* clusCol = evtPt->getCollection(clusCollName.c_str());
  nClusters = clusCol->getNumberOfElements();
  
  for(int iClus = 0; iClus < nClusters; iClus++) { //loop over clusters
    
    Cluster* clus = dynamic_cast<Cluster*>(clusCol->getElementAt(iClus));
    
    const float* position = clus->getPosition();
    vCenter_I.push_back(position[0]);
    vCenter_J.push_back(position[1]);
    vCenter_K.push_back(position[2]);
    
  } //end loop over clusters
     */
}

void ROOTMakerProc::processEvent( LCEvent * evtP ) 
{

  streamlog_out(DEBUG) << "Process ROOT Maker" << std::endl;
  
  if(_headerless && !histsCreated) processRunHeader(0);
  
  if(evtP){
    try{//try 1

      for(unsigned int i=0; i < _hcalCollections.size(); i++){//loop over collections
	try{//try2
	  
	  Clear();
	  evtCount.at(i)++;
	  
	  std::string collName = _hcalCollections[i];
	  
	  LCCollection* col = evtP->getCollection(collName.c_str());
	  int numElements = col->getNumberOfElements();
	  
	  evtnum = evtP->getEventNumber();
	  parent.SetString(collName.c_str());
	  
	  std::vector<int> timeParameters;
	  
	  streamlog_out(DEBUG) << "Starting collection processing" << std::endl;

	  
	  // -------------- SDHCAL_BeamMultiEvents ---------------
	  if(collName == "SDHCAL_BeamMultiEvents") {

	      std::vector<int> hitsPerEvent;
	      col->getParameters().getIntVals("NHitsPerEvent",hitsPerEvent);

	      int currentElementStart = 0;
	      
	      for(int iEvt = 0; iEvt < hitsPerEvent.size(); iEvt++) { //loop over events

		Clear();
		nhit = hitsPerEvent.at(iEvt);

		processGeneralCollection(col, i, currentElementStart);

		currentElementStart += nhit;
		
	      } //end loop over events
	  }

	  // ---------------- General Collections --------------------

	  else {
	    
	    nhit = numElements;
	    processGeneralCollection(col, i, 0, evtP);
	    
	  }
	  
	}catch (lcio::DataNotAvailableException zero) { streamlog_out(DEBUG) << "Error Try 2 with collection: "<< _hcalCollections[i] << std::endl; } //catch 2	
      }//end loop over collection
    }catch (lcio::DataNotAvailableException err) { std::cout << "Error Try 1" << std::endl; }//catch 1
    }
}




//==============================================================
void ROOTMakerProc::end()
{ 

  std::cout << "Writting the ROOT Files." << std::endl;
         
  _outputFile->cd();
  _outputTree->Write();
  
  delete _outputTree;  

  nHit_H->Write();
  delete nHit_H;
  
  TCanvas* thresholdsCanvas = new TCanvas(("Thresholds_" + std::to_string(E)).c_str(), "", 1920, 1080);
  nHit_N3->Draw();
  nHit_N2->Draw("same");
  nHit_N1->Draw("same");
  
  thresholdsCanvas->Write();
  delete thresholdsCanvas;

  _outputFile->Close();
  delete _outputFile;
  
}


 
