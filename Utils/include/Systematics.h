#ifndef __SYSTEMATICSTOOL__
#define __SYSTEMATICSTOOL__

#include <string>
#include <vector>
#include <map>

#include <fstream>

class SystematicsTool {

public:
  
  SystematicsTool(std::string systematicsFileName = "systematics.txt");
  ~SystematicsTool() { cuts.clear(); systematicsFile.close(); };

  bool AddCut(std::string cutName);
  void FillCut(std::string cutName, int result, int upResult, int downResult);
  void WriteSystematics();
  
private:

  struct Cut {
    int nElements;
    std::vector<int> cutResult, cutResultUp, cutResultDown;

    Cut() : nElements(0) {
      cutResult = cutResultUp = cutResultDown = {};
    };
    
    ~Cut() {
      cutResult.clear();
      cutResultUp.clear();
      cutResultDown.clear();
    }

  };

  std::map<std::string,Cut> cuts;

  std::ofstream systematicsFile;
  
};

#endif


