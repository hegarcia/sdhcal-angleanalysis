#ifndef _ROOTMaker_h_
#define _ROOTMaker_h_

#include <string>
#include <marlin/Processor.h>
#include <marlin/Exceptions.h>

//#include "ClusterFactory.h"

#include <TFile.h>
#include <TTree.h>
#include <TH2I.h>
#include <TGraph.h>
#include <TObjString.h>

#include <fstream>

class ROOTMakerProc: public marlin::Processor
{
public:
  
  Processor*  newProcessor() { return new ROOTMakerProc; }
  
  ROOTMakerProc();
  
  ~ROOTMakerProc() {};
  
  void init();

  void processEvent(LCEvent * evtP);

  void processRunHeader(LCRunHeader* runH);
  
  void end();
  
private:

  void Clear();
  void DisplayEvt(int collIndex);
  bool Select(int variableID, int selectionRule, std::vector<float> cuts);

  void processGeneralCollection(LCCollection* col, int collIndex, int startIndex = 0, LCEvent* evtPt = 0);
  void processEventByClusters(LCCollection* col);
  
  std::vector<std::string> _hcalCollections;
  std::vector<std::string> _clustersCollections;
  std::vector<std::string> _collectionToDisplay;
  std::string _treeName;
  std::string _rootName;
  int _noisyASICCut;
  int _noisyDIFCut;
  bool _includeClusters = false;
  bool _headerless;

  std::string _kEncoding;

  bool histsCreated = false;

  /* #### Didsplay Selections ####

     The first element selections[0] is an id for the value to apply selections:
          0 -> absBCID ### To Implement ###
	  1 -> BCID ### To Implement ###
	  2 -> nhit
	  3 -> nLayers ### To Implement ###
	  4 -> N1 ### To Implement ###
	  5 -> N2 ### To Implement ###
	  6 -> N3 ### To Implement ###
	  7 -> timeInSpill
	  8 -> Density
	  9 -> SlopeX
	  10 -> SlopeY

     The second element selections[1] is the id for the kind of selection and it depends on the number of boundaries: 1 or 2  
          0 -> >=  or  low <= <= high 
	  1 -> >   or  low < <= high
	  2 -> ==  or  low < < high
	  3 -> !=  or  low <= < high
	  4 -> <   
	  5 -> <= 

     The third and fourth elements selections[2] and selections[3] is the actual values for the boundaries. */
  
  std::vector<int> variableForSelection = {};
  std::vector<int> selections = {}; 
  std::vector<float> cuts = {};
  std::vector<int> evtsToDisplay = {}; //If this is not empty the processor will only apply the selections and display to the event numbers in this vector

  int skip;

  std::vector<float> runEnergies = {};
  int runIndex = 0;

  bool stopToDisplay = false;

  std::ifstream corrFile;

  int nStats;
  float corrN1;
  float errN1;
  float corrN2;
  float errN2;
  float corrN3;
  float errN3;
  
  // ----- ROOT Variables -----
  
  TFile* _outputFile = nullptr;
  TTree* _outputTree = nullptr;  

  TH2I* XY;
  TH2I* ZY;
  TH2I* XZ;

  TGraph* XZT1;
  TGraph* XZT2;
  TGraph* XZT3;

  TGraph* YZT1;
  TGraph* YZT2;
  TGraph* YZT3;

  TGraph* XYT1;
  TGraph* XYT2;
  TGraph* XYT3;

  std::vector<float> trkPars = {};
  int secondMax = 0;
  int showerStart = -1;

  std::vector<float> rms;

  int nDisplays = 0;
  int nSkipped = 0;

  TH1F* nHit_H;
  TH1F* nHit_N1;
  TH1F* nHit_N2;
  TH1F* nHit_N3;

  // ----- Tree Branch Variables ----- 
  
  int evtnum;
  TObjString parent;
  unsigned long long int absBCID; //Time from the beggining of the run to the end of acquisition
  int BCID;

  int E;
  int nhit;
  int nhit_corr;
  int nLayers;
  int N1, N2, N3;
  float timeInSpill;

  bool noisyASIC = false;
  bool noisyDIF = false;
  
  std::vector<int> vI;
  std::vector<int> vJ;
  std::vector<int> vLayer;
  std::vector<int> nHitsDIF;
  std::vector<int> nHitsASIC;
  std::vector<float> vX;
  std::vector<float> vY;  //Position in mm
  std::vector<float> vZ;
  std::vector<int> vThreshold;
  std::vector<unsigned int> vBCID; //Time from hit to the end of acquisition.

  int nClusters;
  std::vector<int> vCenter_I;
  std::vector<int> vCenter_J;
  std::vector<int> vCenter_K;

  // -------------------------------

  std::vector<int> evtCount;

  std::map<int,std::map<int,int>> nHitsMap = {};
  
  
};

#endif


