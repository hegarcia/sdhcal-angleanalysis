#ifndef __MUONSELECTOR__
#define __MUONSELECTOR__

#include "Systematics.h"

#include <marlin/Processor.h>

#include <IO/LCWriter.h>

#include <EVENT/LCRunHeader.h>
#include <EVENT/LCEvent.h>

#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>

#include <map>
#include <vector>
#include <string>

class MuonSelectorProc: public marlin::Processor
{
public:

  Processor*  newProcessor() { return new MuonSelectorProc; }

  MuonSelectorProc();

  ~MuonSelectorProc() {};

  void init();

  void processRunHeader( LCRunHeader * runHd);
  void processEvent(LCEvent * evtP);
  
  void end();
  
private:

  void writeHistograms();

  // ---- Input variables ----
  
  std::vector<std::string> _hcalCollections;
  std::string _outFileNameMuons;
  std::string _outColNameMuons;
  std::string _outFileNameShowers;
  std::string _outColNameShowers;
  std::string _logRootName;
  float _densityCutMuons;
  float _densityCutDiscarded;
  int _secondMaxCutMuons;
  int _secondMaxCutDiscarded;
  
  // ---- Output variables ----

  LCWriter *_lcWriterMuons, *_lcWriterShowers;

  TFile* _outputFile;

  std::map<std::string, TH1F*> histograms1D;
  std::map<std::string, TH2F*> histograms2D;

  // ---- Counters ----

  int evtnum, runNumber = -1;
  int nDiscarded, nMuons, nShowers;

  // ---- Tools ----

  SystematicsTool *sysToolMuons, *sysToolShowers;
  
};

#endif


