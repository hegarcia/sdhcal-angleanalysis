#ifndef __INTENSITYCORRECTION__
#define __INTENSITYCORRECTION__

#include "Systematics.h"

#include <marlin/Processor.h>

#include <EVENT/LCRunHeader.h>
#include <EVENT/LCEvent.h>

#include <TFile.h>
#include <TH1F.h>
//#include <TGraphAsymmErrors.h>
#include <TGraphErrors.h>

#include <map>
#include <vector>
#include <string>
#include <utility>
#include <fstream>

class IntensityCorrectionProc: public marlin::Processor
{
public:

  Processor*  newProcessor() { return new IntensityCorrectionProc; }

  IntensityCorrectionProc();

  ~IntensityCorrectionProc() {};

  void init();

  void processRunHeader( LCRunHeader * runHd);
  void processEvent(LCEvent * evtP);
  
  void end();
  
private:

  void computeCorrection();
  void writeHistograms();

  // ---- Input variables ----
  
  std::vector<std::string> _hcalCollections;
  std::string _logRootName;
  std::string _correctionFileBaseName;
  std::vector<float> _angleCorrParameters;
  float _binSize;

  // ---- Output variables ----

  TFile* _outputFile;
  std::ofstream correctionFile;
  
  std::map<std::string, TH1F*> histograms1D;

  // ---- Run variables ----

  std::map<int, std::map<int,std::vector<int>>> nHitsMap = {};
  /*TGraphAsymmErrors* fitGraphN1 = nullptr;
  TGraphAsymmErrors* fitGraphN2 = nullptr;
  TGraphAsymmErrors* fitGraphN3 = nullptr;
  */
  TGraphErrors* fitGraphN1 = nullptr;
  TGraphErrors* fitGraphN2 = nullptr;
  TGraphErrors* fitGraphN3 = nullptr;
  

  // ---- Counters ----

  int runNumber = -1;
  
};

#endif


