#ifndef __SEMIDIGITALPARAMETRIZATION__
#define __SEMIDIGITALPARAMETRIZATION__

#include <marlin/Processor.h>

#include <TFile.h>
#include <TH1F.h>
#include <TF1.h>

#include <fstream>

#include <map>
#include <vector>
#include <string>

class SemiDigitalParametrizationProc: public marlin::Processor
{
public:
  
  Processor*  newProcessor() { return new SemiDigitalParametrizationProc; }
  
  SemiDigitalParametrizationProc();
  
  ~SemiDigitalParametrizationProc() {};
  
  void init();

  void processRunHeader(LCRunHeader* runHd);
  void processEvent(LCEvent * evtP);
  
  void end();

  double chiSquare(double* par);

private:

  //void computeEnergySystematics();
  void writeHistograms();

  // ---- Input variables ---- 
  
  std::vector<std::string> _hcalCollections;
  std::string _outROOTName;
  std::string _outParametersFileName;
  std::string _intensityCorrectionFileName;
  std::vector<float> _angleCorrParameters;
  int _maxNHits, _nBins;
  
  std::vector<float> initAlphas, initBetas, initGammas;

  std::ifstream correctionFile;

  std::map<int,int> beamEnergiesMap;
  
  // ---- Output variables ----

  TFile* _outputFile;
  std::ofstream minimFile;

  std::map<std::string, TH1F*> histograms1D;
  
  // ---- Minim variables ----

  std::map<int, std::vector<int>> N_1, N_2, N_3;
  std::map<int, std::map<int,int>> statsPerBin = {};

  // ---- EReco variables ----

  TF1 *alpha, *beta, *gamma, *bestAlpha, *bestBeta, *bestGamma;

  float corrN1, eCorrN1, corrN2, eCorrN2, corrN3, eCorrN3; 
  
  // ---- Counters ----

  int runNumber, beamEnergy = -1;
  
};

#endif


