#ifndef __ELECTRONSELECTOR__
#define __ELECTRONSELECTOR__

#include "Systematics.h"

#include <marlin/Processor.h>

#include <IO/LCWriter.h>

#include <EVENT/LCRunHeader.h>
#include <EVENT/LCEvent.h>

#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>

#include <map>
#include <vector>
#include <string>

class ElectronSelectorProc: public marlin::Processor
{
public:

  Processor*  newProcessor() { return new ElectronSelectorProc; }

  ElectronSelectorProc();

  ~ElectronSelectorProc() {};

  void init();

  void processRunHeader( LCRunHeader * runHd);
  void processEvent(LCEvent * evtP);
  
  void end();
  
private:

  void writeHistograms();

  // ---- Input variables ----
  
  std::vector<std::string> _hcalCollections;
  std::string _outFileNameElectrons;
  std::string _outColNameElectrons;
  std::string _outFileNamePions;
  std::string _outColNamePions;
  std::string _logRootName;
  int _showerStartCut;
  int _NLayersCut;
  int _NLayersLeakageCut;
  float _RMSCut;
  float _densityCut;
  
  // ---- Output variables ----

  LCWriter *_lcWriterElectrons, *_lcWriterPions;

  TFile* _outputFile;

  std::map<std::string, TH1F*> histograms1D;
  std::map<std::string, TH2F*> histograms2D;
  
  // ---- Counters ----

  int evtnum, runNumber = -1;
  int nDiscarded, nElectrons, nPions;

  // ---- Tools ----

  SystematicsTool *sysToolElectrons, *sysToolPions;
  
};

#endif


