#ifndef __COSMICSANGULARSELECTOR__
#define __COSMICSANGULARSELECTOR__

#include "Systematics.h"

#include <marlin/Processor.h>

#include <IO/LCWriter.h>

#include <EVENT/LCRunHeader.h>
#include <EVENT/LCEvent.h>

#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>

#include <map>
#include <vector>
#include <string>

class CosmicsAngularSelectorProc: public marlin::Processor
{
public:

  Processor*  newProcessor() { return new CosmicsAngularSelectorProc; }

  CosmicsAngularSelectorProc();

  ~CosmicsAngularSelectorProc() {};

  void init();

  void processRunHeader( LCRunHeader * runHd);
  void processEvent(LCEvent * evtP);
  
  void end();
  
private:

  void writeHistograms();

  // ---- Input variables ----
  
  std::vector<std::string> _hcalCollections;
  std::string _outFileName;
  std::string _outColName;
  std::string _logRootName;
  float _slopeXCut;
  float _slopeYCut;

  // ---- Output variables ----

  LCWriter* _lcWriter;

  TFile* _outputFile;

  std::map<std::string, TH1F*> histograms1D;
  std::map<std::string, TH2F*> histograms2D;
  
  // ---- Counters ----

  int evtnum, runNumber = -1;
  int nOther, nCosmics;

  // ---- Tools ----

  SystematicsTool* sysTool;
  
};

#endif


