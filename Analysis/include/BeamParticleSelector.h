#ifndef __BEAMPARTICLESELECTOR__
#define __BEAMPARTICLESELECTOR__

#include "Systematics.h"

#include <marlin/Processor.h>

#include <IO/LCWriter.h>

#include <EVENT/LCRunHeader.h>
#include <EVENT/LCEvent.h>

#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>

#include <map>
#include <vector>
#include <string>

class BeamParticleSelectorProc: public marlin::Processor
{
public:

  Processor*  newProcessor() { return new BeamParticleSelectorProc; }

  BeamParticleSelectorProc();

  ~BeamParticleSelectorProc() {};

  void init();

  void processRunHeader( LCRunHeader * runHd);
  void processEvent(LCEvent * evtP);
  
  void end();
  
private:

  void writeHistograms();

  // ---- Input variables ----
  
  std::vector<std::string> _hcalCollections;
  std::string _outFileName;
  std::string _outColName;
  std::string _logRootName;
  int _lastScanLayer;
  int _nLayersScanCut;

  // ---- Output variables ----

  LCWriter* _lcWriter;

  TFile* _outputFile;

  std::map<std::string, TH1F*> histograms1D;
  std::map<std::string, TH2F*> histograms2D;  
  
  // ---- Counters ----

  int evtnum, runNumber = -1;
  int nDiscarded, nBeamParticles;

  // ---- Tools ----

  SystematicsTool* sysTool;
  
};

#endif


