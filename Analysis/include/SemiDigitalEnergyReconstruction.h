#ifndef __SEMIDIGITALENERGYRECONSTRUCTION__
#define __SEMIDIGITALENERGYRECONSTRUCTION__

#include <marlin/Processor.h>

#include <TGraphAsymmErrors.h>
#include <TH1F.h>
#include <TFile.h>
#include <TF1.h>

#include <fstream>

#include <map>
#include <vector>
#include <string>
#include <utility>

class SemiDigitalEnergyReconstructionProc: public marlin::Processor
{
public:
  
  Processor*  newProcessor() { return new SemiDigitalEnergyReconstructionProc; }
  
  SemiDigitalEnergyReconstructionProc();
  
  ~SemiDigitalEnergyReconstructionProc() {};
  
  void init();

  void processRunHeader(LCRunHeader* runP);
  void processEvent(LCEvent * evtP);
  
  void end();

private:

  void computeEnergySystematics();
  void writeHistograms(bool writeAll = true);

  // ---- Input variables ---- 
  
  std::vector<std::string> _hcalCollections;
  std::string _outROOTName;
  std::string _showerSystematicsFileName;
  std::string _intensityCorrectionFileName;
  std::string _parametrizationFileName;
  std::vector<float> _angleCorrParameters;
  int _nBins, _maxNHits, _maxE;

  std::ifstream systematicsFile;
  std::ifstream correctionFile;
  std::ifstream parametrizationFile;

  std::map<int,int> beamEnergiesMap;
  
  // ---- Output variables ----

  TFile* _outputFile;

  std::map<std::string, TGraphAsymmErrors*> graphs1D;
  std::map<std::string, TH1F*> histograms1D;
  
  // ---- EReco variables ----

  TF1 *alpha, *beta, *gamma;
  float corrN1, eCorrN1, corrN2, eCorrN2, corrN3, eCorrN3; 

  // ---- Systematics ----

  std::map<int,std::map<int,std::pair<float,float>>> nHitsSystematicsMap = {}; // [Bin][run] { SysUp, SysDown }

  // ---- Map -> bin , run , values ----

  std::map<int, std::map<int, int>> binRunEntriesMap = {};
  
  std::map<int, int[3]> meanNTPerBin = {};
  std::map<int, float> meanTimeInSpillPerBin = {};
  std::map<int, float> meanAnglePerBin = {};

  std::map<int, int> binEntriesMapAngleCorr = {};
  
  std::map<int, int[3]> meanNPerBinAngleCorr = {};
  std::map<int, float> meanTimeInSpillPerBinAngleCorr = {};

  
  //std::map<int,std::pair<float,float>> ESystematicsMap = {};
  
  // ---- Counters ----

  int evtnum, runNumber, beamEnergy;
};

#endif


