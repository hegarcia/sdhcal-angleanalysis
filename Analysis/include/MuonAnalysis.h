#ifndef __MUONANALYSIS__
#define __MUONANALYSIS__

#include <marlin/Processor.h>

#include <EVENT/LCRunHeader.h>
#include <EVENT/LCEvent.h>

#include <TPaveText.h>
#include <TGraph.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>

#include <map>
#include <vector>
#include <string>
#include <utility>

class MuonAnalysisProc: public marlin::Processor
{
public:

  Processor*  newProcessor() { return new MuonAnalysisProc; }

  MuonAnalysisProc();

  ~MuonAnalysisProc() {};

  void init();

  void processRunHeader( LCRunHeader * runHd);
  void processEvent(LCEvent * evtP);
  
  void end();
  
private:

  void computeEffAndMult(std::string type);
  void writeHistograms();

  // ---- Input variables ----
  
  std::vector<std::string> _hcalCollections;
  std::string _logRootName;
  std::vector<float> _refAngles;
  int _scan, _muonsPerRun;
  
  // ---- Output variables ----

  TFile* _outputFile;

  std::map<std::string, TGraph*> graphs;
  std::map<std::string, TH1F*> globalHistograms1D;
  std::map<std::string, TPaveText*> statBoxes;
  std::map<std::string, TH1F*> histograms1D;
  std::map<std::string, TH2F*> histograms2D;

  // ---- Run variables ----

  std::map<int,std::pair<int,int>> runHitEff = {};
  std::map<int,int> runMult = {};

  /*std::map<int,std::pair<int,int>> runASICEff = {};
  std::map<int,std::pair<int,int>> runDIFEff = {};
  std::map<int,std::pair<int,int>> runLayerEff = {};

  std::map<int,int> runASICMult = {};
  std::map<int,int> runDIFMult = {};
  std::map<int,int> runLayerMult = {};*/

  // ---- Detector variables ----

  std::map<int,std::pair<int,int>> detHitEff = {};
  std::map<int,int> detMult = {};
  
  /*std::map<int,std::pair<int,int>> detASICEff = {};
  std::map<int,std::pair<int,int>> detDIFEff = {};
  std::map<int,std::pair<int,int>> detLayerEff = {};

  std::map<int,int> detASICMult = {};
  std::map<int,int> detDIFMult = {};
  std::map<int,int> detLayerMult = {};*/

  // ---- Counters ----

  int evtnum, runNumber = -1;
  int nMuonsTotal, nMuonsRun;

};

#endif


