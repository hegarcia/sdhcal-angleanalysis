#include "MuonAnalysis.h"

#include <UTIL/CellIDDecoder.h>

#include <EVENT/LCCollection.h>
#include <EVENT/CalorimeterHit.h>

//#include "TPaveText.h"
#include <TF1.h>

#include <iostream>

#include <TMath.h>

MuonAnalysisProc a_MuonAnalysisProc;

//=========================================================
MuonAnalysisProc::MuonAnalysisProc()
  :Processor("MuonAnalysisProc")
{
  
  //Input Collections    
  _hcalCollections.push_back(std::string("SDHCAL_BeamEvents"));
  registerInputCollections(LCIO::RAWCALORIMETERHIT, 
			   "HCALCollections",  
			   "HCAL Collection Names",  
			   _hcalCollections, 
			   _hcalCollections); 
  
  //Output ROOT name
  _logRootName = "LogROOT_BeamSelection";
  registerProcessorParameter("logRoot_Name" ,
                             "LogRoot name",
                             _logRootName,
			     _logRootName);

  //Muons Per Run
  _muonsPerRun = 10000;
  registerProcessorParameter("MuonsPerRun" ,
                             "Number of muons to analyze per run",
                             _muonsPerRun,
			     _muonsPerRun);

  //Input angles to search the muons
  _refAngles = {};
  registerProcessorParameter("InputAngles" ,
                             "Angles of reference to search the muons",
                             _refAngles,
			     _refAngles);

  //Scan window
  _scan = 2;
  registerProcessorParameter("ScanWindow" ,
                             "ScanWindow",
                             _scan,
			     _scan);


} 

void MuonAnalysisProc::init() {

     streamlog_out(DEBUG4) << "MuonAnalysis Init" << std::endl;

     _outputFile = new TFile((_logRootName + ".root").c_str(),"RECREATE");	  

     globalHistograms1D["SlopeX"] = new TH1F("GlobalSlopeX", ";SlopeX;Entries", 1000, -5., 5.);
     globalHistograms1D["SlopeXErr"] = new TH1F("GlobalSlopeXErr", ";SlopeXErr;Entries", 1000, 0., 0.01);
     globalHistograms1D["ParXErr"] = new TH1F("GlobalParXErr", ";ParXErr;Entries", 1000, 0., 2.0);
     globalHistograms1D["ChiNDFX"] = new TH1F("GlobalChiNDFX", ";CHiNDFX;Entries", 500, 0., 200.);

     globalHistograms1D["SlopeY"] = new TH1F("GlobalSlopeY", ";SlopeY;Entries", 1000, -5., 5.);
     globalHistograms1D["ChiNDFY"] = new TH1F("GlobalChiNDFY", ";ChiNDFY;Entries", 500, 0., 200.);
     globalHistograms1D["SlopeYErr"] = new TH1F("GlobalSlopeYErr", ";SlopeYErr;Entries", 1000, 0., 0.01);
     globalHistograms1D["ParYErr"] = new TH1F("GlobalParYErr", ";ParYErr;Entries", 1000, 0., 2.0);


     nMuonsTotal = nMuonsRun = 0;
     
     printParameters();
}


void MuonAnalysisProc::processRunHeader( LCRunHeader* runHd )
{

  streamlog_out(MESSAGE) << "MuonAnalysis processing run: " << runHd->getRunNumber() << std::endl;

  if(runNumber != -1) {
    computeEffAndMult("Run");
    writeHistograms();
    std::cout << "Analyzed muons in this run: " << nMuonsRun << std::endl;
  }
  runNumber = runHd->getRunNumber();

  // ---- Creation of the histograms ----

  streamlog_out(DEBUG4) << "Creating new local histograms" << std::endl;

  _outputFile->mkdir(std::to_string(runNumber).c_str());
  _outputFile->cd(std::to_string(runNumber).c_str());

  histograms1D["SlopeX"] = new TH1F("SlopeX", ";SlopeX;Entries", 100, -5., 5.);
  histograms1D["SlopeXErr"] = new TH1F("SlopeXErr", ";SlopeXErr;Entries", 100, 0., 0.01);
  histograms1D["ParXErr"] = new TH1F("ParXErr", ";ParXErr;Entries", 100, 0., 2.0);
  histograms1D["ChiNDFX"] = new TH1F("ChiNDFX", ";CHiNDFX;Entries", 1000, 0., 1000.);

  histograms1D["SlopeY"] = new TH1F("SlopeY", ";SlopeY;Entries", 100, -5., 5.);
  histograms1D["ChiNDFY"] = new TH1F("ChiNDFY", ";ChiNDFY;Entries", 1000, 0., 1000.);
  histograms1D["SlopeYErr"] = new TH1F("SlopeYErr", ";SlopeYErr;Entries", 100, 0., 0.01);
  histograms1D["ParYErr"] = new TH1F("ParYErr", ";ParYErr;Entries", 100, 0., 2.0);

  // ---- Reset counters and maps ----

  nMuonsTotal += nMuonsRun;
  evtnum = nMuonsRun = 0;
  
  runHitEff.clear();
  runMult.clear();

  /*
  runASICEff.clear();
  runDIFEff.clear();
  runLayerEff.clear();

  runASICMult.clear();
  runDIFMult.clear();
  runLayerMult.clear();*/

}

void MuonAnalysisProc::processEvent( LCEvent * evtP ) 
{

  streamlog_out(DEBUG4) << "Start MuonAnalysis process" << std::endl;

  if(nMuonsRun > _muonsPerRun && _muonsPerRun > 0) return;

  if(evtP){ //Check the evtP
    for(unsigned int i=0; i < _hcalCollections.size(); i++){//loop over collections
      try{

	evtnum++;
	
	LCCollection* col = evtP->getCollection(_hcalCollections[i].c_str());
	int nHits = col->getNumberOfElements();
	
	CellIDDecoder<CalorimeterHit> cd("I:7,J:7,K:10,Dif_id:8,Asic_id:6,Chan_id:7");

	std::map<int,std::map<std::pair<int,int>,bool>> hitMap = {}; 
	std::map<int, std::pair<int,std::pair<float,float>>> trackMap = {};

	for(int iHit = 0; iHit < nHits; iHit++) {
	  
	  CalorimeterHit* hit = dynamic_cast<CalorimeterHit*>(col->getElementAt(iHit));

	  int I = (int)cd(hit)["I"];
	  int J = (int)cd(hit)["J"];
	  int K = (int)cd(hit)["K"];

	  hitMap[K][std::pair<int,int>(I,J)] = true;
	  
	  trackMap[K].first++;
	  trackMap[K].second.first += I*10.4;
	  trackMap[K].second.second += J*10.4;

	}	  

	int eventLength = hitMap.rbegin()->first - hitMap.begin()->first;
	if(eventLength < 10) {
	     std::cout << "FirstK : " << hitMap.begin()->first << " LastK: " << hitMap.rbegin()->first << std::endl; 
	     return;
	}
	for(int iLayer = 0; iLayer < 49; iLayer++) {

	     std::vector<float> x, y, z;
	     x = y = z = {};

	     for(auto layerIt = trackMap.begin(); layerIt != trackMap.end(); layerIt++) {
		  if(layerIt->first == iLayer) continue;

		  float xMean = layerIt->second.second.first/(float)layerIt->second.first;
		  float yMean = layerIt->second.second.second/(float)layerIt->second.first;
		 
		  float xVal, yVal;
		  xVal = yVal = 0;
		  int NHits = 0;
		  
		  for(auto hit : hitMap.at(layerIt->first)) {
	        
		       if(TMath::Abs(hit.first.first*10.4 - xMean) > 8*10.4 && TMath::Abs(hit.first.second*10.4 - yMean) > 8*10.4) continue;

		       NHits++;
		       xVal += hit.first.first*10.4;
		       yVal += hit.first.second*10.4;
		  }
		  
		  xVal /= NHits;
		  yVal /= NHits;
		  x.push_back(xVal);
		  y.push_back(yVal);
		  z.push_back(layerIt->first*28);		  
	     }
	     
	     TGraph *trackXZ = new TGraph(z.size(), z.data(), x.data());
	     trackXZ->Fit("pol1", "Q");
	     double* trackParametersX = (trackXZ->GetFunction("pol1"))->GetParameters();
	     double chiNDFX = (trackXZ->GetFunction("pol1"))->GetChisquare()/(trackXZ->GetFunction("pol1"))->GetNDF();
	     
	     TGraph *trackYZ = new TGraph(z.size(), z.data(), y.data());
	     trackYZ->Fit("pol1", "Q");
	     double* trackParametersY = (trackYZ->GetFunction("pol1"))->GetParameters();
	     double chiNDFY = (trackYZ->GetFunction("pol1"))->GetChisquare()/(trackYZ->GetFunction("pol1"))->GetNDF();
	     
	     // The cut in the diference is 0.09 because it is scan of 5º around the refernce angle
	     if((_refAngles.size() > 0 && (TMath::Abs(trackParametersX[1] - TMath::Tan(_refAngles.at(0)*TMath::Pi()/180.)) > 0.09 || TMath::Abs(trackParametersY[1] - TMath::Tan(_refAngles.at(1)*TMath::Pi()/180.)) > 0.09)) || chiNDFX > 20 || chiNDFY > 20) {
		  delete trackXZ;
		  delete trackYZ;
		  continue;
	     }

	     int trackI = (trackParametersX[0] + trackParametersX[1]*iLayer*28)/10.4;
	     int trackJ = (trackParametersY[0] + trackParametersY[1]*iLayer*28)/10.4;

	     if(trackI < 0 || trackI > 95 || trackJ < 0 || trackJ > 95) continue;
	     
	     histograms1D.at("SlopeX")->Fill(trackParametersX[1]);
	     histograms1D.at("ChiNDFX")->Fill(chiNDFX);
	     globalHistograms1D.at("SlopeX")->Fill(trackParametersX[1]);
	     globalHistograms1D.at("ChiNDFX")->Fill(chiNDFX);

	     histograms1D.at("SlopeY")->Fill(trackParametersY[1]);
	     histograms1D.at("ChiNDFY")->Fill(chiNDFY);
	     globalHistograms1D.at("SlopeY")->Fill(trackParametersY[1]);
	     globalHistograms1D.at("ChiNDFY")->Fill(chiNDFY);

	     histograms1D.at("SlopeXErr")->Fill((trackXZ->GetFunction("pol1"))->GetParErrors()[1]);
	     histograms1D.at("ParXErr")->Fill((trackXZ->GetFunction("pol1"))->GetParErrors()[0]);
	     histograms1D.at("SlopeYErr")->Fill((trackYZ->GetFunction("pol1"))->GetParErrors()[1]);
	     histograms1D.at("ParYErr")->Fill((trackYZ->GetFunction("pol1"))->GetParErrors()[0]);

	     globalHistograms1D.at("SlopeXErr")->Fill((trackXZ->GetFunction("pol1"))->GetParErrors()[1]);
	     globalHistograms1D.at("ParXErr")->Fill((trackXZ->GetFunction("pol1"))->GetParErrors()[0]);
	     globalHistograms1D.at("SlopeYErr")->Fill((trackYZ->GetFunction("pol1"))->GetParErrors()[1]);
	     globalHistograms1D.at("ParYErr")->Fill((trackYZ->GetFunction("pol1"))->GetParErrors()[0]);
	     	     
	     int trackHitID = iLayer*96*96 + trackJ*96 + trackI;
	     //int trackASICID = iLayer*144 + (trackJ/8)*12 + trackI/8;
	     //int trackDIFID = iLayer*3 + trackI/32;

	     runHitEff[trackHitID].first++;
	     detHitEff[trackHitID].first++;

	     //runASICEff[trackASICID].first++;
	     //detASICEff[trackASICID].first++;

	     //runDIFEff[trackDIFID].first++;
	     //detDIFEff[trackDIFID].first++;

	     //runLayerEff[iLayer].first++;
	     //detLayerEff[iLayer].first++;

	     if(hitMap.find(iLayer) == hitMap.end()) continue;


	     std::pair<int,int> smallScanI = { trackI - _scan, trackI + _scan };
	     std::pair<int,int> smallScanJ = { trackJ - _scan, trackJ + _scan };	     

	     bool isHitEff = false; //, isEffLay, isEffDIF, isEffASIC;
	     //isHitEff = isEffLay = isEffDIF = isEffASIC = false;

	     std::pair<int,int> foundHit;

	     for(int iI = smallScanI.first; iI <= smallScanI.second; iI++) {
		  for(int iJ = smallScanJ.first; iJ <= smallScanJ.second; iJ++) {
		       if(hitMap.at(iLayer).find(std::pair<int,int>(iI,iJ)) != hitMap.at(iLayer).end()) {
			    isHitEff = true;
			    foundHit.first = iI;
			    foundHit.second = iJ;
			    runHitEff[trackHitID].second++;
			    detHitEff[trackHitID].second++;
			    /*runASICEff[trackASICID].second++;
			    detASICEff[trackASICID].second++;
			    runDIFEff[trackDIFID].second++;
			    detDIFEff[trackDIFID].second++;
			    runLayerEff[iLayer].second++;
			    detLayerEff[iLayer].second++;*/
			    break;
		       }
		  }
		  if(isHitEff) break;
	     }

	     if(isHitEff) {

		  std::vector<std::pair<int,int>> cluster = { foundHit };
		  for(auto hit : cluster) {
		       if(hitMap.at(iLayer).find(std::pair<int,int>(hit.first - 1,hit.second)) != hitMap.at(iLayer).end() && std::find(cluster.begin(),cluster.end(),std::pair<int,int>(hit.first - 1,hit.second)) == cluster.end()) cluster.push_back(std::pair<int,int>(hit.first - 1,hit.second));
		       if(hitMap.at(iLayer).find(std::pair<int,int>(hit.first + 1,hit.second)) != hitMap.at(iLayer).end() && std::find(cluster.begin(),cluster.end(),std::pair<int,int>(hit.first + 1,hit.second)) == cluster.end()) cluster.push_back(std::pair<int,int>(hit.first + 1,hit.second));
		       if(hitMap.at(iLayer).find(std::pair<int,int>(hit.first,hit.second - 1)) != hitMap.at(iLayer).end() && std::find(cluster.begin(),cluster.end(),std::pair<int,int>(hit.first,hit.second - 1)) == cluster.end()) cluster.push_back(std::pair<int,int>(hit.first,hit.second - 1));
		       if(hitMap.at(iLayer).find(std::pair<int,int>(hit.first,hit.second + 1)) != hitMap.at(iLayer).end() && std::find(cluster.begin(),cluster.end(),std::pair<int,int>(hit.first,hit.second + 1)) == cluster.end()) cluster.push_back(std::pair<int,int>(hit.first,hit.second + 1));
		  }
		  
		  runMult[trackHitID] += cluster.size();
		  detMult[trackHitID] += cluster.size();

		  /*runASICMult[trackASICID] += cluster.size();
		  detASICMult[trackASICID] += cluster.size();
		  runDIFMult[trackDIFID] += cluster.size();
		  detDIFMult[trackDIFID] += cluster.size();
		  runLayerMult[iLayer] += cluster.size();
		  detLayerMult[iLayer] += cluster.size();*/
	     }
		  
	     delete trackXZ;
	     delete trackYZ;

	} // End loop over layers

	nMuonsRun++;
	
      }catch (lcio::DataNotAvailableException zero) { streamlog_out(DEBUG) << "ERROR READING COLLECTION: " << _hcalCollections[i] << std::endl;} 
    }//end loop over collection
  }
  
}


//==============================================================
void MuonAnalysisProc::end()
{

  nMuonsTotal += nMuonsRun;
  std::cout << "NMuonsEndTotal: " << nMuonsTotal << std::endl;

  computeEffAndMult("Run");
  computeEffAndMult("Total");
  writeHistograms();

  _outputFile->cd();

  for(auto gHist : globalHistograms1D) {
       gHist.second->Write();
       delete gHist.second;
  }

  globalHistograms1D.clear();

  _outputFile->Close();
  delete _outputFile;
  
  streamlog_out(DEBUG4) << "Ending MuonAnalysis" << std::endl;

}

void MuonAnalysisProc::computeEffAndMult(std::string type) {

     std::map<int, std::pair<int,int>>* effMap;
     std::map<int,int>* multMap;

     std::string prefix = "";

     if(type == "Run") {
	  effMap = &runHitEff;
	  multMap = &runMult;
	  prefix = std::to_string(runNumber) + "/" + type;
     }
     else if(type == "Total") {
	  effMap = &detHitEff;
	  multMap = &detMult;
	  prefix = type;	  
     }
     else {
	  std::cout << "Wrong type computing the efficiencies and multiplicities: " << type << std::endl;
	  return;
     }

     streamlog_out(DEBUG) << "Creating Efficiency and Multiplicity folders" << std::endl;

     _outputFile->cd();
     _outputFile->mkdir((prefix + "Stats").c_str());
     _outputFile->mkdir((prefix + "DIFEfficiencies").c_str());
     _outputFile->mkdir((prefix + "DIFStatBoxes").c_str());
     _outputFile->mkdir((prefix + "DIFMultiplicities").c_str());
     _outputFile->mkdir((prefix + "ASICEfficiencies").c_str());
     _outputFile->mkdir((prefix + "ASICStatBoxes").c_str());
     _outputFile->mkdir((prefix + "ASICMultiplicities").c_str());
     _outputFile->mkdir((prefix + "HitEfficiencies").c_str());

     std::map<int,std::pair<int,float>> layerEfficiency = {};
     std::map<int,std::pair<int,float>> layerMultiplicity = {};
	  
     std::map<int,std::pair<int,float>> DIFEfficiency = {};
     std::map<int,std::pair<int,float>> DIFMultiplicity = {};

     std::map<int,std::pair<int,float>> ASICEfficiency = {};
     std::map<int,std::pair<int,float>> ASICMultiplicity = {};
        
     for(auto hit : *effMap) {
	  
	  int K = (int)hit.first/(96*96);
	  int I = (int)(hit.first%(96*96))%96;
	  int J = (int)(hit.first%(96*96))/96;

	  // ---- Layer efficiencies ----

	  streamlog_out(DEBUG) << "Computing efficiencies in layer: " << K << std::endl;
	 
	  layerEfficiency[K].first++;
	  layerEfficiency[K].second += hit.second.second/(float)hit.second.first;

	  // ---- Layer stats ----

	  streamlog_out(DEBUG) << "Computing stats in layer: " << K << std::endl;
	  
	  if(histograms2D.find((type + "Stats_Layer" + std::to_string(K)).c_str()) == histograms2D.end()) histograms2D[(type + "Stats_Layer" + std::to_string(K)).c_str()] = new TH2F((type + "Stats_Layer" + std::to_string(K)).c_str(), ";I;J", 96, 0., 96., 96, 0., 96.);	 
	  
	  histograms2D.at((type + "Stats_Layer" + std::to_string(K)).c_str())->SetBinContent(I + 1,J + 1, hit.second.first);

	  // ---- DIF efficiencies ----

	  int DIFID = K*3 + I/32;

	  DIFEfficiency[DIFID].first++;
	  DIFEfficiency[DIFID].second += hit.second.second/(float)hit.second.first;

	  // ---- ASIC efficiencies ---- 

	  streamlog_out(DEBUG) << "Computing asic efficiencies" << std::endl;

	  int ASICID = K*144 + (J/8)*12 + I/8;

	  ASICEfficiency[ASICID].first++;
	  ASICEfficiency[ASICID].second += hit.second.second/(float)hit.second.first;

	  // ---- HitEfficiencies ----

	  streamlog_out(DEBUG) << "Computing hit efficiencies" << std::endl;

	  if(histograms2D.find((type + "HitEfficiency_Layer" + std::to_string(K)).c_str()) == histograms2D.end()) histograms2D[(type + "HitEfficiency_Layer" + std::to_string(K)).c_str()] = new TH2F((type + "HitEfficiency_Layer" + std::to_string(K)).c_str(), ";I;J", 96, 0., 96., 96, 0., 96.);	 
	  
	  histograms2D[(type + "HitEfficiency_Layer" + std::to_string(K)).c_str()]->SetMaximum(100.);
	  histograms2D[(type + "HitEfficiency_Layer" + std::to_string(K)).c_str()]->SetMinimum(-0.01);

	  histograms2D.at((type + "HitEfficiency_Layer" + std::to_string(K)).c_str())->SetBinContent(I + 1,J + 1,hit.second.second*100/(float)hit.second.first);

	  // ---- Multiplicities ----

	  streamlog_out(DEBUG) << "Computing multiplicities" << std::endl;

	  if(hit.second.second != 0) {

	       layerMultiplicity[K].first++;
	       layerMultiplicity[K].second += multMap->at(hit.first)/(float)hit.second.second;

	       DIFMultiplicity[DIFID].first++;
	       DIFMultiplicity[DIFID].second += multMap->at(hit.first)/(float)hit.second.second;

	       ASICMultiplicity[ASICID].first++;
	       ASICMultiplicity[ASICID].second += multMap->at(hit.first)/(float)hit.second.second;
	  }

     }
     
     // ---- ASIC Efficiencies ----

     for(auto asic : ASICEfficiency) {

	  int K = (int)asic.first/144;
	  int ASICI = (asic.first%144)%12;
	  int ASICJ = (asic.first%144)/12;

	  statBoxes[(type + "ASICPerct_" + std::to_string(asic.first)).c_str()] = new TPaveText(ASICI + 0.5, ASICJ + 0.5, ASICI + 1, ASICJ + 1);
	  statBoxes[(type + "ASICPerct_" + std::to_string(asic.first)).c_str()]->SetName(("ASIC_" + std::to_string(asic.first)).c_str());
	  statBoxes[(type + "ASICPerct_" + std::to_string(asic.first)).c_str()]->AddText((std::to_string(asic.second.first*100/(float)layerEfficiency.at(K).first) + "%").c_str());
	       
	  if(histograms2D.find((type + "ASICEfficiency_Layer" + std::to_string(K)).c_str()) == histograms2D.end()) histograms2D[(type + "ASICEfficiency_Layer" + std::to_string(K)).c_str()] = new TH2F((type + "ASICEfficiency_Layer" + std::to_string(K)).c_str(), ";ASICI;ASICJ", 12, 0., 12., 12, 0., 12.);
	  histograms2D.at((type + "ASICEfficiency_Layer" + std::to_string(K)).c_str())->SetMaximum(100.);
	  histograms2D.at((type + "ASICEfficiency_Layer" + std::to_string(K)).c_str())->SetMinimum(-0.01);

	  histograms2D.at((type + "ASICEfficiency_Layer" + std::to_string(K)).c_str())->SetBinContent(ASICI + 1,ASICJ + 1,asic.second.second*100/(float)asic.second.first);
	  
     }

     // ---- ASIC Multiplicities ----

     for(auto asic : ASICMultiplicity) {

	  int K = (int)asic.first/144;
	  int ASICI = (asic.first%144)%12;
	  int ASICJ = (asic.first%144)/12;
	       
	  if(histograms2D.find((type + "ASICMultiplicity_Layer" + std::to_string(K)).c_str()) == histograms2D.end()) histograms2D[(type + "ASICMultiplicity_Layer" + std::to_string(K)).c_str()] = new TH2F((type + "ASICMultiplicity_Layer" + std::to_string(K)).c_str(), ";ASICI;ASICJ", 12, 0., 12., 12, 0., 12.);
	  histograms2D.at((type + "ASICMultiplicity_Layer" + std::to_string(K)).c_str())->SetMaximum(5.);
	  histograms2D.at((type + "ASICMultiplicity_Layer" + std::to_string(K)).c_str())->SetMinimum(-0.01);

	  histograms2D.at((type + "ASICMultiplicity_Layer" + std::to_string(K)).c_str())->SetBinContent(ASICI + 1,ASICJ + 1,asic.second.second/(float)asic.second.first);
	  
     }

     // ---- DIFEfficiencies ----

     streamlog_out(DEBUG) << "Computing dif efficiencies" << std::endl;

     for(auto dif : DIFEfficiency) {
     
	  int K = (int)dif.first/3;
	  int iDIF = dif.first%3;

	  statBoxes[(type + "DIFPerct_" + std::to_string(dif.first)).c_str()] = new TPaveText(iDIF + 0.7, 0.9, iDIF + 0.99, 0.99);
	  statBoxes[(type + "DIFPerct_" + std::to_string(dif.first)).c_str()]->SetName(("DIF_" + std::to_string(dif.first)).c_str());
	  statBoxes[(type + "DIFPerct_" + std::to_string(dif.first)).c_str()]->AddText((std::to_string(dif.second.first*100/(float)layerEfficiency.at(K).first) + "%").c_str());
	       
	  if(histograms2D.find((type + "DIFEfficiency_Layer" + std::to_string(K)).c_str()) == histograms2D.end()) histograms2D[(type + "DIFEfficiency_Layer" + std::to_string(K)).c_str()] = new TH2F((type + "DIFEfficiency_Layer" + std::to_string(K)).c_str(), ";DIF;", 3, 0., 3., 1, 0., 1.);
	  
	  histograms2D.at((type + "DIFEfficiency_Layer" + std::to_string(K)).c_str())->SetMaximum(100.);
	  histograms2D.at((type + "DIFEfficiency_Layer" + std::to_string(K)).c_str())->SetMinimum(-0.01);

	  histograms2D.at((type + "DIFEfficiency_Layer" + std::to_string(K)).c_str())->SetBinContent(iDIF + 1,1,dif.second.second*100/(float)dif.second.first);
	 
     }
     
     // ---- DIFMultiplicities ----

     streamlog_out(DEBUG) << "Computing dif multiplicities" << std::endl;

     for(auto dif : DIFMultiplicity) {
     
	  int K = (int)dif.first/3;
	  int iDIF = dif.first%3;
	  
	  if(histograms2D.find((type + "DIFMultiplicity_Layer" + std::to_string(K)).c_str()) == histograms2D.end()) histograms2D[(type + "DIFMultiplicity_Layer" + std::to_string(K)).c_str()] = new TH2F((type + "DIFMultiplicity_Layer" + std::to_string(K)).c_str(), ";DIF;", 3, 0., 3., 1, 0., 1.);
	  histograms2D.at((type + "DIFMultiplicity_Layer" + std::to_string(K)).c_str())->SetMaximum(5.);
	  histograms2D.at((type + "DIFMultiplicity_Layer" + std::to_string(K)).c_str())->SetMinimum(-0.01);

	  histograms2D.at((type + "DIFMultiplicity_Layer" + std::to_string(K)).c_str())->SetBinContent(iDIF + 1,1,dif.second.second/(float)dif.second.first);

     }

     // ---- LayerEfficiencies ----


     std::vector<float> k, eff, mult, stats;
     k = eff = mult = stats = {};

     float meanLayerEff, meanLayerMult;
     meanLayerEff = meanLayerMult = 0.;

     int nValidMult = 0;	  

     /*for(int iLayer = 0; iLayer < 49; iLayer++) {
	  
	  float layerEff, layerMult;
	  layerEff = layerMult = 0.;

	  for(int iASIC = 0; iASIC < 144; iASIC++) {
	       int ASICID = iLayer*144 + iASIC;
	       if(ASICEfficiency.find(ASICID) != ASICEfficiency.end()) {
		    layerEff += ASICEfficiency.at(ASICID).second*100/(float)ASICEfficiency.at(ASICID).first;
	       }
	       if(ASICMultiplicity.find(ASICID) != ASICMultiplicity.end()) {
		    layerMult += ASICMultiplicity.at(ASICID).second/(float)ASICMultiplicity.at(ASICID).first; 
		    nValidMult++;
	       }  
	  }

	  layerEff /= 144;
	  layerMult /= nValidMult;

	  k.push_back(iLayer);
	  eff.push_back(layerEff);
	  mult.push_back(layerMult);

	  layerEfficiency[iLayer] = layerEff;
	  layerMultiplicity[iLayer] = layerMult;

	  meanLayerEff += layerEff;
	  meanLayerMult += layerMult;

     }*/

     for(auto layer : layerEfficiency) {
	  k.push_back(layer.first);
	  eff.push_back(layer.second.second*100/(float)layer.second.first);
	  if(layerMultiplicity.find(layer.first) != layerMultiplicity.end()) {
	       mult.push_back(layerMultiplicity.at(layer.first).second/(float)layerMultiplicity.at(layer.first).first);
	       nValidMult++;
	      
	       meanLayerMult += layerMultiplicity.at(layer.first).second/(float)layerMultiplicity.at(layer.first).first;
	  }
	  else { mult.push_back(0.); }
	  stats.push_back(layer.second.first);
	  
	  meanLayerEff += layer.second.second*100/(float)layer.second.first;
     }

     if(k.size() == 0) return;

     meanLayerEff /= k.size();
     meanLayerMult /= nValidMult;

     float effStd, multStd;
     effStd = multStd = 0;

     for(auto layer : layerEfficiency) {
	  effStd += TMath::Power(meanLayerEff - layer.second.second*100/(float)layer.second.first,2);
	  if(layerMultiplicity.find(layer.first) != layerMultiplicity.end()) {
	       multStd += TMath::Power(meanLayerMult - layerMultiplicity.at(layer.first).second/(float)layerMultiplicity.at(layer.first).first,2);
	  }
     }

     effStd = TMath::Sqrt(effStd/k.size());
     multStd = TMath::Sqrt(multStd/nValidMult);

     streamlog_out(DEBUG) << "Creating layer efficiencies graph" << std::endl;

     graphs[(type + "LayersEfficiency").c_str()] = new TGraph(k.size(), k.data(), eff.data());
     graphs[(type + "LayersEfficiency").c_str()]->SetTitle(";Layer;Eff(%)");
     graphs[(type + "LayersEfficiency").c_str()]->SetName((type + "LayersEfficiency").c_str());

     TF1* hLine = new TF1("MeanLayerEfficiency", "[0]", k.front(), k.back());
     hLine->SetParameter(0,meanLayerEff);
     hLine->SetParError(0,effStd);
     hLine->SetLineColor(kGreen);
     graphs[(type + "LayersEfficiency").c_str()]->GetListOfFunctions()->Add(hLine);
     
     graphs[(type + "LayersEfficiency")]->SetMaximum(100.);
     graphs[(type + "LayersEfficiency")]->SetMinimum(0.);
     
     streamlog_out(DEBUG) << "Computing layer multiplicities" << std::endl;
     
     graphs[(type + "LayersMultiplicity").c_str()] = new TGraph(k.size(), k.data(), mult.data());
     graphs[(type + "LayersMultiplicity").c_str()]->SetTitle(";Layer;Mult(nHit)");
     graphs[(type + "LayersMultiplicity").c_str()]->SetName((type + "LayersMultiplicity").c_str());
     
     TF1* hLineMult = new TF1("MeanLayerMultiplicity","[0]", k.front(), k.back()); 
     hLineMult->SetParameter(0,meanLayerMult);
     hLineMult->SetParError(0,multStd);
     hLine->SetLineColor(kGreen);
     graphs[(type + "LayersMultiplicity").c_str()]->GetListOfFunctions()->Add(hLineMult);
     
     graphs[(type + "LayersMultiplicity").c_str()]->SetMaximum(3.);
     graphs[(type + "LayersMultiplicity").c_str()]->SetMinimum(0.);
     
     streamlog_out(DEBUG) << "Creating layer stats" << std::endl;
     
     graphs[(type + "LayersStats").c_str()] = new TGraph(k.size(), k.data(), stats.data());
     graphs[(type + "LayersStats").c_str()]->SetTitle(";Layer;Entries");
     graphs[(type + "LayersStats").c_str()]->SetName((type + "LayersStats").c_str());
     
}

void MuonAnalysisProc::writeHistograms() {

  streamlog_out(DEBUG) << "Writing 2D histograms" << std::endl;

  for(auto It2D = histograms2D.begin(); It2D != histograms2D.end(); It2D++) {

       std::string prefix;
       if(It2D->first.find("Run") != std::string::npos) prefix = std::to_string(runNumber) + "/Run";
       else prefix = "Total";

       if(It2D->first.find("HitEfficiency",0) != std::string::npos) {
	    _outputFile->cd((prefix + "HitEfficiencies").c_str());
       }
       else if(It2D->first.find("Stats",0) != std::string::npos) {
	 _outputFile->cd((prefix + "Stats").c_str());
       }
       else if(It2D->first.find("ASICEfficiency",0) != std::string::npos) {
	    _outputFile->cd((prefix + "ASICEfficiencies").c_str());
       }
       else if(It2D->first.find("ASICMultiplicity",0) != std::string::npos) {
	    _outputFile->cd((prefix + "ASICMultiplicities").c_str());
       }
       else if(It2D->first.find("DIFEfficiency",0) != std::string::npos) {
	 _outputFile->cd((prefix + "DIFEfficiencies").c_str());
       }
       else if(It2D->first.find("DIFMultiplicity",0)  != std::string::npos) {
	 _outputFile->cd((prefix + "DIFMultiplicities").c_str());
       }
       It2D->second->Write();
       delete It2D->second;
  }

  histograms2D.clear();

  for(auto box : statBoxes) {
       std::string prefix;
       if(box.first.find("Run") != std::string::npos) prefix = std::to_string(runNumber) + "/Run";
       else prefix = "Total";
  
       if(box.first.find("ASICPerct",0) != std::string::npos) {
	    _outputFile->cd((prefix + "ASICStatBoxes").c_str());
       }
       else if(box.first.find("DIFPerct",0) != std::string::npos) {
	    _outputFile->cd((prefix + "DIFStatBoxes").c_str());
       }
       box.second->Write();
       delete box.second;
  }

  statBoxes.clear();

  _outputFile->cd((std::to_string(runNumber)).c_str());

  streamlog_out(DEBUG) << "Writing 1D histograms" << std::endl;

  for(auto It1D = histograms1D.begin(); It1D != histograms1D.end(); It1D++) {
    It1D->second->Write();
    delete It1D->second;
  }

  histograms1D.clear();

  streamlog_out(DEBUG) << "Writing graphs" << std::endl;

  for(auto ItGraphs = graphs.begin(); ItGraphs != graphs.end(); ItGraphs++) {

       std::string prefix;
       if(ItGraphs->first.find("Run") != std::string::npos) prefix = std::to_string(runNumber);
       else prefix = "";

       _outputFile->cd(prefix.c_str());

       ItGraphs->second->Write();
       delete ItGraphs->second;
  }
  
  graphs.clear();

}
