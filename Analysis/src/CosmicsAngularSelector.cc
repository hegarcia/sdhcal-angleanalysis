#include "CosmicsAngularSelector.h"

#include <UTIL/CellIDDecoder.h>

#include <EVENT/LCCollection.h>
#include <EVENT/CalorimeterHit.h>

#include <IMPL/LCEventImpl.h>
#include <IMPL/LCCollectionVec.h>
#include <IMPL/CalorimeterHitImpl.h>

#include "TPaveText.h"
#include "TGraph.h"
#include "TF1.h"

#include <iostream>

CosmicsAngularSelectorProc a_CosmicsAngularSelectorProc;

//=========================================================
CosmicsAngularSelectorProc::CosmicsAngularSelectorProc()
  :Processor("CosmicsAngularSelectorProc")
{
  
  //Input Collections    
  _hcalCollections.push_back(std::string("SDHCAL_ShowerEvents"));
  registerInputCollections(LCIO::RAWCALORIMETERHIT, 
			   "HCALCollections",  
			   "HCAL Collection Names",  
			   _hcalCollections, 
			   _hcalCollections); 
  
  //Output file base name
  _outFileName="TB_SDHCALCleanedShowerEvents_";
  registerProcessorParameter("LCIOOutputFile", 
			     "Base LCIO file name",
			     _outFileName,
			     _outFileName);

  //Output collection name
  _outColName="SDHCAL_CleanShowerEvents";
  registerProcessorParameter("OutputCollectionName", 
			     "Name of the produced collection in this processor",
			     _outColName,
			     _outColName);

  
  //Output ROOT name
  _logRootName = "LogROOT_CosmicsAngularSelection";
  registerProcessorParameter("logRoot_Name" ,
                             "LogRoot name",
                             _logRootName,
			     _logRootName);

  
  //Cut in the X slope to select cosmics
  _slopeXCut = 0.1;
  registerProcessorParameter("SlopeXCut",
                             "Cut in the X slope to select cosmics",
                             _slopeXCut,
			     _slopeXCut);

  //Cut in the Y slope to select cosmics
  _slopeYCut = 0.1;
  registerProcessorParameter("SlopeYCut",
                             "Cut in the Y slope to select cosmics",
                             _slopeYCut,
			     _slopeYCut);

} 

void CosmicsAngularSelectorProc::init() {

     streamlog_out(DEBUG5) << "CosmicsAngularSelector Init" << std::endl;

     _lcWriter = LCFactory::getInstance()->createLCWriter() ;
     _lcWriter->setCompressionLevel( 0 );

     _outputFile = new TFile((_logRootName + ".root").c_str(),"RECREATE");	  
     
     printParameters();
  
}


void CosmicsAngularSelectorProc::processRunHeader( LCRunHeader* runHd )
{

  streamlog_out(MESSAGE) << "CosmicsAngularSelector processing run: " << runHd->getRunNumber() << std::endl;

  if(runNumber != -1) {
    _lcWriter->close();
    writeHistograms();
    sysTool->WriteSystematics();
    delete sysTool;
  }
  runNumber = runHd->getRunNumber();

  // ---- Creation of the output files ----
  
  streamlog_out(DEBUG3) << "New lcio output" << std::endl;

  _lcWriter->open((_outFileName + std::to_string(runNumber) + ".slcio").c_str(),LCIO::WRITE_NEW);
  _lcWriter->writeRunHeader(runHd);
	 
  streamlog_out(DEBUG3) << "Creating new local histograms" << std::endl;

  _outputFile->mkdir(std::to_string(runNumber).c_str());
  _outputFile->cd(std::to_string(runNumber).c_str());

  // ---- Creation of the histograms ----

  histograms1D["NHits"] = new TH1F("NHits", "Number of hits;NHits;Entries", 500, 0., 5000.);
  histograms1D["NHitsOther"] = new TH1F("NHitsOther", "Number of hits;NHits;Entries", 500, 0., 5000.);
  histograms1D["NHitsCosmics"] = new TH1F("NHitsCosmics", "Number of hits;NHits;Entries", 500, 0., 5000.);

  histograms1D["SlopeX"] = new TH1F("SlopeX", ";SlopeX;Entries", 200, -2., 2.);
  histograms1D["SlopeXOther"] = new TH1F("SlopeXOther", ";SlopeX;Entries", 200, -2., 2.);
  histograms1D["SlopeXCosmics"] = new TH1F("SlopeXCosmics", ";SlopeX;Entries", 200, -2., 2.);  

  histograms1D["SlopeY"] = new TH1F("SlopeY", ";SlopeY;Entries", 200, -2., 2.);
  histograms1D["SlopeYOther"] = new TH1F("SlopeYOther", ";SlopeY;Entries", 200, -2., 2.);
  histograms1D["SlopeYCosmics"] = new TH1F("SlopeYCosmics", ";SlopeY;Entries", 200, -2., 2.);  

  histograms1D["ChiSquareX"] = new TH1F("ChiSquareX", ";ChiSquare;Entries", 1000, 0., 50000.);  
  histograms1D["ChiSquareY"] = new TH1F("ChiSquareY", ";ChiSquare;Entries", 1000, 0., 50000.);  

  histograms1D["NLayers"] = new TH1F("NLayers", ";NLayers;Entries", 10, 0., 10.);  

  histograms2D["SlopeXvsSlopeY"] = new TH2F("SlopeXvsSlopeY", ";SlopeX;SlopeY", 200, -2., 2., 200, -2., 2.);
  histograms2D["ChivsSlopeX"] = new TH2F("ChivsSlopeX", ";ChiSquare;SlopeX", 1000, 0., 50000., 200, -2., 2.);
  histograms2D["ChivsSlopeY"] = new TH2F("ChivsSlopeY", ";ChiSquare;SlopeY", 1000, 0., 50000., 200, -2., 2.);

  // ---- Systematics ----

  sysTool = new SystematicsTool(("Systematics_CosmicsAngularSelector_" + std::to_string(runNumber) + ".txt").c_str());
  sysTool->AddCut("CosmicsAngular");
  
  // ---- Reset counters ----

  evtnum = nOther = nCosmics = 0;
  
}

void CosmicsAngularSelectorProc::processEvent( LCEvent * evtP ) 
{

  streamlog_out(DEBUG5) << "Start CosmicsAngularSelector process" << std::endl;

  if(evtP){ //Check the evtP
    for(unsigned int i=0; i < _hcalCollections.size(); i++){//loop over collections
      try{
	
	LCCollection* col = evtP->getCollection(_hcalCollections[i].c_str());
	int nHits = col->getNumberOfElements();

	CellIDDecoder<CalorimeterHit> cd("I:7,J:7,K:10,Dif_id:8,Asic_id:6,Chan_id:7");
	
	IMPL::LCCollectionVec* outcolnew = new IMPL::LCCollectionVec(LCIO::CALORIMETERHIT);        
	outcolnew->setDefault();
	outcolnew->setFlag(outcolnew->getFlag() | (1 << LCIO::RCHBIT_LONG));

	IMPL::LCCollectionVec* eventcolnew = new IMPL::LCCollectionVec(LCIO::CALORIMETERHIT);
	eventcolnew->setFlag(col->getFlag());
	eventcolnew->setSubset(true);

	std::vector<int> timeParameters = {};
	col->parameters().getIntVals("TimeParameters",timeParameters);

	outcolnew->parameters().setValues("TimeParameters", timeParameters);
	outcolnew->parameters().setValue("TimeInSpill", col->parameters().getFloatVal("TimeInSpill"));

	eventcolnew->parameters().setValues("TimeParameters", timeParameters);
	eventcolnew->parameters().setValue("TimeInSpill", col->parameters().getFloatVal("TimeInSpill"));

	std::map<int, std::vector<const float*>> trackComponents = {};
	
	for(int iHit = 0; iHit < nHits; iHit++) {
	  
	  CalorimeterHit* hit = dynamic_cast<CalorimeterHit*>(col->getElementAt(iHit));

	  int K = (int)cd(hit)["K"];

	  if(K < 10) trackComponents[K].push_back(hit->getPosition());

	  CalorimeterHitImpl* copyHit = new CalorimeterHitImpl(*(static_cast<CalorimeterHitImpl*>(hit)));
	  outcolnew->addElement(copyHit);
	  eventcolnew->addElement(hit);
    
	}

	histograms1D.at("NLayers")->Fill(trackComponents.size());
	
	TGraph* fitGraphX = new TGraph(trackComponents.size());
	TGraph* fitGraphY = new TGraph(trackComponents.size());
	
	int iPoint = 0;
	for(auto kIt = trackComponents.begin(); kIt != trackComponents.end(); kIt++) {

	  float meanX, meanY, meanZ;
	  meanX = meanY = meanZ = 0.;

	  int nHitsInLayer = kIt->second.size();
	  for(auto hitIt = kIt->second.begin(); hitIt != kIt->second.end(); hitIt++) {
	    meanX += (*hitIt)[0];
	    meanY += (*hitIt)[1];
	    meanZ += (*hitIt)[2];
	  }

	  meanX /= nHitsInLayer;
	  meanY /= nHitsInLayer;
	  meanZ /= nHitsInLayer;
	  
	  fitGraphX->SetPoint(iPoint, meanZ, meanX);
	  fitGraphY->SetPoint(iPoint, meanZ, meanY);

	  iPoint++;
	}

	// This cut is for the MC samples in which the beam selector has not been applied yet
	if(iPoint < 2) {
	  streamlog_out(DEBUG5) << "No hits to create a shower" << std::endl;

	  delete fitGraphX;
	  delete fitGraphY;
	
	  return;
	}

	fitGraphX->Fit("pol1", "Q");
	fitGraphY->Fit("pol1", "Q");

	float slopeX = (fitGraphX->GetFunction("pol1"))->GetParameters()[1];
	float chiSquareX = (fitGraphX->GetFunction("pol1"))->GetChisquare()/(fitGraphX->GetFunction("pol1"))->GetNDF();      
	
	float slopeY = (fitGraphY->GetFunction("pol1"))->GetParameters()[1];
	float chiSquareY = (fitGraphY->GetFunction("pol1"))->GetChisquare()/(fitGraphY->GetFunction("pol1"))->GetNDF();

	delete fitGraphX;
	delete fitGraphY;

	histograms1D.at("NHits")->Fill(nHits);
	histograms1D.at("SlopeX")->Fill(slopeX);
	histograms1D.at("ChiSquareX")->Fill(chiSquareX);

	histograms1D.at("SlopeY")->Fill(slopeY);
	histograms1D.at("ChiSquareY")->Fill(chiSquareY);

	histograms2D.at("SlopeXvsSlopeY")->Fill(slopeX, slopeY);
	histograms2D.at("ChivsSlopeX")->Fill(chiSquareX, slopeX);
	histograms2D.at("ChivsSlopeY")->Fill(chiSquareY, slopeY);

	int nHitsOther, nHitsOtherUp, nHitsOtherDown;
	nHitsOther = nHitsOtherUp = nHitsOtherDown = -999;

	evtnum++;
	
	if(TMath::Abs(slopeX) < _slopeXCut*1.1 && TMath::Abs(slopeY) < _slopeYCut*1.1) nHitsOtherUp = nHits;
	if(TMath::Abs(slopeX) < _slopeXCut*0.9 && TMath::Abs(slopeY) < _slopeYCut*0.9) nHitsOtherDown = nHits;
	  
	
	if(TMath::Abs(slopeX) < _slopeXCut && TMath::Abs(slopeY) < _slopeYCut) {
	//if(TMath::Abs(slopeY) <= -_slopeYCut*slopeX/_slopeXCut + _slopeYCut) {
	  nOther++;
	  nHitsOther = nHits;
	  histograms1D.at("NHitsOther")->Fill(nHits);
	  histograms1D.at("SlopeXOther")->Fill(slopeX);
	  histograms1D.at("SlopeYOther")->Fill(slopeY);

	  LCEventImpl* evt = new LCEventImpl();
	  evt->setRunNumber(evtP->getRunNumber());
	  evt->setTimeStamp(evtP->getTimeStamp());
	  	  
	  evt->setEventNumber(nOther);
	  evt->addCollection(outcolnew,_outColName);

	  evtP->addCollection(eventcolnew,_outColName);

	  _lcWriter->writeEvent(evt);
	  delete evt;

	}
	else {
	  nCosmics++;
	  histograms1D.at("NHitsCosmics")->Fill(nHits);
	  histograms1D.at("SlopeXCosmics")->Fill(slopeX);
	  histograms1D.at("SlopeYCosmics")->Fill(slopeY);

	  delete outcolnew;
	  delete eventcolnew;
	}

	sysTool->FillCut("CosmicsAngular", nHitsOther, nHitsOtherUp, nHitsOtherDown);
	
      }catch (lcio::DataNotAvailableException zero) { streamlog_out(DEBUG) << "ERROR READING COLLECTION: " << _hcalCollections[i] << std::endl;} 
    }//end loop over collection
  }
  
}


//==============================================================
void CosmicsAngularSelectorProc::end()
{

  _lcWriter->close();  
  delete _lcWriter;
  writeHistograms();

  sysTool->WriteSystematics();
  delete sysTool;
  
  _outputFile->Close();
  delete _outputFile;

  streamlog_out(DEBUG3) << "Ending CosmicsAngularSelector" << std::endl;

}


void CosmicsAngularSelectorProc::writeHistograms() {

  _outputFile->cd(std::to_string(runNumber).c_str());
  
  TPaveText stats(0.05, 0.1, 0.95, 0.9);
  stats.AddText(("Percentage of cosmic rays:" + std::to_string(nCosmics*100/(float)evtnum) + "%").c_str());
  stats.AddText(("Percentage of other beam particles:" + std::to_string(nOther*100/(float)evtnum) + "%").c_str());
  stats.Write();

  for(auto It1D = histograms1D.begin(); It1D != histograms1D.end(); It1D++) {
    It1D->second->Write();
    delete It1D->second;
  }

  for(auto It2D = histograms2D.begin(); It2D != histograms2D.end(); It2D++) {
    It2D->second->Write();
    delete It2D->second;
  }
  
}

 
