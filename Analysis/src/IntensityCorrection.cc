#include "IntensityCorrection.h"

#include <EVENT/LCCollection.h>
#include <EVENT/CalorimeterHit.h>

#include "TPaveText.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TF1.h"

#include <iostream>
#include <cmath>

IntensityCorrectionProc a_IntensityCorrectionProc;

//=========================================================
IntensityCorrectionProc::IntensityCorrectionProc()
  :Processor("IntensityCorrectionProc")
{
  
  //Input Collections    
  _hcalCollections.push_back(std::string("SDHCAL_PionEvents"));
  registerInputCollections(LCIO::RAWCALORIMETERHIT, 
			   "HCALCollections",  
			   "HCAL Collection Names",  
			   _hcalCollections, 
			   _hcalCollections); 
  
  //Output ROOT name
  _logRootName = "LogROOT_IntensityCorrection";
  registerProcessorParameter("logRoot_Name" ,
                             "LogRoot name",
                             _logRootName,
			     _logRootName);

  
  //Correction file name
  _correctionFileBaseName = "IntensityCorrection_";
  registerProcessorParameter("CorrectionFileBaseName" ,
                             "Base name for the correction file",
                             _correctionFileBaseName,
			     _correctionFileBaseName);

  //Correction parameters
  _angleCorrParameters = { 1.0 , 0.0 };
  registerProcessorParameter("AngleCorrectionParameters" ,
                             "Angle correction and error",
                             _angleCorrParameters,
			     _angleCorrParameters);

  //Time bin size
  _binSize = 0.1;
  registerProcessorParameter("BinSize" ,
                             "Time bin size",
                             _binSize,
			     _binSize);

  

} 

void IntensityCorrectionProc::init() {

     streamlog_out(DEBUG3) << "IntensityCorrection Init" << std::endl;

     _outputFile = new TFile((_logRootName + ".root").c_str(),"RECREATE");	  
     
     printParameters();
  
}


void IntensityCorrectionProc::processRunHeader( LCRunHeader* runHd )
{


  if(runNumber != -1) {
    computeCorrection();
    writeHistograms();
  }
  runNumber = runHd->getRunNumber();

  streamlog_out(MESSAGE) << "IntensityCorrection processing run: " << runHd->getRunNumber() << std::endl;
  // ---- Creation of the histograms ----
  
  streamlog_out(DEBUG3) << "Creating new local histograms" << std::endl;

  _outputFile->mkdir(std::to_string(runNumber).c_str());
  _outputFile->cd(std::to_string(runNumber).c_str());

  histograms1D["NHits"] = new TH1F("NHits", ";NHits;Entries", 500, 0., 5000.);
  histograms1D["NHitsAngleCorr"] = new TH1F("NHitsAngleCorr", ";NHits;Entries", 500, 0., 5000.);
  histograms1D["SlopeX"] = new TH1F("SlopeX", ";SlopeX;Entries", 500, -3., 3.);
  histograms1D["TimeInSpill"] = new TH1F("TimeInSpill", ";T(s);Entries", 100, 0., 10.);
  
  streamlog_out(DEBUG3) << "Creating systematics tool" << std::endl;

  // ---- Reseting event ----

  nHitsMap.clear();
  
}

void IntensityCorrectionProc::processEvent( LCEvent * evtP ) 
{

  streamlog_out(DEBUG3) << "Start IntensityCorrection process" << std::endl;

  if(evtP){ //Check the evtP
    for(unsigned int i=0; i < _hcalCollections.size(); i++){//loop over collections
      try{

	LCCollection* col = evtP->getCollection(_hcalCollections[i].c_str());
	int nHits = col->getNumberOfElements();
	
	float timeInSpill = col->parameters().getFloatVal("TimeInSpill");
	if(timeInSpill > 15) return;
	
	std::vector<float> trkParameters;
	col->parameters().getFloatVals("TrackParameters",trkParameters);
	histograms1D["SlopeX"]->Fill(trkParameters.at(0));
	float angle = TMath::ATan(trkParameters.at(0));
        
	int point = (int)(timeInSpill/_binSize);

	int N1, N2, N3;
	N1 = N2 = N3 = 0;
	
	for(int iHit = 0; iHit < nHits; iHit++) {

	  CalorimeterHit* hit = dynamic_cast<CalorimeterHit*>(col->getElementAt(iHit));

	  int T = (int)hit->getEnergy();

	  switch(T) {
	  case 1:
	    N1++; break;
	  case 2:
	    N2++; break;
	  case 3:
	    N3++; break;
	  };
	  
	}
        
	nHitsMap[point][1].push_back(N1*TMath::Cos(_angleCorrParameters.at(0)*angle));	
	nHitsMap[point][2].push_back(N2*TMath::Cos(_angleCorrParameters.at(0)*angle));
	nHitsMap[point][3].push_back(N3*TMath::Cos(_angleCorrParameters.at(0)*angle));  

	histograms1D.at("NHits")->Fill(nHits);
	histograms1D.at("NHitsAngleCorr")->Fill(nHits*TMath::Cos(_angleCorrParameters.at(0)*angle));
	histograms1D.at("TimeInSpill")->Fill(timeInSpill);
	
      }catch (lcio::DataNotAvailableException zero) { streamlog_out(DEBUG) << "ERROR READING COLLECTION: " << _hcalCollections[i] << std::endl;} 
    }//end loop over collection
  }
  
}


//==============================================================
void IntensityCorrectionProc::end()
{

  computeCorrection();
  writeHistograms();

  _outputFile->Close();
  delete _outputFile;

  streamlog_out(DEBUG3) << "Ending IntensityCorrection" << std::endl;

}

void IntensityCorrectionProc::computeCorrection() {

  std::vector<float> x, ex, y1, y2, y3, ey1, ey2, ey3;
  x = y1 = y2 = y3 = ey1 = ey2 = ey3 = {};
  
  for(auto pointIt = nHitsMap.begin(); pointIt != nHitsMap.end(); pointIt++) {

    if(pointIt->first*_binSize < 1.0 || pointIt->second.at(1).size() < 10) continue;
    
    float meanN1, meanN2, meanN3, meanN1Err, meanN2Err, meanN3Err;
    meanN1 = meanN2 = meanN3 = meanN1Err = meanN2Err = meanN3Err = 0;
    
    int nEntriesN1 = (float)pointIt->second.at(1).size();
    int nEntriesN2 = (float)pointIt->second.at(2).size();
    int nEntriesN3 = (float)pointIt->second.at(3).size();

    for(auto valueIt = pointIt->second.at(1).begin(); valueIt != pointIt->second.at(1).end(); valueIt++) {
	 meanN1 += *valueIt;
	 meanN1Err += sqrt(*valueIt);
    }

    meanN1 /= (float)nEntriesN1;
    meanN1Err = meanN1Err/(float)nEntriesN1 + meanN1*sqrt(nEntriesN1)/(float)(nEntriesN1*nEntriesN1);
   
    for(auto valueIt = pointIt->second.at(2).begin(); valueIt != pointIt->second.at(2).end(); valueIt++) {
	 meanN2 += *valueIt;
	 meanN2Err += sqrt(*valueIt);
    }

    meanN2 /= (float)nEntriesN2;   
    meanN2Err = meanN2Err/(float)nEntriesN2 + meanN2*sqrt(nEntriesN2)/(float)(nEntriesN2*nEntriesN2);

    for(auto valueIt = pointIt->second.at(3).begin(); valueIt != pointIt->second.at(3).end(); valueIt++) {
      meanN3 += *valueIt;
      meanN3Err += sqrt(*valueIt);
    }

    meanN3 /= (float)nEntriesN3;
    meanN3Err = meanN3Err/(float)nEntriesN3 + meanN3*sqrt(nEntriesN3)/(float)(nEntriesN3*nEntriesN3);

    x.push_back(pointIt->first*_binSize);
    ex.push_back(_binSize/2.0);

    y1.push_back(meanN1);
    ey1.push_back(meanN1Err);

    y2.push_back(meanN2);
    ey2.push_back(meanN2Err);
    
    y3.push_back(meanN3);
    ey3.push_back(meanN3Err);

  }

  if(x.size() > 1) {

       streamlog_out(DEBUG3) << "Enough points to fit the intensity correction" << std::endl;

       fitGraphN1 = new TGraphErrors(x.size(),x.data(),y1.data(), ex.data(), ey1.data());
       
       fitGraphN1->SetTitle(";T(s);MeanNHits");
       fitGraphN1->SetMaximum(1500.);
       fitGraphN1->SetMinimum(0.);
       fitGraphN1->Fit("pol1","Q");
       
       fitGraphN2 = new TGraphErrors(x.size(),x.data(),y2.data(), ex.data(), ey2.data());
       fitGraphN2->SetTitle(";T(s);MeanNHits");
       fitGraphN2->SetMaximum(1500.);
       fitGraphN2->SetMinimum(0.);
       fitGraphN2->Fit("pol1","Q");
       
       fitGraphN3 = new TGraphErrors(x.size(),x.data(),y3.data(), ex.data(), ey3.data());
       fitGraphN3->SetTitle(";T(s);MeanNHits");
       fitGraphN3->SetMaximum(1500.);
       fitGraphN3->SetMinimum(0.);
       fitGraphN3->Fit("pol1","Q");

       correctionFile.open((_correctionFileBaseName + std::to_string(runNumber) + ".txt").c_str(), std::ofstream::trunc);
       correctionFile << "Threshold Corr Error" << std::endl;
       correctionFile << "1 " << fitGraphN1->GetFunction("pol1")->GetParameter(1) << " " << fitGraphN1->GetFunction("pol1")->GetParError(1) << std::endl;
       correctionFile << "2 " << fitGraphN2->GetFunction("pol1")->GetParameter(1) << " " << fitGraphN2->GetFunction("pol1")->GetParError(1) << std::endl;
       correctionFile << "3 " << fitGraphN3->GetFunction("pol1")->GetParameter(1) << " " << fitGraphN3->GetFunction("pol1")->GetParError(1) << std::endl;
       
       std::cout  << "Threshold Corr Error" << std::endl;
       std::cout  << "1 " << fitGraphN1->GetFunction("pol1")->GetParameter(1) << " " << fitGraphN1->GetFunction("pol1")->GetParError(1) << std::endl;
       std::cout  << "2 " << fitGraphN2->GetFunction("pol1")->GetParameter(1) << " " << fitGraphN2->GetFunction("pol1")->GetParError(1) << std::endl;
       std::cout  << "3 " << fitGraphN3->GetFunction("pol1")->GetParameter(1) << " " << fitGraphN3->GetFunction("pol1")->GetParError(1) << std::endl;
       
       correctionFile.close();
  }  

}


void IntensityCorrectionProc::writeHistograms() {

  streamlog_out(DEBUG3) << "Writting histograms" << std::endl;

  _outputFile->cd(std::to_string(runNumber).c_str());
  
  TPaveText stats(0.05, 0.1, 0.95, 0.9);
  for(auto pointIt = nHitsMap.begin(); pointIt != nHitsMap.end(); pointIt++) {
    stats.AddText(("Stats in point " + std::to_string(pointIt->first) + " = " + std::to_string(pointIt->second.at(1).size())).c_str());
  }
  stats.Write();

  if(fitGraphN1 != nullptr && fitGraphN2 != nullptr && fitGraphN3 != nullptr) {
       TCanvas* canvasN1 = new TCanvas("N1");
       fitGraphN1->Draw("APE*");
       canvasN1->Write();
  
       TCanvas* canvasN2 = new TCanvas("N2");
       fitGraphN2->Draw("APE*");
       canvasN2->Write();
  
       TCanvas* canvasN3 = new TCanvas("N3");
       fitGraphN3->Draw("APE*");
       canvasN3->Write();

       delete canvasN1;
       delete canvasN2;
       delete canvasN3;

       fitGraphN1 = nullptr;
       fitGraphN2 = nullptr;
       fitGraphN3 = nullptr;
  }
  
  for(auto It1D = histograms1D.begin(); It1D != histograms1D.end(); It1D++) {
    It1D->second->Write();
    delete It1D->second;
  }

  /*
  for(auto It2D = histograms2D.begin(); It2D != histograms2D.end(); It2D++) {
    It2D->second->Write();
    delete It2D->second;
    }*/
  
}

 
