#include "BeamParticleSelector.h"

#include <UTIL/CellIDDecoder.h>

#include <EVENT/LCCollection.h>
#include <EVENT/CalorimeterHit.h>

#include <IMPL/LCEventImpl.h>
#include <IMPL/LCCollectionVec.h>
#include <IMPL/CalorimeterHitImpl.h>

#include "TPaveText.h"
#include "TGraph.h"
#include "TF1.h"

#include <iostream>

BeamParticleSelectorProc a_BeamParticleSelectorProc;

//=========================================================
BeamParticleSelectorProc::BeamParticleSelectorProc()
  :Processor("BeamParticleSelectorProc")
{
  
  //Input Collections    
  _hcalCollections.push_back(std::string("SDHCAL_Events"));
  registerInputCollections(LCIO::RAWCALORIMETERHIT, 
			   "HCALCollections",  
			   "HCAL Collection Names",  
			   _hcalCollections, 
			   _hcalCollections); 
  
  //Output file base name
  _outFileName="TB_SDHCALBeamEvents_";
  registerProcessorParameter("LCIOOutputFile", 
			     "Base LCIO file name",
			     _outFileName,
			     _outFileName);

  //Output collection name
  _outColName="SDHCAL_BeamEvents";
  registerProcessorParameter("OutputCollectionName", 
			     "Name of the produced collection in this processor",
			     _outColName,
			     _outColName);

  
  //Output ROOT name
  _logRootName = "LogROOT_BeamSelection";
  registerProcessorParameter("logRoot_Name" ,
                             "LogRoot name",
                             _logRootName,
			     _logRootName);

  
  //Last layer that defines the beginning of the detector to scan for hits
  _lastScanLayer = 10;
  registerProcessorParameter("LastScanLayer",
                             "Last layer that defines the beginning of the detector to scan for hits",
                             _lastScanLayer,
			     _lastScanLayer);


  //Cut in the number of hits at the beginning of the detector scan
  _nLayersScanCut = 5;
  registerProcessorParameter("NLayersScanCut",
                             "Cut in the number of hits at the beginning of the detector scan",
                             _nLayersScanCut,
			     _nLayersScanCut);

} 

void BeamParticleSelectorProc::init() {

     streamlog_out(DEBUG3) << "BeamParticleSelector Init" << std::endl;

     _lcWriter = LCFactory::getInstance()->createLCWriter() ;
     _lcWriter->setCompressionLevel( 0 );

     _outputFile = new TFile((_logRootName + ".root").c_str(),"RECREATE");	  
     
     printParameters();
  
}


void BeamParticleSelectorProc::processRunHeader( LCRunHeader* runHd )
{

  streamlog_out(MESSAGE) << "BeamParticleSelector processing run: " << runHd->getRunNumber() << std::endl;

  if(runNumber != -1) {
    _lcWriter->close();
    writeHistograms();
    sysTool->WriteSystematics();
    delete sysTool;
  }
  runNumber = runHd->getRunNumber();

  // ---- Creation of the output files ----
  
  streamlog_out(DEBUG3) << "New lcio output" << std::endl;

  _lcWriter->open((_outFileName + std::to_string(runNumber) + ".slcio").c_str(),LCIO::WRITE_NEW);
  _lcWriter->writeRunHeader(runHd);
  streamlog_out(DEBUG3) << "Creating new local histograms" << std::endl;

  _outputFile->mkdir(std::to_string(runNumber).c_str());
  _outputFile->cd(std::to_string(runNumber).c_str());

  // ---- Creation of the histograms ----

  histograms1D["NHits"] = new TH1F("NHits", "Number of hits;NHits;Entries", 500, 0., 5000.);
  histograms1D["NHitsDiscarded"] = new TH1F("NHitsDiscarded", "Number of hits;NHits;Entries", 500, 0., 5000.);
  histograms1D["NHitsBeam"] = new TH1F("NHitsBeam", "Number of hits;NHits;Entries", 500, 0., 5000.);

  
  histograms1D["NHitsInScan"] = new TH1F("NHitsInScan", "Number of hits;NHits;Entries", 500, 0., 1000.);
  
  histograms1D["NHitsInScanBeam"] = new TH1F("NHitsInScanBeam", "Number of hits;NHits;Entries", 500, 0., 1000.);
  histograms1D["NHitsInScanDiscarded"] = new TH1F("NHitsInScanBeamDiscarded", "Number of hits;NHits;Entries", 500, 0., 1000.);

  histograms1D["NHitsInFirstLayer"] = new TH1F("NHitsInFirstLayer", "Number of hits;NHits;Entries", 500, 0., 1000.);
  
  histograms1D["NHitsInFirstLayerBeam"] = new TH1F("NHitsInFirstLayerBeam", "Number of hits;NHits;Entries", 500, 0., 1000.);
  histograms1D["NHitsInFirstLayerDiscarded"] = new TH1F("NHitsInFirstLayerBeamDiscarded", "Number of hits;NHits;Entries", 500, 0., 1000.);

  
  histograms1D["FirstLayer"] = new TH1F("FirstLayer", ";FirstLayer;Entries", 51, 0., 51.);
  histograms1D["FirstLayerBeam"] = new TH1F("FirstLayerBeam", ";FirstLayer;Entries", 51, 0., 51.);
  histograms1D["FirstLayerDiscarded"] = new TH1F("FirstLayerDiscarded", ";FirstLayer;Entries", 51, 0., 51.);

  histograms1D["NLayersInScan"] = new TH1F("NLayersInScan", ";NLayers;Entries", 51, 0., 51.);
  histograms1D["NLayersInScanBeam"] = new TH1F("NLayersInScanBeam", ";NLayers;Entries", 51, 0., 51.);
  histograms1D["NLayersInScanDiscarded"] = new TH1F("NLayersInScanDiscarded", ";NLayers;Entries", 51, 0., 51.);

  
  histograms2D["FirstLayerVsNHitsInScan"] = new TH2F("FirstLayerVsNHitsInScan", ";FirstLayer;NHitsInScan", 51, 0., 51., 500, 0., 1000.);
  histograms2D["FirstLayerVsNHitsInScanBeam"] = new TH2F("FirstLayerVsNHitsInScanBeam", ";FirstLayer;NHitsInScan", 51, 0., 51., 500, 0., 1000.);
  histograms2D["FirstLayerVsNHitsInScanDiscarded"] = new TH2F("FirstLayerVsNHitsInScanDiscarded", ";FirstLayer;NHitsInScan", 51, 0., 51., 500, 0., 1000.);

  histograms1D["SlopeX"] = new TH1F("SlopeX", ";SlopeX;Entries", 2000, -10., 10.);
  histograms1D["SlopeY"] = new TH1F("SlopeY", ";SlopeY;Entries", 2000, -10., 10.);
  
  streamlog_out(DEBUG3) << "Creating systematics tool" << std::endl;

  sysTool = new SystematicsTool(("Systematics_BeamParticleSelector_" + std::to_string(runNumber) + ".txt").c_str());
  sysTool->AddCut("NHitsInScanCut");
    
  
  // ---- Reset counters and variables ----

  evtnum = nDiscarded = nBeamParticles = 0;
  
}

void BeamParticleSelectorProc::processEvent( LCEvent * evtP ) 
{

  streamlog_out(DEBUG3) << "Start BeamParticleSelector process" << std::endl;

  if(evtP){ //Check the evtP
    for(unsigned int i=0; i < _hcalCollections.size(); i++){//loop over collections
      try{

	evtnum++;
	
	LCCollection* col = evtP->getCollection(_hcalCollections[i].c_str());
	int nHits = col->getNumberOfElements();

	streamlog_out(DEBUG3) << "Initial number of hits: " << nHits << std::endl;
	
	CellIDDecoder<CalorimeterHit> cd("I:7,J:7,K:10,Dif_id:8,Asic_id:6,Chan_id:7");

	std::map<int,int> kDistribution = {};
	
	IMPL::LCCollectionVec* outcolnew = new IMPL::LCCollectionVec(LCIO::CALORIMETERHIT);
	outcolnew->setDefault();
	outcolnew->setFlag(outcolnew->getFlag() | (1 << LCIO::RCHBIT_LONG));

	IMPL::LCCollectionVec* eventcolnew = new IMPL::LCCollectionVec(LCIO::CALORIMETERHIT);
	eventcolnew->setFlag(col->getFlag());
	eventcolnew->setSubset(true);

	std::vector<int> timeParameters = {};
	col->parameters().getIntVals("TimeParameters",timeParameters);

	outcolnew->parameters().setValues("TimeParameters", timeParameters);
	outcolnew->parameters().setValue("TimeInSpill", col->parameters().getFloatVal("TimeInSpill"));

	eventcolnew->parameters().setValues("TimeParameters", timeParameters);
	eventcolnew->parameters().setValue("TimeInSpill", col->parameters().getFloatVal("TimeInSpill"));

	std::map<int, std::vector<const float*>> trackComponents = {};
	
	for(int iHit = 0; iHit < nHits; iHit++) {
	  
	  CalorimeterHit* hit = dynamic_cast<CalorimeterHit*>(col->getElementAt(iHit));

	  int K = (int)cd(hit)["K"];
	  kDistribution[K]++;

	  if(K < 10) trackComponents[K].push_back(hit->getPosition());

	  CalorimeterHitImpl* copyHit = new CalorimeterHitImpl(*(static_cast<CalorimeterHitImpl*>(hit)));
	  outcolnew->addElement(copyHit);
	  eventcolnew->addElement(hit);
    
	}
        
	TGraph* fitGraphX = new TGraph(trackComponents.size());
	TGraph* fitGraphY = new TGraph(trackComponents.size());
	
	int iPoint = 0;
	for(auto kIt = trackComponents.begin(); kIt != trackComponents.end(); kIt++) {

	  float meanX, meanY, meanZ;
	  meanX = meanY = meanZ = 0.;

	  int nHitsInLayer = kIt->second.size();
	  for(auto hitIt = kIt->second.begin(); hitIt != kIt->second.end(); hitIt++) {
	    meanX += (*hitIt)[0];
	    meanY += (*hitIt)[1];
	    meanZ += (*hitIt)[2];
	  }

	  meanX /= nHitsInLayer;
	  meanY /= nHitsInLayer;
	  meanZ /= nHitsInLayer;
	  
	  fitGraphX->SetPoint(iPoint, meanZ, meanX);
	  fitGraphY->SetPoint(iPoint, meanZ, meanY);

	  iPoint++;
	}

	float slopeX, X0, slopeY, Y0;
	slopeX = slopeY = X0 = Y0 = -999.;

        if(iPoint >= 2) {

	     fitGraphX->Fit("pol1", "Q");
	     fitGraphY->Fit("pol1", "Q");

	     slopeX = (fitGraphX->GetFunction("pol1"))->GetParameters()[1];
	     slopeY = (fitGraphY->GetFunction("pol1"))->GetParameters()[1];

	     X0 = (fitGraphX->GetFunction("pol1"))->GetParameters()[0];
	     Y0 = (fitGraphY->GetFunction("pol1"))->GetParameters()[0];
	}

	delete fitGraphX;
	delete fitGraphY;

	std::vector<float> trackParameters = { slopeX, X0, slopeY, Y0 };
	histograms1D.at("SlopeX")->Fill(slopeX);
	histograms1D.at("SlopeY")->Fill(slopeY);

	outcolnew->parameters().setValues("TrackParameters", trackParameters);
	eventcolnew->parameters().setValues("TrackParameters", trackParameters);
        
	int nHitsInFirstLayer, nHitsInScan, nLayersInScan, firstLayer = 999;
	nHitsInFirstLayer = nHitsInScan = nLayersInScan = 0;
	
	for(auto kIt = kDistribution.begin(); kIt != kDistribution.end(); kIt++) {
	  
	  if(kIt->first < firstLayer) {
	    firstLayer = kIt->first;
	    nHitsInFirstLayer = kIt->second;
	  }
	  if(kIt->first < _lastScanLayer) {
	    nHitsInScan += kIt->second;
	    nLayersInScan++;
	  }
	  
	}
	
	histograms1D.at("NHits")->Fill(nHits);
	histograms1D.at("NHitsInScan")->Fill(nHitsInScan);
	histograms1D.at("NHitsInFirstLayer")->Fill(nHitsInFirstLayer);
	histograms1D.at("NLayersInScan")->Fill(nLayersInScan);	
	histograms1D.at("FirstLayer")->Fill(firstLayer);
	histograms2D["FirstLayerVsNHitsInScan"]->Fill(firstLayer,nHitsInScan);

	int nHitsCut, nHitsUp, nHitsDown;
	nHitsCut = nHitsUp = nHitsDown = -999;
	
	if(nLayersInScan >= _nLayersScanCut - 1) nHitsUp = nHits;
	if(nLayersInScan >= _nLayersScanCut + 1) nHitsDown = nHits;
	
	if(nLayersInScan >= _nLayersScanCut && slopeX > -999 && slopeY > -999) {
	  nHitsCut = nHits;
	  nBeamParticles++;
	  
	  histograms1D.at("NHitsBeam")->Fill(nHits);
	  histograms1D.at("NHitsInScanBeam")->Fill(nHitsInScan);
	  histograms1D.at("NHitsInFirstLayerBeam")->Fill(nHitsInFirstLayer);
	  histograms1D.at("NLayersInScanBeam")->Fill(nLayersInScan);	
	  histograms1D.at("FirstLayerBeam")->Fill(firstLayer);
	  histograms2D["FirstLayerVsNHitsInScanBeam"]->Fill(firstLayer,nHitsInScan);

	  LCEventImpl* evt = new LCEventImpl();
	  evt->setRunNumber(evtP->getRunNumber());
	  evt->setTimeStamp(evtP->getTimeStamp());
	  evt->setEventNumber(nBeamParticles);

	  evt->addCollection(outcolnew,_outColName);
	  evtP->addCollection(eventcolnew,_outColName);	

	  _lcWriter->writeEvent(evt);
	  delete evt;
	  
	}
	else {
	  nDiscarded++;
	  
	  histograms1D.at("NHitsDiscarded")->Fill(nHits);
	  histograms1D.at("NHitsInScanDiscarded")->Fill(nHitsInScan);
	  histograms1D.at("NHitsInFirstLayerDiscarded")->Fill(nHitsInFirstLayer);
	  histograms1D.at("NLayersInScanDiscarded")->Fill(nLayersInScan);		  
	  histograms1D.at("FirstLayerDiscarded")->Fill(firstLayer);
	  histograms2D["FirstLayerVsNHitsInScanDiscarded"]->Fill(firstLayer,nHitsInScan);

	  delete outcolnew;
	  delete eventcolnew;
	  
	}

	sysTool->FillCut("NHitsInScanCut",nHitsCut, nHitsUp, nHitsDown);

      }catch (lcio::DataNotAvailableException zero) { streamlog_out(DEBUG) << "ERROR READING COLLECTION: " << _hcalCollections[i] << std::endl;} 
    }//end loop over collection
  }
  
}


//==============================================================
void BeamParticleSelectorProc::end()
{

  _lcWriter->close();  
  delete _lcWriter;
  writeHistograms();

  sysTool->WriteSystematics();
  delete sysTool;
  
  _outputFile->Close();
  delete _outputFile;

  streamlog_out(DEBUG3) << "Ending BeamParticleSelector" << std::endl;

}


void BeamParticleSelectorProc::writeHistograms() {

  _outputFile->cd(std::to_string(runNumber).c_str());
  
  TPaveText stats(0.05, 0.1, 0.95, 0.9);
  stats.AddText(("Percentage of Beam Particles:" + std::to_string(nBeamParticles*100/(float)evtnum) + "%").c_str());
  stats.AddText(("Percentage of Noise+Cosmics:" + std::to_string(nDiscarded*100/(float)evtnum) + "%").c_str());
  stats.Write();

  for(auto It1D = histograms1D.begin(); It1D != histograms1D.end(); It1D++) {
    It1D->second->Write();
    delete It1D->second;
  }

  for(auto It2D = histograms2D.begin(); It2D != histograms2D.end(); It2D++) {
    It2D->second->Write();
    delete It2D->second;
  }
  
}

 
