#include "ElectronSelector.h"

#include <UTIL/CellIDDecoder.h>

#include <EVENT/LCCollection.h>
#include <EVENT/CalorimeterHit.h>

#include <IMPL/LCEventImpl.h>
#include <IMPL/LCCollectionVec.h>
#include <IMPL/CalorimeterHitImpl.h>

#include "TPaveText.h"

#include <iostream>
#include <cmath>

ElectronSelectorProc a_ElectronSelectorProc;

//=========================================================
ElectronSelectorProc::ElectronSelectorProc()
  :Processor("ElectronSelectorProc")
{
  
  //Input Collections    
  _hcalCollections.push_back(std::string("SDHCAL_CleanShowerEvents"));
  registerInputCollections(LCIO::RAWCALORIMETERHIT, 
			   "HCALCollections",  
			   "HCAL Collection Names",  
			   _hcalCollections, 
			   _hcalCollections); 
  
  //Output file base name for electrons
  _outFileNameElectrons="TB_SDHCALElectronEvents_";
  registerProcessorParameter("LCIOOutputFileElectrons", 
			     "Base LCIO file name for electrons",
			     _outFileNameElectrons,
			     _outFileNameElectrons);

  //Output collection name for electrons
  _outColNameElectrons="SDHCAL_ElectronEvents";
  registerProcessorParameter("OutputCollectionNameElectrons", 
			     "Name of the produced collection in this processor for electrons",
			     _outColNameElectrons,
			     _outColNameElectrons);

  
  //Output file base name for pions
  _outFileNamePions="TB_SDHCALPionEvents_";
  registerProcessorParameter("LCIOOutputFilePions", 
			     "Base LCIO file name for pions",
			     _outFileNamePions,
			     _outFileNamePions);

  //Output collection name for pions
  _outColNamePions="SDHCAL_PionEvents";
  registerProcessorParameter("OutputCollectionNamePions", 
			     "Name of the produced collection in this processor for pions",
			     _outColNamePions,
			     _outColNamePions);

  //Output ROOT name
  _logRootName = "LogROOT_ElectronSelector";
  registerProcessorParameter("logRoot_Name" ,
                             "LogRoot name",
                             _logRootName,
			     _logRootName);

  
  //Shower start cut
  _showerStartCut = 10;
  registerProcessorParameter("ShowerStartCut",
                             "Cut to the start point of the shower to select electrons",
                             _showerStartCut,
			     _showerStartCut);

  //NLayers in the event cut
  _NLayersCut = 20;
  registerProcessorParameter("NLayersCut",
                             "Cut to the number of layers to select electrons",
                             _NLayersCut,
			     _NLayersCut);


  //NLayers in the event cut for leakage
  _NLayersLeakageCut = 40;
  registerProcessorParameter("NLayersLeakageCut",
                             "Cut to the number of layers to select leakage",
                             _NLayersLeakageCut,
			     _NLayersLeakageCut);

  
  //RMS in the event cut
  _RMSCut = 20.;
  registerProcessorParameter("RMSCut",
                             "Cut to global RMS to select electrons",
                             _RMSCut,
			     _RMSCut);

  
  //Density cut to select remaining noise or leakage
  _densityCut = 6.;
  registerProcessorParameter("DensityCut",
                             "Density cut to select remaining noise or leakage",
                             _densityCut,
			     _densityCut);

} 

void ElectronSelectorProc::init() {

     streamlog_out(DEBUG6) << "ElectronSelector Init" << std::endl;

     _lcWriterElectrons = LCFactory::getInstance()->createLCWriter() ;
     _lcWriterElectrons->setCompressionLevel( 0 );

     _lcWriterPions = LCFactory::getInstance()->createLCWriter() ;
     _lcWriterPions->setCompressionLevel( 0 );

     _outputFile = new TFile((_logRootName + ".root").c_str(),"RECREATE");	  
     
     printParameters();
  
}


void ElectronSelectorProc::processRunHeader( LCRunHeader* runHd )
{

  streamlog_out(MESSAGE) << "ElectronSelector processing run: " << runHd->getRunNumber() << std::endl;

  if(runNumber != -1) {
    _lcWriterElectrons->close();
    _lcWriterPions->close();
    writeHistograms();
    sysToolElectrons->WriteSystematics();
    sysToolPions->WriteSystematics();
    delete sysToolElectrons;
    delete sysToolPions;
  }
  runNumber = runHd->getRunNumber();

  // ---- Creation of the output files ----
  
  streamlog_out(DEBUG6) << "New lcio outputs" << std::endl;

  _lcWriterElectrons->open((_outFileNameElectrons + std::to_string(runNumber) + ".slcio").c_str(),LCIO::WRITE_NEW);
  _lcWriterElectrons->writeRunHeader(runHd);

  _lcWriterPions->open((_outFileNamePions + std::to_string(runNumber) + ".slcio").c_str(),LCIO::WRITE_NEW);
  _lcWriterPions->writeRunHeader(runHd);
  
  streamlog_out(DEBUG6) << "Creating new local histograms" << std::endl;

  _outputFile->mkdir(std::to_string(runNumber).c_str());
  _outputFile->cd(std::to_string(runNumber).c_str());

  // ---- Creation of the histograms ----

  histograms1D["NHits"] = new TH1F("NHits", ";NHits;Entries", 250, 0., 2000.); //
  histograms1D["NHitsElectrons"] = new TH1F("NHitsElectrons", ";NHits;Entries", 250, 0., 2000.);
  histograms1D["NHitsPions"] = new TH1F("NHitsPions", ";NHits;Entries", 250, 0., 2000.);
  histograms1D["NHitsDiscarded"] = new TH1F("NHitsDiscarded", ";NHits;Entries", 250, 0., 2000.);

  histograms2D["NHitsVsRMS"] = new TH2F("NHitsVsRMS", ";NHits;RMS",250, 0., 2000., 1000, 0., 500.);
  
  histograms2D["NHitsVsNLayers"] = new TH2F("NHitsVsNLayers", ";NHits;NLayers",250, 0., 2000., 50, 0., 50.);
  histograms2D["NHitsVsShowerStart"] = new TH2F("NHitsVsShowerStart", ";NHits;Start", 250, 0., 2000., 50, 0., 50.);
  histograms2D["NHitsVsDensity"] = new TH2F("NHitsVsDensity", ";NHits;Density", 250, 0., 2000., 1000, 0., 100.); 

  histograms1D["NHitsPerLayer"] = new TH1F("NHitsPerLayer", ";NHits;Entries", 200, 0., 200.); //

  histograms1D["NLayers"] = new TH1F("NLayers", ";NLayers;Entries", 50, 0., 50.);
  histograms1D["NLayersElectrons"] = new TH1F("NLayersElectrons", ";NLayers;Entries", 50, 0., 50.);
  histograms1D["NLayersPions"] = new TH1F("NLayersPions", ";NLayers;Entries", 50, 0., 50.);
  histograms1D["NLayersDiscarded"] = new TH1F("NLayersDiscarded", ";NLayers;Entries", 50, 0., 50.);

  histograms2D["NLayersVsRMS"] = new TH2F("NLayersVsRMS", ";NLayers;RMS",50, 0., 50., 1000, 0., 500.);

  
  histograms1D["ShowerStart"] = new TH1F("ShowerStart", ";StartLayer;Entries", 51, -1., 50.); //
  histograms1D["ShowerStartElectrons"] = new TH1F("ShowerStartElectrons", ";StartLayer;Entries", 51, -1., 50.);
  histograms1D["ShowerStartPions"] = new TH1F("ShowerStartPions", ";StartLayer;Entries", 51, -1., 50.);
  histograms1D["ShowerStartDiscarded"] = new TH1F("ShowerStartDiscarded", ";StartLayer;Entries", 51, -1., 50.);


  histograms2D["ShowerStartVsNLayers"] = new TH2F("ShowerStartVsNLayers", ";Start;NLayers",51, -1., 50., 50, 0., 50.);
  
  histograms2D["ShowerStartVsRMS"] = new TH2F("ShowerStartVsRMS", ";Start;RMS",51, -1., 50., 1000, 0., 500.);
  
  histograms1D["RMS"] = new TH1F("RMS", ";RMS;Entries", 1000, 0., 500.); //
  histograms1D["RMSElectrons"] = new TH1F("RMSElectrons", ";RMS;Entries", 1000, 0., 500.); //
  histograms1D["RMSPions"] = new TH1F("RMSPions", ";RMS;Entries", 1000, 0., 500.); //
  histograms1D["RMSDiscarded"] = new TH1F("RMSDiscarded", ";RMS;Entries", 1000, 0., 500.); //

  histograms1D["RMSILayer"] = new TH1F("RMSILayer", ";RMSILayer;Entries", 500, 0., 200.); //
  histograms1D["RMSJLayer"] = new TH1F("RMSJLayer", ";RMSJLayer;Entries", 500, 0., 200.); //

  histograms2D["RMSIVsRMSJ"] = new TH2F("RMSIVsRMSJ", ";RMSI;RMSJ;", 500, 0., 200., 500, 0., 200.);
  
  histograms1D["Density"] = new TH1F("Density", ";Density;Entries", 1000, 0., 100.); 
  histograms1D["DensityElectrons"] = new TH1F("DensityElectrons", ";Density;Entries", 1000, 0., 100.); 
  histograms1D["DensityPions"] = new TH1F("DensityPions", ";Density;Entries", 1000, 0., 100.); 
  histograms1D["DensityDiscarded"] = new TH1F("DensityDiscarded", ";Density;Entries", 1000, 0., 100.); 
  
  histograms2D["DensityVsRMS"] = new TH2F("DensityVsRMS", ";Density;RMS", 1000, 0., 100., 1000, 0. ,500.); ;
  histograms2D["DensityVsStart"] = new TH2F("DensityVsStart", ";Density;Start", 1000, 0., 100., 50, 0. ,50.); ;
  histograms2D["DensityVsNLayers"] = new TH2F("DensityVsNLayers", ";Density;NLayers", 1000, 0., 100., 50, 0. ,50.); ; 

  // ---- Systematics tools ----
  
  streamlog_out(DEBUG6) << "Creating systematics tools" << std::endl;

  sysToolElectrons = new SystematicsTool(("Systematics_ElectronSelector_Electrons_" + std::to_string(runNumber) + ".txt").c_str());
  sysToolElectrons->AddCut("ElectronsSelection");
  
  sysToolPions = new SystematicsTool(("Systematics_ElectronSelector_Pions_" + std::to_string(runNumber) + ".txt").c_str());
  sysToolPions->AddCut("ElectronsSelection");
  sysToolPions->AddCut("FinalCleanupSelection");
  
  // ---- Reset counters ----

  evtnum = nElectrons = nPions = nDiscarded = 0;
  
}

void ElectronSelectorProc::processEvent( LCEvent * evtP ) 
{

  streamlog_out(DEBUG6) << "Start ElectronSelector process" << std::endl;

  if(evtP){ //Check the evtP
    for(unsigned int i=0; i < _hcalCollections.size(); i++){//loop over collections
      try{

	evtnum++;
	
	LCCollection* col = evtP->getCollection(_hcalCollections[i].c_str());
	int nHits = col->getNumberOfElements();
	
	CellIDDecoder<CalorimeterHit> cd("I:7,J:7,K:10,Dif_id:8,Asic_id:6,Chan_id:7");

	std::map<int,std::vector<int>> IDistribution = {};
	std::map<int,std::vector<int>> JDistribution = {};
	std::map<int,int> KDistribution = {};

	IMPL::LCCollectionVec* outcolnew = new IMPL::LCCollectionVec(LCIO::CALORIMETERHIT);
	outcolnew->setDefault();
	outcolnew->setFlag(outcolnew->getFlag() | (1 << LCIO::RCHBIT_LONG));
	
	IMPL::LCCollectionVec* eventcolnew = new IMPL::LCCollectionVec(LCIO::CALORIMETERHIT);
	eventcolnew->setFlag(col->getFlag());
	eventcolnew->setSubset(true);
	
	std::vector<int> timeParameters = {};
	col->parameters().getIntVals("TimeParameters",timeParameters);

	std::vector<float> trackParameters;
	col->parameters().getFloatVals("TrackParameters",trackParameters);

	bool PC = (bool)col->parameters().getIntVal("PC");
	int secondMax = col->parameters().getIntVal("SecondMax");
	
	outcolnew->parameters().setValues("TimeParameters", timeParameters);
	outcolnew->parameters().setValue("TimeInSpill", col->parameters().getFloatVal("TimeInSpill"));
	outcolnew->parameters().setValues("TrackParameters", trackParameters);
        
	eventcolnew->parameters().setValues("TimeParameters", timeParameters);
	eventcolnew->parameters().setValue("TimeInSpill", col->parameters().getFloatVal("TimeInSpill"));
	eventcolnew->parameters().setValues("TrackParameters", trackParameters);	


	float meanI = 0.;
	float meanJ = 0.;
	
	for(int iHit = 0; iHit < nHits; iHit++) {
	  
	  CalorimeterHit* hit = dynamic_cast<CalorimeterHit*>(col->getElementAt(iHit));

	  int I = (int)cd(hit)["I"];
	  int J = (int)cd(hit)["J"];
	  int K = (int)cd(hit)["K"];

	  meanI += I;
	  meanJ += J;
	  
	  KDistribution[K]++;
	  IDistribution[K].push_back(I);
	  JDistribution[K].push_back(J);
	  
	  CalorimeterHitImpl* copyHit = new CalorimeterHitImpl(*(static_cast<CalorimeterHitImpl*>(hit)));
	  outcolnew->addElement(copyHit);
	  eventcolnew->addElement(hit);
	  
	}

	meanI /= nHits;
	meanJ /= nHits;

	int possibleShowerStart, showerStart, prevShowerLayer;
	possibleShowerStart = showerStart = -1;
	int counter = 0;

	float rmsI = 0.;
	float rmsJ = 0.;
	
	for(auto kIt = KDistribution.begin(); kIt != KDistribution.end(); kIt++) {
	  	  
	  streamlog_out(DEBUG6) << "Processing layer: " << kIt->first << std::endl;

	  int currLayer = kIt->first;
	  
	  float layerMeanI = 0.;
	  float layerMeanJ = 0.;

	  for(int iHit = 0; iHit < kIt->second; iHit++) {
	    layerMeanI += IDistribution.at(currLayer).at(iHit);
	    layerMeanJ += JDistribution.at(currLayer).at(iHit);
	  }

	  layerMeanI /= kIt->second;
	  layerMeanJ /= kIt->second;

	  float layerRMSI = 0.;
	  float layerRMSJ = 0.;
	  
	  for(int iHit = 0; iHit < kIt->second; iHit++) {
	    
	    streamlog_out(DEBUG6) << "Processing hit: " << iHit << std::endl;
	    
	    rmsI += (meanI - IDistribution.at(currLayer).at(iHit))*(meanI - IDistribution.at(currLayer).at(iHit));
	    rmsJ += (meanJ - JDistribution.at(currLayer).at(iHit))*(meanJ - JDistribution.at(currLayer).at(iHit));

	    layerRMSI += (layerMeanI - IDistribution.at(currLayer).at(iHit))*(layerMeanI - IDistribution.at(currLayer).at(iHit));
	    layerRMSJ += (layerMeanJ - JDistribution.at(currLayer).at(iHit))*(layerMeanJ - JDistribution.at(currLayer).at(iHit));
	    
	  }

	  layerRMSI = sqrt(layerRMSI/kIt->second);
	  layerRMSJ = sqrt(layerRMSJ/kIt->second);
	  
	  if(kIt->second >= 5 && layerRMSI > 0.5 && layerRMSJ > 0.5 && showerStart == -1) {
    
	    if(possibleShowerStart == -1) {
	      possibleShowerStart = currLayer;
	      counter = 1;
	    }
	    else if(currLayer - prevShowerLayer < 2) {
	      counter++;
	      if(counter == 3) {
		showerStart = possibleShowerStart;
	      }
	    }
	    else {
	      possibleShowerStart = currLayer;
	      counter = 1;
	    }
	   
	    prevShowerLayer = currLayer;
	    
	  }
	  
	  histograms1D.at("NHitsPerLayer")->Fill(kIt->second);
	  
	  histograms1D.at("RMSILayer")->Fill(layerRMSI);
	  histograms1D.at("RMSJLayer")->Fill(layerRMSJ); 
	  histograms2D.at("RMSIVsRMSJ")->Fill(layerRMSI, layerRMSJ);

	}

	rmsI = sqrt(rmsI/nHits);
	rmsJ = sqrt(rmsJ/nHits);

	float density = nHits/(float)KDistribution.size();
	
	histograms1D.at("NLayers")->Fill(KDistribution.size());	
	histograms1D.at("NHits")->Fill(nHits);	
	histograms1D.at("ShowerStart")->Fill(showerStart);
	histograms1D.at("RMS")->Fill(rmsI*rmsJ);
	histograms1D.at("Density")->Fill(rmsI*rmsJ);

	histograms2D.at("ShowerStartVsNLayers")->Fill(showerStart,KDistribution.size());
	histograms2D.at("ShowerStartVsRMS")->Fill(showerStart,rmsI*rmsJ);
	histograms2D.at("NHitsVsShowerStart")->Fill(nHits,showerStart);
	histograms2D.at("NHitsVsNLayers")->Fill(nHits,KDistribution.size());
	histograms2D.at("NHitsVsRMS")->Fill(nHits,rmsI*rmsJ);
	histograms2D.at("NHitsVsDensity")->Fill(nHits,density);
	histograms2D.at("NLayersVsRMS")->Fill(KDistribution.size(),rmsI*rmsJ);
	histograms2D.at("DensityVsRMS")->Fill(density,rmsI*rmsJ);
	histograms2D.at("DensityVsNLayers")->Fill(density,KDistribution.size());
	histograms2D.at("DensityVsStart")->Fill(density,showerStart);
        
	LCEventImpl* evt = new LCEventImpl();
	evt->setRunNumber(evtP->getRunNumber());
	evt->setTimeStamp(evtP->getTimeStamp());

	int nHitsElectrons, nHitsElectronsUp, nHitsElectronsDown, nHitsPions, nHitsPionsUp, nHitsPionsDown;
	nHitsElectrons = nHitsPions = nHits;
	nHitsElectronsUp = nHitsElectronsDown = nHitsPionsUp = nHitsPionsDown = -999;

        
	if(showerStart < _showerStartCut*1.1 && KDistribution.size() < _NLayersCut*1.1 && rmsI*rmsJ < _RMSCut*1.1) {
	  nHitsElectronsUp = nHits;
	}
	else nHitsPionsDown = nHits;

	if(showerStart < _showerStartCut*0.9 && KDistribution.size() < _NLayersCut*0.9 && rmsI*rmsJ < _RMSCut*0.9) {
	  nHitsElectronsDown = nHits;
	}
	else nHitsPionsUp = nHits;

	int nHitsPionsNotDiscarded = nHits;
	int nHitsPionsNotDiscardedUp, nHitsPionsNotDiscardedDown;
	nHitsPionsNotDiscardedUp = nHitsPionsNotDiscardedDown = -999;

	if(!(showerStart < _showerStartCut && KDistribution.size() < _NLayersCut && rmsI*rmsJ < _RMSCut) && !(showerStart == -1 || rmsI*rmsJ < _RMSCut*1.1 || (density < _densityCut*1.1 && KDistribution.size() > _NLayersLeakageCut*1.1))) nHitsPionsNotDiscardedDown = nHits;

	if(!(showerStart < _showerStartCut && KDistribution.size() < _NLayersCut && rmsI*rmsJ < _RMSCut) && !(showerStart == -1 || rmsI*rmsJ < _RMSCut*0.9 || (density < _densityCut*0.9 && KDistribution.size() > _NLayersLeakageCut*0.9))) nHitsPionsNotDiscardedUp = nHits;
	
	if(showerStart < _showerStartCut && KDistribution.size() < _NLayersCut && rmsI*rmsJ < _RMSCut) {

	  nHitsPions = -999;
	  nHitsPionsNotDiscarded = -999;
	  
	  histograms1D.at("NHitsElectrons")->Fill(nHits);	
	  histograms1D.at("ShowerStartElectrons")->Fill(showerStart);
	  histograms1D.at("RMSElectrons")->Fill(rmsI*rmsJ);
	  histograms1D.at("NLayersElectrons")->Fill(KDistribution.size());
	  histograms1D.at("DensityElectrons")->Fill(rmsI*rmsJ);

	  evt->setEventNumber(++nElectrons);
	  evt->addCollection(outcolnew,_outColNameElectrons);
	  evtP->addCollection(eventcolnew,_outColNameElectrons);

	  _lcWriterElectrons->writeEvent(evt);

	}
	else if(showerStart == -1) { // || KDistribution.size() < _NLayersLeakageCut) { //|| rmsI*rmsJ < _RMSCut || (density < _densityCut && ) ) {
	  nDiscarded++;
	  
	  nHitsPionsNotDiscarded = -999;
	  nHitsElectrons = -999;
	  
	  histograms1D.at("NHitsDiscarded")->Fill(nHits);	
	  histograms1D.at("ShowerStartDiscarded")->Fill(showerStart);
	  histograms1D.at("RMSDiscarded")->Fill(rmsI*rmsJ);
	  histograms1D.at("NLayersDiscarded")->Fill(KDistribution.size());
	  histograms1D.at("DensityDiscarded")->Fill(rmsI*rmsJ);
	  	  
	  delete outcolnew;
	  delete eventcolnew;
	}
	else {

	  nHitsElectrons = -999;

	  histograms1D.at("NHitsPions")->Fill(nHits);	
	  histograms1D.at("ShowerStartPions")->Fill(showerStart);
	  histograms1D.at("RMSPions")->Fill(rmsI*rmsJ);
	  histograms1D.at("NLayersPions")->Fill(KDistribution.size());
	  histograms1D.at("DensityPions")->Fill(rmsI*rmsJ);
	  
	  evt->setEventNumber(++nPions);
	  evt->addCollection(outcolnew,_outColNamePions);
	  evtP->addCollection(eventcolnew,_outColNamePions);

	  _lcWriterPions->writeEvent(evt);

	}

	sysToolElectrons->FillCut("ElectronsSelection", nHitsElectrons, nHitsElectronsUp, nHitsElectronsDown);

	sysToolPions->FillCut("ElectronsSelection", nHitsPions, nHitsPionsUp, nHitsPionsDown);
	sysToolPions->FillCut("FinalCleanupSelection", nHitsPionsNotDiscarded, nHitsPionsNotDiscardedUp, nHitsPionsNotDiscardedDown);
        
	delete evt;
	
      }catch (lcio::DataNotAvailableException zero) { streamlog_out(DEBUG) << "ERROR READING COLLECTION: " << _hcalCollections[i] << std::endl;} 
    }//end loop over collection
  }
  
}


//==============================================================
void ElectronSelectorProc::end()
{
  
  _lcWriterElectrons->close();  
  delete _lcWriterElectrons;
  _lcWriterPions->close();  
  delete _lcWriterPions;
  
  writeHistograms();

  sysToolElectrons->WriteSystematics();
  delete sysToolElectrons;
  sysToolPions->WriteSystematics();
  delete sysToolPions;
  
  _outputFile->Close();
  delete _outputFile;

  streamlog_out(DEBUG4) << "Ending ElectronSelector" << std::endl;

}


void ElectronSelectorProc::writeHistograms() {

  _outputFile->cd(std::to_string(runNumber).c_str());
  
  TPaveText stats(0.05, 0.1, 0.95, 0.9);
  stats.AddText(std::to_string(runNumber).c_str());
  stats.AddText(("Percentage of Pions:" + std::to_string(nPions*100/(float)evtnum) + "%").c_str());
  stats.AddText(("Percentage of Electrons:" + std::to_string(nElectrons*100/(float)evtnum) + "%").c_str());
  stats.AddText(("Percentage of Discarded:" + std::to_string(nDiscarded*100/(float)evtnum) + "%").c_str());
  stats.Write();

  for(auto It1D = histograms1D.begin(); It1D != histograms1D.end(); It1D++) {
    It1D->second->Write();
    delete It1D->second;
  }

  for(auto It2D = histograms2D.begin(); It2D != histograms2D.end(); It2D++) {
    It2D->second->Write();
    delete It2D->second;
  }
  
}

 
