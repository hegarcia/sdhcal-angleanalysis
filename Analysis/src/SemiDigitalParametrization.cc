#include "SemiDigitalParametrization.h"

#include <marlin/Global.h>

#include <EVENT/LCCollection.h>
#include <EVENT/CalorimeterHit.h>

#include <TCanvas.h>
#include <TLegend.h>
#include <TFitter.h>
#include <TMath.h>
#include <TMinuit.h>

#include <algorithm>

SemiDigitalParametrizationProc a_SemiDigitalParametrizationProc;
SemiDigitalParametrizationProc* a_Pointer;

void minuitFunction(int& nDim, double* gout, double& result, double par[], int flg) {     //const double* xx) {   

     streamlog_out(DEBUG2) << "Calling minuit Function" << std::endl; 
     
     result = a_Pointer->chiSquare(par);   //par[0],par[1],par[2],par[3],par[4],par[5],par[6],par[7],par[8]);
}

//=========================================================
SemiDigitalParametrizationProc::SemiDigitalParametrizationProc()
     :Processor("SemiDigitalParametrizationProc")
{
    
  //Input Collections    
  _hcalCollections.push_back(std::string("SDHCAL_PionEvents"));
  registerInputCollections(LCIO::RAWCALORIMETERHIT, 
			   "HCALCollections",  
			   "HCAL Collection Names",  
			   _hcalCollections, 
			   _hcalCollections); 


  //Output ROOT file name
  _outROOTName="SemiDigitalParametrization";
  registerProcessorParameter("OutputROOTName", 
			     "Output ROOT file name",
			     _outROOTName,
			     _outROOTName);

  //Output ROOT file name
  _outParametersFileName="SemiDigitalParameters";
  registerProcessorParameter("OutParametersFileName", 
			     "Output for the parameters name",
			     _outParametersFileName,
			     _outParametersFileName);


  //Corrections file name
  _intensityCorrectionFileName="IntensityCorrection";
  registerProcessorParameter("IntensityCorrectionFile", 
			     "Name of the intensity correction file",
			     _intensityCorrectionFileName,
			     _intensityCorrectionFileName);

 
  //Initial Alpha parameters
  initAlphas = std::vector<float>(3, 0.);
  registerProcessorParameter("InitAlphas", 
			     "Alpha_0 Alpha_1 Alpha_2",
			     initAlphas,
			     initAlphas);
  
  //Initial Beta parameters
  initBetas = std::vector<float>(3, 0.);
  registerProcessorParameter("InitBetas", 
			     "Betas_0 Betas_1 Betas_2",
			     initBetas,
			     initBetas);

  //Initial Gamma parameters
  initGammas = std::vector<float>(3, 0.);
  registerProcessorParameter("InitGammas", 
			     "Gammas_0 Gammas_1 Gammas_2",
			     initGammas,
			     initGammas);


  //Angle correction parameters
  _angleCorrParameters = { 1.0, 0.0 };
  registerProcessorParameter("AngleCorrParameters", 
			     "Angle correction and error",
			     _angleCorrParameters,
			     _angleCorrParameters);


  //Max value for the NHits in the histograms
  _maxNHits = 2000;
  registerProcessorParameter("MaxNHits", 
			     "Max value for the NHits in the histograms",
			     _maxNHits,
			     _maxNHits);


  //NBins in the histograms
  _nBins = 250;
  registerProcessorParameter("NBins", 
			     "NBins in the histogramas",
			     _nBins,
			     _nBins);

} 


void SemiDigitalParametrizationProc::init() {

  streamlog_out(DEBUG2) << "SemiDigitalParametrization Init" << std::endl;

  a_Pointer = this;
  
  std::vector<int> runBeamEnergies;
  std::vector<int> runNumbers;
  marlin::Global::parameters->getIntVals("BeamEnergies",runBeamEnergies);
  marlin::Global::parameters->getIntVals("RunNumbers",runNumbers);
  if(runNumbers.size() != runBeamEnergies.size()) {
       std::cout << "Run numbers size = " << runNumbers.size() << " is not the same as beam energies = " << runBeamEnergies.size() << std::endl;
  }
  else {
       for(int iRun = 0; iRun < runNumbers.size(); iRun++) {
	    beamEnergiesMap[runNumbers.at(iRun)] = runBeamEnergies.at(iRun);
    }
  }

  if(initAlphas.size() != 3) {
    std::cout << "Wrong alphas size = " << initAlphas.size() << std::endl;
    initAlphas = std::vector<float>(3, 0.);
  }

  if(initBetas.size() != 3) {
    std::cout << "Wrong betas size = " << initBetas.size() << std::endl;
    initBetas = std::vector<float>(3, 0.);
  }

  if(initGammas.size() != 3) {
    std::cout << "Wrong gammas size = " << initGammas.size() << std::endl;
    initGammas = std::vector<float>(3, 0.);
  }
  
  _outputFile = new TFile((_outROOTName + ".root").c_str(),"RECREATE");

  // ---- Parameter functions ----
  
  alpha = new TF1("Alpha","[0] + [1]*x + [2]*x*x", 0., _maxNHits);
  beta = new TF1("Beta","[0] + [1]*x + [2]*x*x", 0., _maxNHits);
  gamma = new TF1("Gamma","[0] + [1]*x + [2]*x*x", 0., _maxNHits);

  bestAlpha = new TF1("BestAlpha","[0] + [1]*x + [2]*x*x", 0., _maxNHits);
  bestAlpha->SetLineColor(4);
  bestAlpha->SetTitle(";N_Hit;");
  bestAlpha->SetMaximum(1.0);
  bestAlpha->SetMinimum(0.);

  bestBeta = new TF1("BestBeta","[0] + [1]*x + [2]*x*x", 0., _maxNHits);
  bestBeta->SetLineColor(3);

  bestGamma = new TF1("BestGamma","[0] + [1]*x + [2]*x*x", 0., _maxNHits);
  bestGamma->SetLineColor(2);

  runNumber = beamEnergy = -1;
  N_1 = N_2 = N_3  = {};
       
  printParameters();

}


void SemiDigitalParametrizationProc::processRunHeader(LCRunHeader* runHd)
{

  runNumber = runHd->getRunNumber();  

  if(beamEnergy != -1 && beamEnergiesMap.at(runNumber) != beamEnergy) {
       writeHistograms();;
  }

  streamlog_out(MESSAGE) << "SemiDigitalParametrization processing run:" << runNumber << std::endl;

  beamEnergy = beamEnergiesMap.at(runNumber);

  // ---- Openning auxiliary files ----

  correctionFile.open((_intensityCorrectionFileName + std::to_string(runNumber) + ".txt").c_str());
  if(!correctionFile.is_open()) std::cout << "Could not open: " << _intensityCorrectionFileName + ".txt" << std::endl;

  std::string tmp;
  std::getline(correctionFile, tmp);
  correctionFile >> tmp >> corrN1 >> eCorrN1;
  correctionFile >> tmp >> corrN2 >> eCorrN2;
  correctionFile >> tmp >> corrN3 >> eCorrN3;

  if(corrN1 > 0 || (corrN1 + eCorrN1)/corrN1 < -0.4) { corrN1 = 0.0; eCorrN1 = 0.0; }
  if(corrN2 > 0 || (corrN2 + eCorrN2)/corrN2 < -0.4) { corrN2 = 0.0; eCorrN2 = 0.0; } 
  if(corrN3 > 0 || (corrN3 + eCorrN3)/corrN3 < -0.4) { corrN3 = 0.0; eCorrN3 = 0.0; }

  streamlog_out(MESSAGE) << "Intensity correction values:" << std::endl;
  streamlog_out(MESSAGE) << "\t 1 " << corrN1 << " " << eCorrN1 << std::endl;
  streamlog_out(MESSAGE) << "\t 2 " << corrN2 << " " << eCorrN2 << std::endl;
  streamlog_out(MESSAGE) << "\t 3 " << corrN3 << " " << eCorrN3 << std::endl;

  correctionFile.close();

  _outputFile->mkdir((std::to_string(beamEnergy) + "GeV").c_str());
  _outputFile->cd((std::to_string(beamEnergy) + "GeV").c_str());

  /*histograms1D[("CorrectedSaturationTotal" + std::to_string(runNumber)).c_str()] = new TH1F("CorrectedSaturation_NTotal", ";T(s);NHits", 100, 0., 10.);

  histograms1D[("CorrectedSaturationN1" + std::to_string(runNumber)).c_str()] = new TH1F("CorrectedSaturation_NTotal", ";T(s);NHits", 100, 0., 10.);
  histograms1D[("CorrectedSaturationN2" + std::to_string(runNumber)).c_str()] = new TH1F("CorrectedSaturation_NTotal", ";T(s);NHits", 100, 0., 10.);
  histograms1D[("CorrectedSaturationN3" + std::to_string(runNumber)).c_str()] = new TH1F("CorrectedSaturation_NTotal", ";T(s);NHits", 100, 0., 10.);*/

}


void SemiDigitalParametrizationProc::processEvent( LCEvent * evtP ) 
{

  streamlog_out(DEBUG1) << "Process SemiDigitalParametrization" << std::endl;

  if(evtP){
      for(unsigned int i=0; i < _hcalCollections.size(); i++) {//loop over collections
	try{

	  LCCollection* col = evtP->getCollection(_hcalCollections.at(i));
	  float timeInSpill = col->getParameters().getFloatVal("TimeInSpill");
	  if(timeInSpill < 1.0) return;

	  std::vector<float> trackParameters;
	  col->parameters().getFloatVals("TrackParameters",trackParameters);

	  float angleRad = TMath::ATan(trackParameters.at(0));

	  int N1, N2, N3;
	  N1 = N2 = N3 = 0;
	  
	  int nHits = 0;
	  int nHits_1 = 0;
	  int nHits_2 = 0;
	  int nHits_3 = 0;
	  
	  for(int iHit = 0; iHit < col->getNumberOfElements(); iHit++) {
	       CalorimeterHit* hit = dynamic_cast<CalorimeterHit*>(col->getElementAt(iHit));
	       int T = (int)hit->getEnergy();
	       
	       switch(T) {
	       case 1: N1++; break;
	       case 2: N2++; break;
	       case 3: N3++; break;
	       }
	  }

	  int correctedN1 = N1*TMath::Cos(_angleCorrParameters.at(0)*angleRad) + corrN1*(1.0 - timeInSpill);
	  int correctedN2 = N2*TMath::Cos(_angleCorrParameters.at(0)*angleRad); + corrN2*(1.0 - timeInSpill);
	  int correctedN3 = N3*TMath::Cos(_angleCorrParameters.at(0)*angleRad); + corrN3*(1.0 - timeInSpill);
	  int correctedN = correctedN1 + correctedN2 + correctedN3;

	  int binNHits = (int)(correctedN*_nBins/(float)_maxNHits);
	  statsPerBin[beamEnergy][binNHits]++;

	  //int bin = (int)(timeInSpill/0.1) + 1;
	  
	  /*statTMap[bin]++;
	    nHitTMap_NTotal[bin] += nHits;
	    nHitTMap_N1[bin] += nHits_1;
	    nHitTMap_N2[bin] += nHits_2;
	    nHitTMap_N3[bin] += nHits_3;*/

	  N_1[beamEnergy].push_back(correctedN1);
	  N_2[beamEnergy].push_back(correctedN2);
	  N_3[beamEnergy].push_back(correctedN3);
	  
	} catch (lcio::DataNotAvailableException zero) { std::cout << "ERROR READING COLLECTION: " << _hcalCollections[i] << std::endl;} 
      }//end loop over collection
  }
}


//==============================================================
void SemiDigitalParametrizationProc::end()
{
  streamlog_out(MESSAGE) << "Ending SemiDigitalParametrization" << std::endl;  

  double p1 = -1; 

  TFitter* minimizer = new TFitter(9);
  minimizer->ExecuteCommand("SET PRINTOUT",&p1,9);
  minimizer->SetFCN(minuitFunction);

  double initParams[9] = { initAlphas.at(0), initAlphas.at(1), initAlphas.at(2), initBetas.at(0), initBetas.at(1), initBetas.at(2), initGammas.at(0), initGammas.at(1), initGammas.at(2) };
  std::vector<double> bestParams, bestParamsErr;
  bestParams.assign(initParams, initParams+9);
  bestParamsErr = std::vector<double>(9,0.0);

  double minimum = chiSquare(initParams);
  streamlog_out(MESSAGE) << "First minimum:" << minimum << std::endl;  

  streamlog_out(DEBUG2) << "Minimizing" << std::endl; 
 
  for(int i = 0; i < 10; i++) {
  
       std::cout << "Loop " << i << std::endl;

       minimizer->SetParameter(0,"Alpha0",bestParams[0],.00001,0.0,0.15);       
       minimizer->SetParameter(1,"Alpha1",bestParams[1],.000000001,0.0,0.000035);
       minimizer->SetParameter(2,"Alpha2",bestParams[2],.00000000001,-0.00000006,0.0);

       minimizer->SetParameter(3,"Beta0",bestParams[3],.001,0.06,0.25);
       minimizer->SetParameter(4,"Beta1",bestParams[4],.00000001,-0.000025,0.000003);
       minimizer->SetParameter(5,"Beta2",bestParams[5],.00000000001,-0.000000038,-0.0000000008);
       
       minimizer->SetParameter(6,"Gamma0",bestParams[6],.001,0.14,0.4);
       minimizer->SetParameter(7,"Gamma1",bestParams[7],.0000001,0.0,0.00025);
       minimizer->SetParameter(8,"Gamma2",bestParams[8],.00000000001,0.0,0.0000001);
       

       streamlog_out(DEBUG2) << "Executing SIMPLEX" << std::endl;
       double args[2] = { 50000, 0.001 };
       minimizer->ExecuteCommand("SIMPLEX",args,2);
 
       double params[9] = { minimizer->GetParameter(0), minimizer->GetParameter(1), minimizer->GetParameter(2), minimizer->GetParameter(3), minimizer->GetParameter(4), minimizer->GetParameter(5), minimizer->GetParameter(6), minimizer->GetParameter(7), minimizer->GetParameter(8) };
       double newMinim = chiSquare(params);

       if(newMinim < minimum) {
	    minimum = newMinim;
	    bestParams.assign(params,params+9);
	    bestParamsErr = { minimizer->GetParError(0), minimizer->GetParError(1), minimizer->GetParError(2), minimizer->GetParError(3), minimizer->GetParError(4), minimizer->GetParError(5), minimizer->GetParError(6), minimizer->GetParError(7), minimizer->GetParError(8) };
       }
  }

  streamlog_out(MESSAGE) << "Last minimum: " << minimum << std::endl;  

  std::cout << "Alpha: " << bestParams[0] << " " << bestParams[1] << " " << bestParams[2] << " " << std::endl;
  std::cout << "Beta: " << bestParams[3] << " " << bestParams[4] << " " << bestParams[5] << " " << std::endl;
  std::cout << "Gamma: " << bestParams[6] << " " << bestParams[7] << " " << bestParams[8] << " " << std::endl;

  minimFile.open((_outParametersFileName + ".txt").c_str());

  minimFile << bestParams[0] << " " << bestParamsErr[0] << " " << bestParams[1] << " " << bestParamsErr[1] << " " << bestParams[2] << " " << bestParamsErr[2] << "\n"; 
  minimFile << bestParams[3]  << " " << bestParamsErr[3] << " " << bestParams[4]  << " " << bestParamsErr[4] << " " << bestParams[5] << " " << bestParamsErr[5] << "\n";
  minimFile << bestParams[6]  << " " << bestParamsErr[6] << " " << bestParams[7]  << " " << bestParamsErr[7]  << " " << bestParams[8]  << " " << bestParamsErr[8]  << "\n";
  
  minimFile.close();

  bestAlpha->SetParameters(bestParams[0], bestParams[1], bestParams[2]);
  bestBeta->SetParameters(bestParams[3], bestParams[4], bestParams[5]);
  bestGamma->SetParameters(bestParams[6], bestParams[7], bestParams[8]);

  _outputFile->cd();

  TLegend* legend = new TLegend(0.7, 0.7, 0.9, 0.9);
  legend->AddEntry(bestAlpha, "#alpha = #alpha_{0} + #alpha_{1} N_{Hit} + #alpha_{2} N^{2}_{Hit}");
  legend->AddEntry(bestBeta, "#beta = #beta_{0} + #beta_{1} N_{Hit} + #beta_{2} N^{2}_{Hit}");
  legend->AddEntry(bestGamma, "#gamma = #gamma_{0} + #gamma_{1} N_{Hit} + #gamma_{2} N^{2}_{Hit}");

  TCanvas* parsCanvas = new TCanvas("Parameters");
  bestAlpha->Draw();
  bestBeta->Draw("same");
  bestGamma->Draw("same");
  legend->Draw("same");

  parsCanvas->Write();
  delete parsCanvas;

  writeHistograms();

  _outputFile->Close();
  delete _outputFile;
}


void SemiDigitalParametrizationProc::writeHistograms()
{

     streamlog_out(DEBUG2) << "Writting histograms for run: " << runNumber << std::endl;
  
     _outputFile->cd((std::to_string(beamEnergy) + "GeV").c_str());
     
     for(auto It1D = histograms1D.begin(); It1D != histograms1D.end(); It1D++) {
	  It1D->second->Write();
	  delete It1D->second;
     }

}


double SemiDigitalParametrizationProc::chiSquare(double* par) //(double x1, double x2, double x3, double x4, double x5, double x6, double x7, double x8, double x9)  //double* par) 
{

     streamlog_out(DEBUG3) << "Calling chiSquare function with parameters:" << std::endl; 
     streamlog_out(DEBUG3) << "Alpha:" << par[0] << " " << par[1] << " " << par[2] << std::endl; 
     streamlog_out(DEBUG3) << "Beta:" << par[3] << " " << par[4] << " " << par[5] << std::endl; 
     streamlog_out(DEBUG3) << "Gamma:" << par[6] << " " << par[7] << " " << par[8] << std::endl; 

     alpha->SetParameters(par[0], par[1], par[2]);
     beta->SetParameters(par[3], par[4], par[5]);
     gamma->SetParameters(par[6], par[7], par[8]);
     
     
     /*streamlog_out(DEBUG3) << "Calling chiSquare function with parameters:" << std::endl; 
     streamlog_out(DEBUG3) << "Alpha:" << par[0] << " " << par[1]  << std::endl; 
     streamlog_out(DEBUG3) << "Beta:" << par[2] << " " << par[3]  << std::endl; 
     streamlog_out(DEBUG3) << "Gamma:" << par[4] << " " << par[5]  << std::endl; 

     alpha->SetParameters(par[0], 0.0, par[1]);
     beta->SetParameters(par[2], 0.0, par[3]);
     gamma->SetParameters(par[4], 0.0, par[5]);*/


     streamlog_out(DEBUG3) << "Starting loop over the energies" << std::endl; 

     float chisquare = 0.;
     int totalEvents = 0;
     for(auto eIt = N_1.begin(); eIt != N_1.end(); eIt++)
	  {
	       int beamE = eIt->first;
	       int nEvents = std::min((int)N_1.at(beamE).size(), 2000);
	       if(nEvents == 5000) std::cout << "Not enough events at beam energy: " << beamE << std::endl;
	       for(int iEv = 0; iEv < nEvents; iEv++) {
		    
		    int NTotal = N_1.at(beamE).at(iEv) + N_2.at(beamE).at(iEv) + N_3.at(beamE).at(iEv);
		    int bin = (int)(NTotal*_nBins/(float)_maxNHits);

		    float alphaValue = alpha->Eval(NTotal)*N_1.at(beamE).at(iEv);
		    float betaValue = beta->Eval(NTotal)*N_2.at(beamE).at(iEv);
		    float gammaValue = gamma->Eval(NTotal)*N_3.at(beamE).at(iEv);
			
		    float eReco = -beamE;
		    if(alphaValue >= 0 && betaValue >= 0 && gammaValue >= 0) eReco = alphaValue + betaValue + gammaValue;
	       
		    chisquare += TMath::Power((beamE-eReco),2)/(float)(beamE*TMath::Sqrt(statsPerBin.at(beamE).at(bin)));   
		    totalEvents++;
	       }
	  }

     return (chisquare/totalEvents);
}
