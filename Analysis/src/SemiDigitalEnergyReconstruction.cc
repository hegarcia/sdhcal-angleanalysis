#include "SemiDigitalEnergyReconstruction.h"

#include <EVENT/LCCollection.h>
#include <EVENT/CalorimeterHit.h>

#include <marlin/Global.h>

#include <TCanvas.h>
#include <TLegend.h>
#include <TMath.h>


SemiDigitalEnergyReconstructionProc a_SemiDigitalEnergyReconstructionProc;

SemiDigitalEnergyReconstructionProc::SemiDigitalEnergyReconstructionProc()
  :Processor("SemiDigitalEnergyReconstructionProc") {

  //Input Collections    
  _hcalCollections.push_back(std::string("SDHCAL_PionEvents"));
  registerInputCollections(LCIO::RAWCALORIMETERHIT, 
			   "HCALCollections",  
			   "HCAL Collection Names",  
			   _hcalCollections, 
			   _hcalCollections); 
  
  //Output ROOT file name
  _outROOTName="SemiDigitalEnergyReconstruction";
  registerProcessorParameter("OutputROOTName", 
			     "Output ROOT file name",
			     _outROOTName,
			     _outROOTName);

  //Systematics file name
  _showerSystematicsFileName="ShowerSystematics";
  registerProcessorParameter("SystematicsFileName", 
			     "Name of the systematics file",
			     _showerSystematicsFileName,
			     _showerSystematicsFileName);

  //Corrections file name
  _intensityCorrectionFileName="IntensityCorrection";
  registerProcessorParameter("IntensityCorrectionFile", 
			     "Name of the intensity correction file",
			     _intensityCorrectionFileName,
			     _intensityCorrectionFileName);

  //Corrections file name
  _parametrizationFileName="SemiDigitalParameters";
  registerProcessorParameter("ParametrizationFileName", 
			     "Name of the parametrization file",
			     _parametrizationFileName,
			     _parametrizationFileName);


  //Angle correction parameters
  _angleCorrParameters = { 1.0, 0.0 };
  registerProcessorParameter("AngleCorrParameters", 
			     "Angle correction and error",
			     _angleCorrParameters,
			     _angleCorrParameters);

  //NBins in the histograms
  _nBins = 250;
  registerProcessorParameter("NBins", 
			     "NBins in the histogramas",
			     _nBins,
			     _nBins);

  //Max value for the NHits in the histograms
  _maxNHits = 2000;
  registerProcessorParameter("MaxNHits", 
			     "Max value for the NHits in the histograms",
			     _maxNHits,
			     _maxNHits);

  //Max value for the Energy in the histograms
  _maxE = 100;
  registerProcessorParameter("MaxE", 
			     "Max value for the Energy in the histograms",
			     _maxE,
			     _maxE);
  
}


void SemiDigitalEnergyReconstructionProc::init() {

  std::vector<int> runBeamEnergies;
  std::vector<int> runNumbers;
  marlin::Global::parameters->getIntVals("BeamEnergies",runBeamEnergies);
  marlin::Global::parameters->getIntVals("RunNumbers",runNumbers);

  if(runNumbers.size() != runBeamEnergies.size()) {
    std::cout << "Run numbers size = " << runNumbers.size() << " is not the same as beam energies = " << runBeamEnergies.size() << std::endl;
  }
  else {
    for(int iRun = 0; iRun < runNumbers.size(); iRun++) {
      beamEnergiesMap[runNumbers.at(iRun)] = runBeamEnergies.at(iRun);
    }
  }
  
  _outputFile = new TFile((_outROOTName + ".root").c_str(),"RECREATE");

  // ---- Parameter functions ----

  parametrizationFile.open((_parametrizationFileName + ".txt").c_str());

  std::vector<float> alphas, betas, gammas, alphaErrs, betaErrs, gammaErrs;
  alphas = betas = gammas = alphaErrs = betaErrs = gammaErrs = std::vector<float>(3,0.0);

  if(!parametrizationFile.is_open()) std::cout << "Could not open: " << _parametrizationFileName + ".txt" << std::endl;
  else {
       parametrizationFile >> alphas.at(0) >> alphaErrs.at(0)  >> alphas.at(1) >> alphaErrs.at(1)  >> alphas.at(2) >> alphaErrs.at(2);

       parametrizationFile >> betas.at(0) >> betaErrs.at(0)  >> betas.at(1) >> betaErrs.at(1)  >> betas.at(2) >> betaErrs.at(2);

       parametrizationFile >> gammas.at(0) >> gammaErrs.at(0)  >> gammas.at(1) >> gammaErrs.at(1)  >> gammas.at(2) >> gammaErrs.at(2);
  }
  
  alpha = new TF1("Alpha","[0] + [1]*x + [2]*x*x", 0., _maxNHits);
  alpha->SetParameters(alphas.at(0), alphas.at(1), alphas.at(2));
  alpha->SetLineColor(4);
  alpha->SetTitle(";N_Hit;");
  alpha->SetMaximum(1.0);
  alpha->SetMinimum(0.);

  beta = new TF1("Beta","[0] + [1]*x + [2]*x*x", 0., _maxNHits);
  beta->SetParameters(betas.at(0), betas.at(1), betas.at(2));
  beta->SetLineColor(3);

  gamma = new TF1("Gamma","[0] + [1]*x + [2]*x*x", 0., _maxNHits);
  gamma->SetParameters(gammas.at(0), gammas.at(1), gammas.at(2));
  gamma->SetLineColor(2);

  runNumber = beamEnergy = -1;
  
  printParameters();
  
}

void SemiDigitalEnergyReconstructionProc::processRunHeader(LCRunHeader* runHd)
{

  if(runNumber != -1) {
  
       streamlog_out(MESSAGE) << "New run. Computing mean values"  << std::endl;
       writeHistograms(false);
       
  }
    
  if(beamEnergy != -1 && beamEnergiesMap.at(runHd->getRunNumber()) != beamEnergy) {
  
       streamlog_out(MESSAGE) << "New energy. Writing and systematics"  << std::endl;
  
       for(int iBin = 0; iBin < _nBins; iBin++) {
	    if(binRunEntriesMap.find(iBin) == binRunEntriesMap.end()) continue;
	    
	    int nTotalEntries = 0;
	    for(auto runIt = binRunEntriesMap.at(iBin).begin(); runIt != binRunEntriesMap.at(iBin).end(); runIt++) {
		 nTotalEntries += runIt->second;
	    }

	    meanNTPerBin.at(iBin)[0] /= (float)nTotalEntries;
	    meanNTPerBin.at(iBin)[1] /= (float)nTotalEntries;
	    meanNTPerBin.at(iBin)[2] /= (float)nTotalEntries;
	    meanTimeInSpillPerBin.at(iBin) /= (float)nTotalEntries;
	    meanAnglePerBin.at(iBin) /= (float)nTotalEntries;
     
       }

       for(int iBin = 0; iBin < _nBins; iBin++){
           if(binEntriesMapAngleCorr.find(iBin) == binEntriesMapAngleCorr.end()) continue;
	   
	   meanNPerBinAngleCorr.at(iBin)[0] /= (float)binEntriesMapAngleCorr.at(iBin);
	   meanNPerBinAngleCorr.at(iBin)[1] /= (float)binEntriesMapAngleCorr.at(iBin);
	   meanNPerBinAngleCorr.at(iBin)[2] /= (float)binEntriesMapAngleCorr.at(iBin);
	   meanTimeInSpillPerBinAngleCorr.at(iBin) /= (float)binEntriesMapAngleCorr.at(iBin);

       }

       computeEnergySystematics();
       writeHistograms(true);
       
       nHitsSystematicsMap.clear();
       binRunEntriesMap.clear();
       meanNTPerBin.clear();
       meanTimeInSpillPerBin.clear();
       meanAnglePerBin.clear();

       binEntriesMapAngleCorr.clear();
       meanNPerBinAngleCorr.clear();
       meanNPerBinAngleCorr.clear();
       meanNPerBinAngleCorr.clear();
       meanTimeInSpillPerBinAngleCorr.clear();

       //ESystematicsMap.clear();

  }

  runNumber = runHd->getRunNumber();

  streamlog_out(MESSAGE) << "SemiDigitalEnergyReconstruction processing run: " << runNumber << std::endl;
  
  // ---- Openning auxiliary files ----

  streamlog_out(DEBUG) << "Reading systematics" << std::endl;

  systematicsFile.open((_showerSystematicsFileName + std::to_string(runNumber) + ".txt").c_str());
  if(!systematicsFile.is_open()) std::cout << "Could not open: " << _showerSystematicsFileName + std::to_string(runNumber) + ".txt" << std::endl;

  std::string tmp;
  std::getline(systematicsFile, tmp);
  int iBin;
  float sysUp, sysDown;

  while(systematicsFile) {
    systematicsFile >> iBin >> sysUp >> sysDown;
    if(sysUp < 0) std::cout << "Wrong systematic error Up: " << sysUp << " for run: " << runNumber << " in bin: " << iBin << std::endl;
    if(sysDown < 0) std::cout << "Wrong systematic error Down: " << sysDown << " for run: " << runNumber << " in bin: " << iBin << std::endl; 
    nHitsSystematicsMap[iBin][runNumber] = std::pair<float,float>(sysUp, sysDown);
  }
  systematicsFile.close();

  streamlog_out(DEBUG) << "Reading intensity corrections" << std::endl;
 
  correctionFile.open((_intensityCorrectionFileName + std::to_string(runNumber) + ".txt").c_str());
  if(!correctionFile.is_open()) std::cout << "Could not open: " << _intensityCorrectionFileName + std::to_string(runNumber) + ".txt" << std::endl;

  std::getline(correctionFile, tmp);
  correctionFile >> tmp >> corrN1 >> eCorrN1;
  correctionFile >> tmp >> corrN2 >> eCorrN2;
  correctionFile >> tmp >> corrN3 >> eCorrN3;

  if(corrN1 > 0 || (corrN1 + eCorrN1)/corrN1 < -0.4) { corrN1 = 0.0; eCorrN1 = 0.0; }
  if(corrN2 > 0 || (corrN2 + eCorrN2)/corrN2 < -0.4) { corrN2 = 0.0; eCorrN2 = 0.0; } 
  if(corrN3 > 0 || (corrN3 + eCorrN3)/corrN3 < -0.4) { corrN3 = 0.0; eCorrN3 = 0.0; }

  streamlog_out(MESSAGE) << "Intensity correction values:" << std::endl;
  streamlog_out(MESSAGE) << "\t 1 " << corrN1 << " " << eCorrN1 << std::endl;
  streamlog_out(MESSAGE) << "\t 2 " << corrN2 << " " << eCorrN2 << std::endl;
  streamlog_out(MESSAGE) << "\t 3 " << corrN3 << " " << eCorrN3 << std::endl;

  correctionFile.close();

  if(beamEnergy == -1 || beamEnergiesMap.at(runHd->getRunNumber()) != beamEnergy) {

       beamEnergy = beamEnergiesMap.at(runNumber);

       _outputFile->mkdir((std::to_string(beamEnergy) + "GeV").c_str());
       _outputFile->cd((std::to_string(beamEnergy) + "GeV").c_str());
       
       // ---- Creating per energy histograms ---- 
       
       histograms1D["NHits"] = new TH1F("NHits", ";NHits;Entries", _nBins, 0., _maxNHits);
       histograms1D["N1"] = new TH1F("N1", ";NHits;Entries", _nBins, 0., _maxNHits);
       histograms1D["N2"] = new TH1F("N2", ";NHits;Entries", _nBins, 0., _maxNHits);
       histograms1D["N3"] = new TH1F("N3", ";NHits;Entries", _nBins, 0., _maxNHits);

       histograms1D["NHitsAngleCorr"] = new TH1F("NHitsAngleCorr", ";NHits;Entries", _nBins, 0., _maxNHits);
       histograms1D["NHitsAngleCorrUp"] = new TH1F("NHitsAngleCorrUp", ";NHits;Entries", _nBins, 0., _maxNHits);
       histograms1D["NHitsAngleCorrDown"] = new TH1F("NHitsAngleCorrDown", ";NHits;Entries", _nBins, 0., _maxNHits);
       histograms1D["N1AngleCorr"] = new TH1F("N1AngleCorr", ";NHits;Entries", _nBins, 0., _maxNHits);
       histograms1D["N2AngleCorr"] = new TH1F("N2AngleCorr", ";NHits;Entries", _nBins, 0., _maxNHits);
       histograms1D["N3AngleCorr"] = new TH1F("N3AngleCorr", ";NHits;Entries", _nBins, 0., _maxNHits);
       
       histograms1D["NHitsCorr"] = new TH1F("NHitsCorr", ";NHits;Entries", _nBins, 0., _maxNHits);
       histograms1D["NHitsCorrUp"] = new TH1F("NHitsCorrUp", ";NHits;Entries", _nBins, 0., _maxNHits);
       histograms1D["NHitsCorrDown"] = new TH1F("NHitsCorrDown", ";NHits;Entries", _nBins, 0., _maxNHits);
      
       histograms1D["N1Corr"] = new TH1F("N1Corr", ";NHits;Entries", _nBins, 0., _maxNHits);
       histograms1D["N2Corr"] = new TH1F("N2Corr", ";NHits;Entries", _nBins, 0., _maxNHits);
       histograms1D["N3Corr"] = new TH1F("N3Corr", ";NHits;Entries", _nBins, 0., _maxNHits);
       
       histograms1D["Energy"] = new TH1F("Energy", ";Energy(GeV);Entries", _nBins, 0., _maxE);
  }
     
  // ---- Creating per run histograms ---- 
  
  _outputFile->cd();
  _outputFile->mkdir((std::to_string(beamEnergy) + "GeV/" + std::to_string(runNumber)).c_str());
  _outputFile->cd((std::to_string(beamEnergy) + "GeV/" + std::to_string(runNumber)).c_str());
  
  histograms1D["RunSlopeX"] = new TH1F("RunSlopeX", ";SlopeX;Entries", 1000, -10., 10.);
  histograms1D["RunSlopeY"] = new TH1F("RunSlopeY", ";SlopeY;Entries", 1000, -10., 10.);
  
  histograms1D["RunAngleRad"] = new TH1F("RunAngleRad", ";Angle(Rad);Entries", 130, -1.6, 1.6);
  histograms1D["RunAngleCorr"] = new TH1F("RunAngleCorr", ";AngleCorr;Entries", 50, 0., 1.);
  
  histograms1D["RunNHits"] = new TH1F("RunNHits", ";NHits;Entries", _nBins, 0., _maxNHits);
  histograms1D["RunNHitsAngleCorr"] = new TH1F("RunNHitsAngleCorr", ";NHits;Entries", _nBins, 0., _maxNHits);
  histograms1D["RunNHitsCorr"] = new TH1F("RunNHitsCorr", ";NHits;Entries", _nBins, 0., _maxNHits);

}


void SemiDigitalEnergyReconstructionProc::processEvent( LCEvent * evtP ) 
{

  streamlog_out(DEBUG1) << "Process SemiDigitalEnergyReconstruction" << std::endl;

  if(evtP){
    for(unsigned int i=0; i < _hcalCollections.size(); i++) {//loop over collections
      try{

	  LCCollection* col = evtP->getCollection(_hcalCollections.at(i));
	  float timeInSpill = col->getParameters().getFloatVal("TimeInSpill");
	  if(timeInSpill < 1.0) return;

	  std::vector<float> trackParameters;
	  col->parameters().getFloatVals("TrackParameters",trackParameters);

	  float angleRad = TMath::ATan(trackParameters.at(0));

	  histograms1D.at("RunAngleRad")->Fill(angleRad);	  
	  histograms1D.at("RunAngleCorr")->Fill(TMath::Cos(_angleCorrParameters.at(0)*angleRad));

	  histograms1D.at("RunSlopeX")->Fill(trackParameters.at(0));
	  histograms1D.at("RunSlopeY")->Fill(trackParameters.at(2));

	  int nHits = col->getNumberOfElements();
	  histograms1D.at("NHits")->Fill(nHits);
	  histograms1D.at("RunNHits")->Fill(nHits);

	  int bin = (int)(_nBins*nHits/(float)_maxNHits);
	  binRunEntriesMap[bin][runNumber]++;
	  
	  int N1, N2, N3;
	  N1 = N2 = N3 = 0;
	  for(int iHit = 0; iHit < col->getNumberOfElements(); iHit++) {
	       CalorimeterHit* hit = dynamic_cast<CalorimeterHit*>(col->getElementAt(iHit));
	       int T = (int)hit->getEnergy();
	       
	       switch(T) {
	       case 1: N1++; break;
	       case 2: N2++; break;
	       case 3: N3++; break;
	       }
	  }

	  histograms1D.at("N1")->Fill(N1);
	  histograms1D.at("N2")->Fill(N2);
	  histograms1D.at("N3")->Fill(N3);

	  meanNTPerBin[bin][0] += N1;
	  meanNTPerBin[bin][1] += N2;
	  meanNTPerBin[bin][2] += N3;	  
	  meanTimeInSpillPerBin[bin] += timeInSpill;
	  meanAnglePerBin[bin] += angleRad;
	  
	  int angleCorrectedN1 = N1*TMath::Cos(_angleCorrParameters.at(0)*angleRad);
	  int correctedN1 = angleCorrectedN1 + corrN1*(1.0 - timeInSpill);
	  int correctedN1Up = angleCorrectedN1 + (corrN1 - eCorrN1)*(1.0 - timeInSpill);
	  int correctedN1Down = angleCorrectedN1 + TMath::Min(0.f,corrN1 + eCorrN1)*(1.0 - timeInSpill);
	  	  
	  int angleCorrectedN2 = N2*TMath::Cos(_angleCorrParameters.at(0)*angleRad);
	  int correctedN2 = angleCorrectedN2 + corrN2*(1.0 - timeInSpill);
	  int correctedN2Up = angleCorrectedN2 + (corrN2 - eCorrN2)*(1.0 - timeInSpill);
	  int correctedN2Down = angleCorrectedN2 + TMath::Min(0.f,corrN2 + eCorrN2)*(1.0 - timeInSpill);
	  
	  int angleCorrectedN3 = N3*TMath::Cos(_angleCorrParameters.at(0)*angleRad);	  
	  int correctedN3 = angleCorrectedN3 + corrN3*(1.0 - timeInSpill);	  
	  int correctedN3Up = angleCorrectedN3 + (corrN3 - eCorrN3)*(1.0 - timeInSpill);
	  int correctedN3Down = angleCorrectedN3 + TMath::Min(0.f,corrN3 + eCorrN3)*(1.0 - timeInSpill);

	  int angleCorrectedN = angleCorrectedN1 + angleCorrectedN2 + angleCorrectedN3;
	  int binAngleCorr =  (int)(_nBins*angleCorrectedN/(float)_maxNHits);

	  binEntriesMapAngleCorr[binAngleCorr]++;
	  meanNPerBinAngleCorr[binAngleCorr][0] += angleCorrectedN1;
	  meanNPerBinAngleCorr[binAngleCorr][1] += angleCorrectedN2;
	  meanNPerBinAngleCorr[binAngleCorr][2] += angleCorrectedN3;	  
	  meanTimeInSpillPerBinAngleCorr[binAngleCorr] += timeInSpill;

	  int angleCorrectedNUp = nHits*TMath::Cos((_angleCorrParameters.at(0) - _angleCorrParameters.at(1))*angleRad);
	  int angleCorrectedNDown = nHits*TMath::Cos((_angleCorrParameters.at(0) + _angleCorrParameters.at(1))*angleRad);
						   
	  int correctedN = correctedN1 + correctedN2 + correctedN3;
	  int correctedNUp = correctedN1Up + correctedN2Up + correctedN3Up;
	  int correctedNDown = correctedN1Down + correctedN2Down + correctedN3Down;

						   
	  histograms1D.at("NHitsAngleCorr")->Fill(angleCorrectedN);
	  histograms1D.at("NHitsAngleCorrUp")->Fill(angleCorrectedNUp);
	  histograms1D.at("NHitsAngleCorrDown")->Fill(angleCorrectedNDown);
	 
	  histograms1D.at("RunNHitsAngleCorr")->Fill(angleCorrectedN);
	  
	  histograms1D.at("N1AngleCorr")->Fill(angleCorrectedN1);
	  histograms1D.at("N2AngleCorr")->Fill(angleCorrectedN2);
	  histograms1D.at("N3AngleCorr")->Fill(angleCorrectedN3);

	  histograms1D.at("NHitsCorr")->Fill(correctedN);
	  histograms1D.at("NHitsCorrUp")->Fill(correctedNUp);
	  histograms1D.at("NHitsCorrDown")->Fill(correctedNDown);
	  
	  histograms1D.at("RunNHitsCorr")->Fill(correctedN);

	  histograms1D.at("N1Corr")->Fill(correctedN1);
	  histograms1D.at("N2Corr")->Fill(correctedN2);
	  histograms1D.at("N3Corr")->Fill(correctedN3);

	  streamlog_out(DEBUG1) << "Compute energy" << std::endl;

	  float E = alpha->Eval(correctedN)*correctedN1 + beta->Eval(correctedN)*correctedN2 + gamma->Eval(correctedN)*correctedN3;

	  histograms1D.at("Energy")->Fill(E);

      }catch (lcio::DataNotAvailableException zero) { std::cout << "ERROR READING COLLECTION: " << _hcalCollections[i] << std::endl;}
    }//end loop over collection   
  } 
}


//==============================================================
void SemiDigitalEnergyReconstructionProc::end()
{

  for(int iBin = 0; iBin < _nBins; iBin++) {
      if(binRunEntriesMap.find(iBin) == binRunEntriesMap.end()) continue;
      
      int nTotalEntries = 0;
      for(auto runIt = binRunEntriesMap.at(iBin).begin(); runIt != binRunEntriesMap.at(iBin).end(); runIt++) {
	   nTotalEntries += runIt->second;
      }
      
      meanNTPerBin.at(iBin)[0] /= (float)nTotalEntries;
      meanNTPerBin.at(iBin)[1] /= (float)nTotalEntries;
      meanNTPerBin.at(iBin)[2] /= (float)nTotalEntries;
      meanTimeInSpillPerBin.at(iBin) /= (float)nTotalEntries;
      meanAnglePerBin.at(iBin) /= (float)nTotalEntries;
  }

  for(int iBin = 0; iBin < _nBins; iBin++){
       if(binEntriesMapAngleCorr.find(iBin) == binEntriesMapAngleCorr.end()) continue;
	    
       meanNPerBinAngleCorr.at(iBin)[0] /= (float)binEntriesMapAngleCorr.at(iBin);
       meanNPerBinAngleCorr.at(iBin)[1] /= (float)binEntriesMapAngleCorr.at(iBin);
       meanNPerBinAngleCorr.at(iBin)[2] /= (float)binEntriesMapAngleCorr.at(iBin);
       meanTimeInSpillPerBinAngleCorr.at(iBin) /= (float)binEntriesMapAngleCorr.at(iBin);     
  }
  
  computeEnergySystematics();
  writeHistograms(true);

  _outputFile->cd();

  TLegend* legend = new TLegend(0.8, 0.8, 0.9, 0.9);
  legend->AddEntry(alpha, "#alpha");
  legend->AddEntry(beta, "#beta");
  legend->AddEntry(gamma, "#gamma");
  
  TCanvas* parsCanvas = new TCanvas("Parameters");
  alpha->Draw();
  beta->Draw("same");
  gamma->Draw("same");
  legend->Draw("same");

  parsCanvas->Write();
  delete parsCanvas;
  
  _outputFile->Close();
  delete _outputFile;

  systematicsFile.close();
  correctionFile.close();
  
}


void SemiDigitalEnergyReconstructionProc::computeEnergySystematics() {

  streamlog_out(DEBUG7) << "Compute systematics run: " << runNumber << std::endl;

  streamlog_out(DEBUG7) << "Compute full cut systematics." << std::endl;

  std::map<int, std::pair<float,float>> finalCutsSystematics = {}; // Bin, (sysUp, sysDown)
  std::map<int, std::pair<int,std::pair<float,float>>> finalCutsSystematicsAngleCorr = {};
  std::map<int, std::pair<int,std::pair<float,float>>> finalCutsSystematicsIntCorr = {};

  for(int iBin = 0; iBin < _nBins; iBin++) {

       if(binRunEntriesMap.find(iBin) == binRunEntriesMap.end() || nHitsSystematicsMap.find(iBin) == nHitsSystematicsMap.end()) continue;
 
       int nTotalEntries = 0;
       for(auto runIt = binRunEntriesMap.at(iBin).begin(); runIt != binRunEntriesMap.at(iBin).end(); runIt++) {
	    if(nHitsSystematicsMap.at(iBin).find(runIt->first) == nHitsSystematicsMap.at(iBin).end()) continue;
	    nTotalEntries += runIt->second;
	    finalCutsSystematics[iBin].first += nHitsSystematicsMap.at(iBin).at(runIt->first).first*runIt->second;
	    finalCutsSystematics[iBin].second += nHitsSystematicsMap.at(iBin).at(runIt->first).second*runIt->second;
       }

       finalCutsSystematics[iBin].first /= (float)nTotalEntries;
       finalCutsSystematics[iBin].second /= (float)nTotalEntries;

       int meanN1AngleCorr = meanNTPerBin.at(iBin)[0]*TMath::Cos(_angleCorrParameters.at(0)*meanAnglePerBin.at(iBin));
       int meanN2AngleCorr = meanNTPerBin.at(iBin)[1]*TMath::Cos(_angleCorrParameters.at(0)*meanAnglePerBin.at(iBin));
       int meanN3AngleCorr = meanNTPerBin.at(iBin)[2]*TMath::Cos(_angleCorrParameters.at(0)*meanAnglePerBin.at(iBin));

       int meanN1Corr = meanN1AngleCorr + corrN1*(1.0 - meanTimeInSpillPerBin.at(iBin));
       int meanN2Corr = meanN2AngleCorr + corrN2*(1.0 - meanTimeInSpillPerBin.at(iBin));
       int meanN3Corr = meanN3AngleCorr + corrN3*(1.0 - meanTimeInSpillPerBin.at(iBin));

       int meanNHitsAngleCorr = meanN1AngleCorr + meanN2AngleCorr + meanN3AngleCorr; 
       int binAngleCorr = meanNHitsAngleCorr*_nBins/(float)_maxNHits;

       finalCutsSystematicsAngleCorr[binAngleCorr].first += nTotalEntries;
       finalCutsSystematicsAngleCorr[binAngleCorr].second.first += finalCutsSystematics.at(iBin).first*nTotalEntries;
       finalCutsSystematicsAngleCorr[binAngleCorr].second.second += finalCutsSystematics.at(iBin).second*nTotalEntries; 

       int meanNHitsCorr = meanN1Corr + meanN2Corr + meanN3Corr;
       int binCorr = meanNHitsCorr*_nBins/(float)_maxNHits;

       finalCutsSystematicsIntCorr[binCorr].first += nTotalEntries;
       finalCutsSystematicsIntCorr[binCorr].second.first += finalCutsSystematics.at(iBin).first*nTotalEntries;
       finalCutsSystematicsIntCorr[binCorr].second.second += finalCutsSystematics.at(iBin).second*nTotalEntries; 
	 
  }

  std::vector<float> xHits, exHits, yNHits, eyNHitsDown, eyNHitsUp, yAngleCorrNHits, eyAngleCorrNHitsUp, eyAngleCorrNHitsDown, yIntCorrNHits, eyIntCorrNHitsUp, eyIntCorrNHitsDown;
  xHits = exHits = yNHits = eyNHitsDown = eyNHitsUp = yAngleCorrNHits = eyAngleCorrNHitsUp = eyAngleCorrNHitsDown = yIntCorrNHits = eyIntCorrNHitsUp = eyIntCorrNHitsDown = {}; 

  for(int iBin = 0; iBin < _nBins; iBin++) {
       
       exHits.push_back(0.5*_maxNHits/(float)_nBins);

       float eyUp, eyDown;
       eyUp = eyDown = 0.;

       xHits.push_back((iBin + 0.5)*_maxNHits/(float)_nBins);
       yNHits.push_back(histograms1D.at("NHits")->GetBinContent(iBin + 1));       
       
       if(finalCutsSystematics.find(iBin) != finalCutsSystematics.end()) {
	    eyUp = finalCutsSystematics.at(iBin).first*yNHits.back();
	    eyDown = finalCutsSystematics.at(iBin).second*yNHits.back();
       }

       eyNHitsUp.push_back(eyUp);
       eyNHitsDown.push_back(eyDown);

       eyUp = eyDown = 0.;
       yAngleCorrNHits.push_back(histograms1D.at("NHitsAngleCorr")->GetBinContent(iBin + 1));     
	   
       if(finalCutsSystematicsAngleCorr.find(iBin) != finalCutsSystematicsAngleCorr.end()) {
	    eyUp = finalCutsSystematicsAngleCorr.at(iBin).second.first*yAngleCorrNHits.back()/(float)finalCutsSystematicsAngleCorr.at(iBin).first;
	    eyDown = finalCutsSystematicsAngleCorr.at(iBin).second.second*yAngleCorrNHits.back()/(float)finalCutsSystematicsAngleCorr.at(iBin).first;
	    
       }
       
       eyAngleCorrNHitsUp.push_back(eyUp);
       eyAngleCorrNHitsDown.push_back(eyDown);

       eyUp = eyDown = 0.;

       yIntCorrNHits.push_back(histograms1D.at("NHitsCorr")->GetBinContent(iBin + 1));     
	    
       if(finalCutsSystematicsIntCorr.find(iBin) != finalCutsSystematicsIntCorr.end()) {
	    eyUp = finalCutsSystematicsIntCorr.at(iBin).second.first*yIntCorrNHits.back()/(float)finalCutsSystematicsIntCorr.at(iBin).first;
	    eyDown = finalCutsSystematicsIntCorr.at(iBin).second.second*yIntCorrNHits.back()/(float)finalCutsSystematicsIntCorr.at(iBin).first;
       }   

       eyIntCorrNHitsUp.push_back(eyUp);
       eyIntCorrNHitsDown.push_back(eyDown);       

  }

  graphs1D["NHitsCutsSystematics"] =  new TGraphAsymmErrors(xHits.size(), xHits.data(), yNHits.data(), exHits.data(), exHits.data(), eyNHitsDown.data(), eyNHitsUp.data());
  graphs1D.at("NHitsCutsSystematics")->SetName("NHitsCutsSystematics");

  graphs1D["NHitsCutsSystematicsAngleCorr"] =  new TGraphAsymmErrors(xHits.size(), xHits.data(), yAngleCorrNHits.data(), exHits.data(), exHits.data(), eyAngleCorrNHitsDown.data(), eyAngleCorrNHitsUp.data());
  graphs1D.at("NHitsCutsSystematicsAngleCorr")->SetName("NHitsCutsSystematicsAngleCorr");

  graphs1D["NHitsCutsSystematicsIntCorr"] =  new TGraphAsymmErrors(xHits.size(), xHits.data(), yIntCorrNHits.data(), exHits.data(), exHits.data(), eyIntCorrNHitsDown.data(), eyIntCorrNHitsUp.data());
  graphs1D.at("NHitsCutsSystematicsIntCorr")->SetName("NHitsCutsSystematicsIntCorr");
  
  // -------------------

  streamlog_out(DEBUG7) << "Compute angle corr systematics." << std::endl;

  std::map<int, std::pair<float,float>> angleCorrSystematics = {};
  std::map<int, std::pair<int,std::pair<float,float>>> angleCorrSystematicsIntCorr = {};

  for(int iBin = 0; iBin < _nBins; iBin++) {

       int nHits, nHitsUp, nHitsDown;
       nHits = histograms1D.at("NHitsAngleCorr")->GetBinContent(iBin + 1);
       nHitsUp = histograms1D.at("NHitsAngleCorrUp")->GetBinContent(iBin + 1);
       nHitsDown = histograms1D.at("NHitsAngleCorrDown")->GetBinContent(iBin + 1);
       
       float eUp = TMath::Max(TMath::Max(nHitsUp - nHits, nHitsDown - nHits),0)/(float)nHits;
       float eDown = TMath::Max(TMath::Max(nHits - nHitsUp, nHits - nHitsDown),0)/(float)nHits;
       
       angleCorrSystematics[iBin].first = eUp;
       angleCorrSystematics[iBin].second = eDown;

       if(binEntriesMapAngleCorr.find(iBin) == binEntriesMapAngleCorr.end()) continue;

       int meanN1Corr = meanNPerBinAngleCorr.at(iBin)[0] + corrN1*(1.0 - meanTimeInSpillPerBinAngleCorr.at(iBin));
       int meanN2Corr = meanNPerBinAngleCorr.at(iBin)[1] + corrN2*(1.0 - meanTimeInSpillPerBinAngleCorr.at(iBin));
       int meanN3Corr = meanNPerBinAngleCorr.at(iBin)[2] + corrN3*(1.0 - meanTimeInSpillPerBinAngleCorr.at(iBin));
       int meanNCorr = meanN1Corr + meanN2Corr + meanN3Corr;

       int binCorr = meanNCorr*_nBins/(float)_maxNHits;
       
       std::cout << "Bin: " << iBin << " -> " << binCorr << ". N1: " << meanN1Corr << " N2: " << meanN2Corr << " N3: " << meanN3Corr << " T: " << meanTimeInSpillPerBinAngleCorr.at(iBin) << std::endl;

       angleCorrSystematicsIntCorr[binCorr].first += binEntriesMapAngleCorr.at(iBin);
       angleCorrSystematicsIntCorr[binCorr].second.first += TMath::Min(eUp*binEntriesMapAngleCorr.at(iBin)/(float)histograms1D.at("NHitsCorr")->GetBinContent(binCorr + 1), eUp);
       angleCorrSystematicsIntCorr[binCorr].second.second += eDown*binEntriesMapAngleCorr.at(iBin)/(float)histograms1D.at("NHitsCorr")->GetBinContent(binCorr + 1);

  }

  yNHits.clear();
  eyNHitsUp.clear();
  eyNHitsDown.clear();

  yIntCorrNHits.clear();
  eyIntCorrNHitsUp.clear();
  eyIntCorrNHitsDown.clear();

  for(int iBin = 0; iBin < _nBins; iBin++) {
       
       yNHits.push_back(histograms1D.at("NHitsAngleCorr")->GetBinContent(iBin + 1));       

       float eyUp, eyDown;
       eyUp = eyDown = 0.;

       if(angleCorrSystematics.find(iBin) != angleCorrSystematics.end()) {
	    eyUp = angleCorrSystematics.at(iBin).first*yNHits.back();
	    eyDown = angleCorrSystematics.at(iBin).second*yNHits.back();
       }

       std::cout << "AngleCorr Bin: " << iBin << " EUp: " << eyUp/yNHits.back() << " EDown: " << eyDown/yNHits.back() << " Y: " << yNHits.back()  << " FEUp" << eyUp << " FEDown " << eyDown << std::endl; 

       eyNHitsUp.push_back(eyUp);
       eyNHitsDown.push_back(eyDown);
      
       yIntCorrNHits.push_back(histograms1D.at("NHitsCorr")->GetBinContent(iBin + 1));

       eyUp = eyDown = 0;

       if(angleCorrSystematicsIntCorr.find(iBin) != angleCorrSystematicsIntCorr.end()) {
	    eyUp = angleCorrSystematicsIntCorr.at(iBin).second.first*yIntCorrNHits.back();
	    eyDown = angleCorrSystematicsIntCorr.at(iBin).second.second*yIntCorrNHits.back();
       }

       std::cout << "IntCorr Bin: " << iBin << " EUp: " << eyUp/yIntCorrNHits.back() << " EDown: " << eyDown/yIntCorrNHits.back() << "Y: " << yIntCorrNHits.back() << " FEUp" << eyUp << " FEDown " << eyDown << std::endl; 

       eyIntCorrNHitsUp.push_back(eyUp);
       eyIntCorrNHitsDown.push_back(eyDown);

  }

  graphs1D["NHitsAngleCorrSystematics"] =  new TGraphAsymmErrors(xHits.size(), xHits.data(), yNHits.data(), exHits.data(), exHits.data(), eyNHitsDown.data(), eyNHitsUp.data());
  graphs1D.at("NHitsAngleCorrSystematics")->SetName("NHitsAngleCorrSystematics");

  graphs1D["NHitsAngleCorrSystematicsIntCorr"] =  new TGraphAsymmErrors(xHits.size(), xHits.data(), yIntCorrNHits.data(), exHits.data(), exHits.data(), eyIntCorrNHitsDown.data(), eyIntCorrNHitsUp.data());
  graphs1D.at("NHitsAngleCorrSystematicsIntCorr")->SetName("NHitsAngleCorrSystematicsIntCorr");



  // -------------------
    
  /*std::map<int, std::pair<float,float>> finalNHitsSystematics = {};
  std::map<int, int> correctedBinEntries = {};

  std::map<int, std::pair<float,float>> finalESystematics = {};
  std::map<int, int> EBinEntries = {};

    
  for(int iBin = 0; iBin < _nBins; iBin++) {
    if(binRunEntriesMap.find(iBin) == binRunEntriesMap.end()) continue;
    if(nHitsSystematicsMap.find(iBin) == nHitsSystematicsMap.end()) continue;
    
    for(auto runIt = nHitsSystematicsMap.at(iBin).begin(); runIt != nHitsSystematicsMap.at(iBin).end(); runIt++) {
	 if(meanNTPerBin.at(iBin).find(runIt->first) == meanNTPerBin.at(iBin).end()) continue;
	 
      
      int meanN1corrected = meanNTPerBin.at(iBin).at(runIt->first)[0] + corrN1*(1.0 - meanTimeInSpillPerBin.at(iBin).at(runIt->first));
      int meanN2corrected = meanNTPerBin.at(iBin).at(runIt->first)[1] + corrN2*(1.0 - meanTimeInSpillPerBin.at(iBin).at(runIt->first));
      int meanN3corrected = meanNTPerBin.at(iBin).at(runIt->first)[2] + corrN3*(1.0 - meanTimeInSpillPerBin.at(iBin).at(runIt->first));
      
      int meanNcorrected = meanN1corrected + meanN2corrected + meanN3corrected;
      int correctedBinNHits = (int)((_nBins/(float)_maxNHits)*meanNcorrected);
      
      if(finalNHitsSystematics.find(correctedBinNHits) == finalNHitsSystematics.end()) {
	finalNHitsSystematics[correctedBinNHits] = std::pair<float,float>(0.,0.);
      }

      correctedBinEntries[correctedBinNHits] += binRunEntriesMap.at(iBin).at(runIt->first);
      
      int binContent = histograms1D.at("NHitsCorrected" )->GetBinContent(correctedBinNHits + 1);
      float systIntensityUp, systIntensityDown;
      systIntensityUp = systIntensityDown = 0.0;
      
      if(binContent != 0) {
	systIntensityUp = (histograms1D.at("NHitsCorrectedUp" )->GetBinContent(correctedBinNHits + 1) - binContent)/(float)binContent;
	systIntensityDown = (histograms1D.at("NHitsCorrectedDown" )->GetBinContent(correctedBinNHits + 1) - binContent)/(float)binContent;
      }

      float finalSystIntensityUp = TMath::Abs(TMath::Max(TMath::Max(systIntensityUp,systIntensityDown),0.f));
      float finalSystIntensityDown = TMath::Abs(TMath::Min(TMath::Min(systIntensityUp,systIntensityDown),0.f));
      
      finalNHitsSystematics[correctedBinNHits].first += ((1+runIt->second.first)*(1+finalSystIntensityUp) - 1)*binRunEntriesMap.at(iBin).at(runIt->first);
      
      finalNHitsSystematics[correctedBinNHits].second += (1-(1-runIt->second.second)*(1-finalSystIntensityDown))*binRunEntriesMap.at(iBin).at(runIt->first);

      float meanE = alpha->Eval(meanNcorrected)*meanN1corrected + beta->Eval(meanNcorrected)*meanN2corrected + gamma->Eval(meanNcorrected)*meanN3corrected;

      int binE = (int)((_nBins/(float)_maxE)*meanE);
      
      if(finalESystematics.find(binE) == finalESystematics.end()) {
	finalESystematics[binE] = std::pair<float,float>(0.,0.);
      }
      
      EBinEntries[binE] += binRunEntriesMap.at(iBin).at(runIt->first);
      
      finalESystematics[binE].first += ((1+runIt->second.first)*(1+finalSystIntensityUp) - 1)*binRunEntriesMap.at(iBin).at(runIt->first);
      finalESystematics[binE].second += (1-(1-runIt->second.second)*(1-finalSystIntensityDown))*binRunEntriesMap.at(iBin).at(runIt->first);  
      
    } // end loop over runs

  } // end loop over bins

  std::vector<float> xHits, exHits, yHits, eyUpHits, eyDownHits, eyStat, eyFullUpHits, eyFullDownHits;
  xHits = exHits = yHits = eyUpHits = eyDownHits = eyStat = eyFullUpHits = eyFullDownHits = {};


  streamlog_out(DEBUG7) << "Compute final Energy systematics" << std::endl;

  for(auto binIt = finalNHitsSystematics.begin(); binIt != finalNHitsSystematics.end(); binIt++) {
    
    yHits.push_back(histograms1D.at("NHitsCorrected" )->GetBinContent(binIt->first + 1));
    
    eyUpHits.push_back(binIt->second.first*histograms1D.at("NHitsCorrected" )->GetBinContent(binIt->first + 1)/(float)correctedBinEntries.at(binIt->first));
    eyDownHits.push_back(binIt->second.second*histograms1D.at("NHitsCorrected" )->GetBinContent(binIt->first + 1)/(float)correctedBinEntries.at(binIt->first));       

    float statErrorPerc = TMath::Sqrt(histograms1D.at("NHitsCorrected" )->GetBinContent(binIt->first + 1))/(float)histograms1D.at("NHitsCorrected" )->GetBinContent(binIt->first + 1);
    
    eyStat.push_back(TMath::Sqrt(histograms1D.at("NHitsCorrected" )->GetBinContent(binIt->first + 1)));

    float fullErrorPercUp = (1 + statErrorPerc)*(1 + binIt->second.first/(float)correctedBinEntries.at(binIt->first)) - 1; 
    float fullErrorPercDown = 1 - (1 - statErrorPerc)*(1 - binIt->second.second/(float)correctedBinEntries.at(binIt->first));

    eyFullUpHits.push_back(fullErrorPercUp*histograms1D.at("NHitsCorrected" )->GetBinContent(binIt->first + 1));
    eyFullDownHits.push_back(fullErrorPercDown*histograms1D.at("NHitsCorrected" )->GetBinContent(binIt->first + 1));
    
  }

  graphs1D["NHitsCorrectedSysErrors"] =  new TGraphAsymmErrors(xHits.size(), xHits.data(), yHits.data(), exHits.data(), exHits.data(), eyDownHits.data(), eyUpHits.data());
  graphs1D["NHitsCorrectedSysErrors"]->SetName("NHitsCorrectedSysErrors");
  
  graphs1D["NHitsCorrectedStatErrors"] =  new TGraphAsymmErrors(xHits.size(), xHits.data(), yHits.data(), exHits.data(), exHits.data(), eyStat.data(), eyStat.data());
  graphs1D["NHitsCorrectedStatErrors"]->SetName("NHitsCorrectedStatErrors");

  graphs1D["NHitsCorrectedFullErrors"] =  new TGraphAsymmErrors(xHits.size(), xHits.data(), yHits.data(), exHits.data(), exHits.data(), eyFullDownHits.data(), eyFullUpHits.data());
  graphs1D["NHitsCorrectedFullErrors"]->SetName("NHitsCorrectedFullErrors");
  
  std::vector<float> xE, exE, yE, eyUpE, eyDownE, eyStatE, eyFullUpE, eyFullDownE;
  xE = exE = yE = eyUpE = eyDownE = eyStatE = eyFullUpE = eyFullDownE = {};

  for(auto binIt = finalESystematics.begin(); binIt != finalESystematics.end(); binIt++) {

    xE.push_back((binIt->first + 0.5)*_maxE/(float)_nBins);
    exE.push_back(0.5*_maxE/(float)_nBins);
    yE.push_back(histograms1D.at("Energy")->GetBinContent(binIt->first + 1));

    eyUpE.push_back(binIt->second.first*histograms1D.at("Energy")->GetBinContent(binIt->first + 1)/(float)EBinEntries.at(binIt->first));
    eyDownE.push_back(binIt->second.second*histograms1D.at("Energy")->GetBinContent(binIt->first + 1)/(float)EBinEntries.at(binIt->first));

    float statErrorPerc = TMath::Sqrt(histograms1D.at("Energy")->GetBinContent(binIt->first + 1))/(float)histograms1D.at("Energy")->GetBinContent(binIt->first + 1);
    
    eyStatE.push_back(TMath::Sqrt(histograms1D.at("Energy")->GetBinContent(binIt->first + 1)));

    float fullErrorPercUp = (1 + statErrorPerc)*(1 + binIt->second.first/(float)EBinEntries.at(binIt->first)) - 1; 
    float fullErrorPercDown = 1 - (1 - statErrorPerc)*(1 - binIt->second.second/(float)EBinEntries.at(binIt->first));

    eyFullUpE.push_back(fullErrorPercUp*histograms1D.at("Energy")->GetBinContent(binIt->first + 1));
    eyFullDownE.push_back(fullErrorPercDown*histograms1D.at("Energy")->GetBinContent(binIt->first + 1));
    
  }

  streamlog_out(DEBUG7) << "Creating Graphs" << std::endl;    

  graphs1D["ESysErrors"] =  new TGraphAsymmErrors(xE.size(), xE.data(), yE.data(), exE.data(), exE.data(), eyDownE.data(), eyUpE.data());
  graphs1D["ESysErrors"]->SetName("ESysErrors");
  
  graphs1D["EStatErrors"] =  new TGraphAsymmErrors(xE.size(), xE.data(), yE.data(), exE.data(), exE.data(), eyStatE.data(), eyStatE.data());
  graphs1D["EStatErrors"]->SetName("EStatErrors");

  graphs1D["EFullErrors"] =  new TGraphAsymmErrors(xE.size(), xE.data(), yE.data(), exE.data(), exE.data(), eyFullDownE.data(), eyFullUpE.data());
  graphs1D["EFullErrors"]->SetName("EFullErrors");
*/
}


void SemiDigitalEnergyReconstructionProc::writeHistograms(bool writeAll)
{

  streamlog_out(DEBUG7) << "Writting histograms for run: " << runNumber << std::endl;
  
  _outputFile->cd((std::to_string(beamEnergy) + "GeV").c_str());

  if(writeAll) {
       for(auto It1D = graphs1D.begin(); It1D != graphs1D.end(); It1D++) {
	    It1D->second->Write();
	    delete It1D->second;
       }
  }

  auto It1D = histograms1D.begin();
  while(It1D != histograms1D.end()){
    if(It1D->first.rfind("Run",0) == 0) {
	 _outputFile->cd((std::to_string(beamEnergy) + "GeV/" + std::to_string(runNumber)).c_str());
    }
    else if(writeAll) _outputFile->cd((std::to_string(beamEnergy) + "GeV").c_str());
    else { It1D++; continue; }

    It1D->second->Write();
    delete It1D->second;
    It1D = histograms1D.erase(It1D);
    }
  
}
