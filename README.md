# SDHCAL-AngleAnalysis

Aalysis about the effect of the incident angle with the sdhcal prototype.

Year of the data -> 2015
Year of the analysis -> 2020
ILCSoft version -> v02-01

Streamout and event reconstruction based on SDHCAL_RawData and Trivent_v02.2.2

The first thing to do allways is to initialize ilcsoft:

    source $ILCSOFT/init_ilcsoft.sh  (where $ILCSOFT is the path to the ilcosft installation)


#### --- PRODUCTION CUTS AND SELECTIONS --- ####

SDHCAL_RawData -> If an ASIC has al 64 pads fired then those hits are removed.
Trivent ->
	Skipping DIFs: 3 182 105 80
	Event cuts: nSets == 1 ; nHit_10 >= 4 ; nHit_6 >= 3 

#### -------------- INSTALL --------------- ####

    ./script/build.sh Full
		  


#### -------------- COMPILE --------------- ####

To just recompile modified files:

    ./script/build.sh



#### -------------- RUNNING --------------- ####

After initializing ilcsoft the procesors need to be loaded:

    source Load_MarlinDLL.sh

Then the the steer files for Marlin are in ./steer/

Modify the parameter LCIOInputFiles to the path of the file/s you want to process and then:

    Marlin ./steer/steerFile


#### ------------ PROCESSORS -------------- ####

     SDHCAL_RawData: Converts Raw Stream into LCIO Objects
     
Trivent:
	- Electronic noise cuts and time reconstruction of the events.
	- Encoding of output hits: I:7,J:7,K:10,Dif_id:8,Asic_id:6,Chan_id:7

################## CHECK ##################################


	- Produces two collections SDHCAL_BeamMultiEvents (Added to the LCEvent) and SDHCAL_BeamEvents (Saved in the output .slcio output)

     ROOTMaker: Creates the ROOT Files with events in the Tree and displays events following selection cuts

     ParticleSelector: Takes beam collections and applies cuts based on the density, second maximum of hits and the penetrability condition to separate muons, cosmics rays and shower into different collections. #Based on 2018 analysis. Requires refinement at lower energies.# Output collection names:

         - MuonEvents, CosmicEvents and ShowerEvents.

     MuonAnalysis: Gets a muon collection, clusterizes all hits, reconstructs the tracks of the muon and computes its angles in X and Y. Also computes the multiplicity and efficiency for each layer of the SDHCal using the tracks. 


#### -------------- STEER ----------------- ####

1) Streamout_Trivent.xml

   - SDHCAL_RawData
   - Trivent
   - ROOTMaker

2) ROOT_Analyses.xml

   - ROOTMaker

3) Analysis.xml

   - ParticleSelector
   - MuonAnalysis
   - ROOTMaker



#### -------------- OUTPUT ---------------- ####

*_logROOT.root -> ROOT File with some output and debug histograms

*.slcio -> File with the proccesed events. By default the name of the collection with the hits is "SDHCAL_HIt". If unsure use anajob or dumpevent on this file.

Output_${CollectionName}.root -> ROOT File holding the event display and the Tree with the events for that collection. Name of the Tree by default: "SDHCAL"



#### ------------- ANALYSIS --------------- ####


v01.00 (May 2020) -> Reading the raw stram of data from the sdhcal DAQ output file (.slcio format), doing time event reconstruction and applying cuts to select beam events.

       - ASIC events with all 64 pads fired at the same time removed.
       - Readouts with more than 200000 hits considered noise and removed.
       - Events reconstructed if more than 7 hits in the same time bin. Time window = 1
       - Events with less than 7 layers with hits removed.

v01.01 (June 2020) -> Applies cuts to the beam events to separate muons, cosmics rays and showers. Then reconstructs the tracks of muons and apply cuts to discard tracks with a big slope in the Y axis and tracks with few remaining layers with signal. Finally, the efficiency and multiplicity of the SDHCAL is computed using the tracks.

       - To separate the showers from the muons and cosmic rays cuts to density and second maximum of hits in a single layer are applied. Default: density < 3 || secondMax < 5
       - To distinguish between muons and consmics rays the PC is checked. If true the event is a muon.
       #Note: this selection needs to be revisited for low energies#
       - The cuts to the muon tracks are: slope_Y < 0.04 and nLayers with signal > 5
       - Definitions ->
       	 	     - Efficiency: a layer is said to be efficient if for an event accepted as a muon track there is a cluster associated to the track. 
		     - Multiplicity: the size of the clusters associated to the muon tracks.
       - This has to be repeated for many tracks to have an statistically meaningful result.