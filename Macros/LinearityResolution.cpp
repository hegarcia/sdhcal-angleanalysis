#include "ComplexFunctions.h"

void LinearityResolution(string inputFileName, string outFileBaseName = "LinearityResolution")
{
     
     CF_Handler complexF;
     TFile* outputFile = new TFile((outFileBaseName + ".root").c_str(),"RECREATE");

     TFile* inputFile = new TFile(inputFileName.c_str(), "READ");

     std::vector<float> beamE, beamEErr, recoE, sigma, res, resErr, dev, devErr;
     beamE = beamEErr = recoE = sigma = res = resErr = dev = devErr = {};

     for(const auto&& key: *inputFile->GetListOfKeys()) {
	  TString keyName = key->GetName();
	  if(!(keyName.EndsWith("GeV"))) continue;

	  inputFile->cd(keyName);
	  TH1F* energyGraph = (TH1F*)inputFile->Get(keyName + "/Energy");
	  float max = energyGraph->GetBinContent(energyGraph->GetMaximumBin());
	  /*for(int iPoint = 0; iPoint < energyGraph->GetN(); iPoint++) {
	       double x, y; 
	       energyGraph->GetPoint(iPoint, x, y); 
	       if(y > max) max = y;
	       }*/

	  keyName.ReplaceAll("GeV","");
	  float beamEnergy = keyName.Atof();
	  
	  std::vector<double> parsInitials;
	  float recoEnergy, recoEnergyE, sigmaFit, sigmaFitE;

	  outputFile->cd();

	  TCanvas* ECanvas = new TCanvas(keyName + "GeV");
	  energyGraph->Draw("HISTE");
        
	  complexF.changeMode(0);
	  parsInitials = { 1.0, 2.0, beamEnergy, beamEnergy*0.1, max};
	  complexF.setParsInitialValues(parsInitials.data());

	  TFitResultPtr fitResCB = energyGraph->Fit("Complex","QE0S");
	  TF1* fitCB = new TF1(*energyGraph->GetFunction("Complex"));
	  double qualityCBFit = 1000;

	  if(fitResCB->IsValid()) {
	       qualityCBFit = fitResCB->Chi2()/fitResCB->Ndf();
	       fitCB->SetLineColor(kGreen);
	       fitCB->Draw("Same");
	  }

	  complexF.changeMode(4);
	  parsInitials = {beamEnergy, beamEnergy*0.1, 5.0, 1.0, max};
	  complexF.setParsInitialValues(parsInitials.data());
	  
	  TFitResultPtr fitResLandGaus = energyGraph->Fit("Complex","QE0S");
	  TF1* fitLandGaus = energyGraph->GetFunction("Complex");
	  double qualityLandGausFit = 1000;      

	  if(fitResLandGaus->IsValid()) {
	       qualityLandGausFit = fitResLandGaus->Chi2()/fitResLandGaus->Ndf();
	       fitLandGaus->SetLineColor(kBlue);
	       fitLandGaus->Draw("Same");
	  }


	  TFitResultPtr fitResGaus = energyGraph->Fit("gaus","QE0S");
	  TF1* fitGaus = energyGraph->GetFunction("gaus");
	  double qualityGausFit = 1000;      

	  if(fitResGaus->IsValid()) {
	       qualityGausFit = fitResGaus->Chi2()/fitResGaus->Ndf();
	       fitGaus->SetLineColor(kRed);
	       fitGaus->Draw("Same");
	  }

	  TF1* selFunc;
	  if(qualityCBFit < qualityLandGausFit && qualityCBFit < qualityGausFit) {
	       
	       const double* params = fitResCB->GetParams();
	       const double* errors = fitResCB->GetErrors();
	      
	       std::cout << "Selecting CB" << std::endl;

	       selFunc = fitCB;
	       recoEnergy = params[2];
	       
	       sigmaFit = params[3];
	       sigmaFitE = errors[3];
	       
	  }
	  else if(qualityLandGausFit < qualityGausFit){

	       std::cout << "Selecting Land Gaus" << std::endl;

	       const double* params = fitResLandGaus->GetParams();
	       const double* errors = fitResLandGaus->GetErrors();
	      
	       selFunc = fitLandGaus;
	       recoEnergy = params[0];
	       
	       sigmaFit = params[1];
	       sigmaFitE = errors[1];
	       
	       
	  }
	  else {

	       std::cout << "Selecting Gaus" << std::endl;

	       const double* params = fitResGaus->GetParams();
	       const double* errors = fitResGaus->GetErrors();
      
	       selFunc = fitGaus;
	       recoEnergy = params[0];
	       
	       sigmaFit = params[1];
	       sigmaFitE = errors[1];
	  }
	       
	  recoE.push_back(recoEnergy);
	  sigma.push_back(sigmaFit);
	  
	  dev.push_back((recoEnergy-beamEnergy)/recoEnergy);
	  devErr.push_back(sigmaFit/recoEnergy);

	  res.push_back(sigmaFit/recoEnergy);

	  std::cout << recoEnergy << std::endl;
	  std::cout << sigmaFitE << std::endl;
	  std::cout << sigmaFitE/recoEnergy << std::endl;

	  resErr.push_back(sigmaFitE/recoEnergy);

	  beamE.push_back(beamEnergy);
	  beamEErr.push_back(beamEnergy*0.01);

	  energyGraph->GetListOfFunctions()->Clear();
	  energyGraph->GetListOfFunctions()->Add(selFunc);
	  
	  ECanvas->Write();
	  delete ECanvas;

     } 
     
     outputFile->cd();

     std::cout << "Computing linearity and resolution" << std::endl;

     TGraphErrors* linearity = new TGraphErrors(beamE.size(), beamE.data(), recoE.data(), beamEErr.data(), sigma.data());
     linearity->GetXaxis()->SetLimits(0.,90.);
     linearity->SetMaximum(90.);
     linearity->SetMinimum(0.);
     linearity->SetName("Linearity");
     linearity->SetTitle(";E_{Beam};E_{Reco}");
     linearity->Fit("pol1", "QES");
     linearity->Write();
     delete linearity;

     TGraphErrors* deviations = new TGraphErrors(beamE.size(), beamE.data(), dev.data(), beamEErr.data(), devErr.data());
     deviations->SetName("Deviations");
     deviations->SetTitle(";E_{Beam};(E_{Reco} - E_{Beam})/E_{Reco}");
     deviations->Write();
     delete deviations;

     TGraphErrors* resolution = new TGraphErrors(beamE.size(), beamE.data(), res.data(), beamEErr.data(), resErr.data());
     resolution->SetTitle(";E_{Beam};#sigma/E_{Reco}");
     resolution->SetName("Resolution");
     resolution->Write();
     
     outputFile->Close();
     delete outputFile;

}


