void EffAndMult(string inputFileNames = "", string outFileBaseName = "EffAndMult")
{

     TFile* outputFile = new TFile((outFileBaseName + ".root").c_str(),"RECREATE");
     
     std::vector<std::string> files = {};
     std::string fileName = "";
     for(int iChar = 0; iChar <= inputFileNames.size(); iChar++) {
	  
	  if(iChar == inputFileNames.size() || inputFileNames.at(iChar) == ' ') {
	       files.push_back(fileName);
	       fileName.clear();
	       continue;
	  }
	  
	  fileName += inputFileNames.at(iChar);
	  
     }

     gStyle->SetOptStat(0000);

     std::vector<float> meanEffs, meanMults, meanEffsErrors, meanMultsErrors, angles, anglesRad, anglesErr, anglesRadErr;
     meanEffs = meanMults = meanEffsErrors = meanMultsErrors = angles = anglesRad = anglesErr, anglesRadErr = {};

     for(auto fileIt = files.begin(); fileIt != files.end(); fileIt++) {

	  TFile* inputFile = new TFile((*fileIt).c_str(), "READ");
	  if(!inputFile->IsOpen()) {
	       std::cout << "Could not open file: " << *fileIt << std::endl;
	       exit(0);
	  }
	  float angle = TMath::ATan(((TH1F*)inputFile->Get("GlobalSlopeX"))->GetMean())*180.0/TMath::Pi();
	  float angleErr = TMath::ATan(((TH1F*)inputFile->Get("GlobalSlopeX"))->GetStdDev())*180.0/TMath::Pi();
	  angles.push_back(angle);
	  anglesErr.push_back(angleErr);
	  anglesRad.push_back(angle*TMath::Pi()/180.);
	  anglesRadErr.push_back(angleErr*TMath::Pi()/180.);

	  outputFile->mkdir((std::to_string(angle) + "Deg").c_str());
	  outputFile->cd((std::to_string(angle) + "Deg").c_str());

	  TGraph* efficiency = (TGraph*)inputFile->Get("TotalLayersEfficiency");
	  efficiency->SetMinimum(60);
	  efficiency->SetMarkerStyle(21);
	  efficiency->SetMarkerColor(kBlue);
	  efficiency->SetMarkerSize(0.6);
	  TF1* meanEff = efficiency->GetFunction("MeanLayerEfficiency");

	  std::vector<float> x, y, ex, ey;
	  x = y = ey = {};

	  for(int iLayer = 0; iLayer < 49; iLayer++) {
	       x.push_back(iLayer);
	       y.push_back(meanEff->GetParameter(0)); 
	       ey.push_back(meanEff->GetParError(0));
	  }
	  
	  TGraphErrors* meanEffBand = new TGraphErrors(x.size(), x.data(), y.data(), 0, ey.data());
	  meanEffBand->SetFillColor(kGreen);
	  meanEffBand->SetFillStyle(3005);
 
	  meanEffs.push_back(meanEff->GetParameter(0));
	  meanEffsErrors.push_back(meanEff->GetParError(0));

	  TCanvas* effCanvas = new TCanvas("EffCanvas");
	  efficiency->Draw("AP");
	  meanEffBand->Draw("Same3");
	  effCanvas->Write();
	  delete effCanvas;

	  TGraph* multiplicity = (TGraph*)inputFile->Get("TotalLayersMultiplicity");
	  multiplicity->SetMarkerStyle(22);
	  multiplicity->SetMarkerColor(kRed);
	  multiplicity->SetMarkerSize(0.7);
	  TF1* meanMult = multiplicity->GetFunction("MeanLayerMultiplicity");
        
	  y = ey = {};

	  for(int iLayer = 0; iLayer < 49; iLayer++) {
	       y.push_back(meanMult->GetParameter(0)); 
	       ey.push_back(meanMult->GetParError(0));
	  }
        
	  TGraphErrors* meanMultBand = new TGraphErrors(x.size(), x.data(), y.data(), 0, ey.data());
	  meanMultBand->SetFillColor(kBlack);
	  meanMultBand->SetFillStyle(3005);
 
	  meanMults.push_back(meanMult->GetParameter(0));
	  meanMultsErrors.push_back(meanMult->GetParError(0)*0.5);	  

	  TCanvas* multCanvas = new TCanvas("MultCanvas");
	  multiplicity->Draw("APE");
	  meanMultBand->Draw("same4");	  
	  multCanvas->Write();
	  delete multCanvas;

	  TDirectory* statsDir = inputFile->GetDirectory("TotalStats",true);
	  std::vector<TH2F*> stats = {};
	  for(const auto&& key: *statsDir->GetListOfKeys()) {
	       stats.push_back((TH2F*)statsDir->Get(key->GetName()));
	  }
	  	  
	  outputFile->mkdir((std::to_string(angle) + "Deg/Stats").c_str());
	  outputFile->cd((std::to_string(angle) + "Deg/Stats").c_str());
	  
	  TCanvas* canvas[6];
	  canvas[0] = new TCanvas("CanvasStats_1","",0,0,2160,2160);
	  canvas[0]->Divide(4,4);
	  canvas[1] = new TCanvas("CanvasStats_2","",0,0,2160,2160);
	  canvas[1]->Divide(4,4);
	  canvas[2] = new TCanvas("CanvasStats_3","",0,0,2160,2160);
	  canvas[2]->Divide(4,4);
	  canvas[3] = new TCanvas("CanvasStats_4","",0,0,2160,2160);
	  canvas[3]->Divide(4,4);      
	  
	  for(int iHist = 0; iHist < stats.size(); iHist++) {
	       std::string name = stats.at(iHist)->GetName();
	       int layer =  std::stoi(name.substr(name.rfind("Layer") + 5));
	       int iCanvas = layer/16;
	       canvas[iCanvas]->cd(layer%16 + 1);
	       stats.at(iHist)->Draw("COLZ");
	  }
	  
	  canvas[0]->Write();
	  canvas[1]->Write();
	  canvas[2]->Write();
	  canvas[3]->Write();
	  delete canvas[0];
	  delete canvas[1];
	  delete canvas[2];
	  delete canvas[3];

	  gStyle->SetPalette(kDarkBodyRadiator);
	  
	  TDirectory* effDir = inputFile->GetDirectory("TotalHitEfficiencies",true);
	  std::vector<TH2F*> effs = {};
	  for(const auto&& key: *effDir->GetListOfKeys()) {
	       effs.push_back((TH2F*)effDir->Get(key->GetName()));
	  }

	  outputFile->mkdir((std::to_string(angle) + "Deg/HitEfficiencies").c_str());
	  outputFile->cd((std::to_string(angle) + "Deg/HitEfficiencies").c_str());
	  
	  canvas[0] = new TCanvas("CanvasHitEffs_1","",0,0,2160,2160);
	  canvas[0]->Divide(4,4);
	  canvas[1] = new TCanvas("CanvasHitEffs_2","",0,0,2160,2160);
	  canvas[1]->Divide(4,4);
	  canvas[2] = new TCanvas("CanvasHitEffs_3","",0,0,2160,2160);
	  canvas[2]->Divide(4,4);
	  canvas[3] = new TCanvas("CanvasHitEffs_4","",0,0,2160,2160);
	  canvas[3]->Divide(4,4);     

	  for(int iHist = 0; iHist < effs.size(); iHist++) {
	       std::string name = effs.at(iHist)->GetName();
	       int layer =  std::stoi(name.substr(name.rfind("Layer") + 5));
	       int iCanvas = layer/16;
	       canvas[iCanvas]->cd(layer%16 + 1);
	       effs.at(iHist)->Draw("COLZ");
	  }
	  
	  canvas[0]->Write();
	  canvas[1]->Write();
	  canvas[2]->Write();
	  canvas[3]->Write();
	  delete canvas[0];
	  delete canvas[1];
	  delete canvas[2];
	  delete canvas[3];


	  TDirectory* ASICEffDir = inputFile->GetDirectory("TotalASICEfficiencies",true);
	  std::vector<TH2F*> ASICEffs = {};
	  for(const auto&& key: *ASICEffDir->GetListOfKeys()) {
	       ASICEffs.push_back((TH2F*)ASICEffDir->Get(key->GetName()));
	  }

	  TDirectory* ASICBoxesDir = inputFile->GetDirectory("TotalASICStatBoxes",true);
	  std::vector<TPaveText*> ASICBoxes = {};
	  for(const auto&& key: *ASICBoxesDir->GetListOfKeys()) {
	       ASICBoxes.push_back((TPaveText*)ASICBoxesDir->Get(key->GetName()));
	  }

	  outputFile->mkdir((std::to_string(angle) + "Deg/ASICEfficiencies").c_str());
	  outputFile->cd((std::to_string(angle) + "Deg/ASICEfficiencies").c_str());
	  
	  for(int iHist = 0; iHist < ASICEffs.size(); iHist++) {
	       std::string name = ASICEffs.at(iHist)->GetName();
	       int layer =  std::stoi(name.substr(name.rfind("Layer") + 5));
	       TCanvas* newCanvas = new TCanvas(("CanvasASICEffs_" + std::to_string(layer)).c_str());
	       ASICEffs.at(iHist)->Draw("COLZ");
	       for(auto box : ASICBoxes){
		    std::string boxName = box->GetName();
		    int ASICID = std::stoi(boxName.substr(boxName.rfind("_") + 1));
		    if(ASICID/144 == layer) box->Draw("SAME");
	       }
	       newCanvas->Write();
	       delete newCanvas;
	  }
    
	  TDirectory* DIFEffDir = inputFile->GetDirectory("TotalDIFEfficiencies",true);
	  std::vector<TH2F*> DIFEffs = {};
	  for(const auto&& key: *DIFEffDir->GetListOfKeys()) {
	       DIFEffs.push_back((TH2F*)DIFEffDir->Get(key->GetName()));
	  }

	  TDirectory* DIFBoxesDir = inputFile->GetDirectory("TotalDIFStatBoxes",true);
	  std::vector<TPaveText*> DIFBoxes = {};
	  for(const auto&& key: *DIFBoxesDir->GetListOfKeys()) {
	       DIFBoxes.push_back((TPaveText*)DIFBoxesDir->Get(key->GetName()));
	  }
	  	  
	  outputFile->mkdir((std::to_string(angle) + "Deg/DIFEfficiencies").c_str());
	  outputFile->cd((std::to_string(angle) + "Deg/DIFEfficiencies").c_str());
	  
	  canvas[0] = new TCanvas("CanvasDIFEff_1","",0,0,2160,2160);
	  canvas[0]->Divide(3,3);
	  canvas[1] = new TCanvas("CanvasDIFEff_2","",0,0,2160,2160);
	  canvas[1]->Divide(3,3);
	  canvas[2] = new TCanvas("CanvasDIFEff_3","",0,0,2160,2160);
	  canvas[2]->Divide(3,3);
	  canvas[3] = new TCanvas("CanvasDIFEff_4","",0,0,2160,2160);
	  canvas[3]->Divide(3,3);
	  canvas[4] = new TCanvas("CanvasDIFEff_5","",0,0,2160,2160);
	  canvas[4]->Divide(3,3);      
	  canvas[5] = new TCanvas("CanvasDIFEff_6","",0,0,2160,2160);
	  canvas[5]->Divide(3,3);            
	  
	  for(int iHist = 0; iHist < DIFEffs.size(); iHist++) {
	       std::string name = stats.at(iHist)->GetName();
	       int layer =  std::stoi(name.substr(name.rfind("Layer") + 5));
	       int iCanvas = layer/9;
	       canvas[iCanvas]->cd(layer%9 + 1);
	       DIFEffs.at(iHist)->Draw("COLZ");
	       for(auto box : DIFBoxes){
		    std::string boxName = box->GetName();
		    int DIFID = std::stoi(boxName.substr(boxName.rfind("_") + 1));
		    if(DIFID/3 == layer) box->Draw("SAME");
	       }
	  }
	  
	  canvas[0]->Write();
	  canvas[1]->Write();
	  canvas[2]->Write();
	  canvas[3]->Write();
	  canvas[4]->Write();
	  canvas[5]->Write();
	  delete canvas[0];
	  delete canvas[1];
	  delete canvas[2];
	  delete canvas[3];
	  delete canvas[4];
	  delete canvas[5];

     }

     outputFile->cd();

     TGraphErrors* effsPerAngle = new TGraphErrors(angles.size(), angles.data(), meanEffs.data(), anglesErr.data(), meanEffsErrors.data());
     effsPerAngle->SetName("EffsPerAngle");
     effsPerAngle->SetTitle(";Angle(#theta);Eff(%)");
     effsPerAngle->SetMarkerStyle(21);
     effsPerAngle->GetXaxis()->SetLimits(-5,35);
     effsPerAngle->Write();
     delete effsPerAngle;

     TGraphErrors* multsPerAngle = new TGraphErrors(angles.size(), angles.data(), meanMults.data(), anglesErr.data(), meanMultsErrors.data());
     multsPerAngle->SetName("MultsPerAngle");
     multsPerAngle->SetTitle("Angle(#theta);Multiplicity(nHits)");
     multsPerAngle->SetMarkerStyle(22);
     multsPerAngle->GetXaxis()->SetLimits(-5,35);
     multsPerAngle->Write();
     delete multsPerAngle;

     TGraphErrors* multsPerAngleRad = new TGraphErrors(anglesRad.size(), anglesRad.data(), meanMults.data(), anglesRadErr.data(), meanMultsErrors.data());
     
     std::cout << anglesRad.size() << std::endl;

     TF1* cosFunc = new TF1("CosF",(std::to_string(meanMults.at(0)) + "/cos([0]*x)").c_str(), 0, 1.0);
     TF1* sinFunc = new TF1("SinF", "sin([0]*x)", 0, 30);

     multsPerAngleRad->Fit(cosFunc, "E");
     
     sinFunc->SetParameter(0,cosFunc->GetParameter(0));

     vector<float> meanMultsCorr, meanMultsCorrErrUp, meanMultsCorrErrDown;
     meanMultsCorr = meanMultsCorrErrUp = meanMultsCorrErrDown = {};

     for(int iAngle = 0; iAngle < angles.size(); iAngle++) {
	  float corr = meanMults.at(iAngle)*TMath::Cos(cosFunc->GetParameter(0)*anglesRad.at(iAngle));
	  meanMultsCorr.push_back(corr);
	  float errUp, errDown;
	  errUp = (meanMults.at(iAngle) + meanMultsErrors.at(iAngle))*TMath::Cos((cosFunc->GetParameter(0) - cosFunc->GetParError(0))*TMath::Abs(anglesRad.at(iAngle) - anglesRadErr.at(iAngle))) - corr;
	  meanMultsCorrErrUp.push_back(errUp);
	  errDown = corr - (meanMults.at(iAngle) - meanMultsErrors.at(iAngle))*TMath::Cos((cosFunc->GetParameter(0) + cosFunc->GetParError(0))*TMath::Abs(anglesRad.at(iAngle) + anglesRadErr.at(iAngle)));
	  meanMultsCorrErrDown.push_back(errDown);
     }

     multsPerAngleRad->Write();
     delete multsPerAngleRad;

     TGraphAsymmErrors* multsPerAngleCorr = new TGraphAsymmErrors(angles.size(), angles.data(), meanMultsCorr.data(), anglesErr.data(), anglesErr.data(), meanMultsCorrErrDown.data(), meanMultsCorrErrUp.data());
     multsPerAngleCorr->SetName("MultsPerAngleCorr");
     multsPerAngleCorr->SetTitle("Angle(#theta);Multiplicity(nHits)");
     multsPerAngleCorr->SetMarkerStyle(22);
     multsPerAngleCorr->GetXaxis()->SetLimits(-5,35);
     
     TF1* fitFunc = new TF1("FitFunc", "pol1", -5, 35);
     fitFunc->SetParameters(1.7,0.);
     multsPerAngleCorr->Fit(fitFunc);

     multsPerAngleCorr->Write();
     delete multsPerAngleCorr;

     outputFile->Close();
     delete outputFile;

}


