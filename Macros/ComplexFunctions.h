#ifndef __COMPLEXFUNCTIONS__
#define __COMPLEXFUNCTIONS__

/* -------------------------------
   
   Valid function modes:

   0 -> Crystall Ball
   1 -> Breit wigner
   2 -> Gauss
   3 -> CB*BW
   4 -> Gauss*Landau
   
   Each 

   ------------------------------- */


class ComplexFunctions
{

 public:

     ComplexFunctions() { setMode(); };

     void setMode(int newFuncMode = 0) {

	  mode = newFuncMode; 

	  switch(mode) {
	  case 0: case 3: 
	       nParameters = 5; 
	       parameterNames = {"#alpha", "n", "#mu", "#sigma", "N"};
	       break;
	  case 1: case 2: 
	       nParameters = 3;
	       parameterNames = {"#mu", "#sigma", "N"};
	       break;
	  case 4:
	       nParameters = 5;
	       parameterNames = {"#mu", "#sigma", "MPV", "Width", "N"};
	       break;
	  }

     }

     double operator() (double *x, double* par)
     {
	  double returnValue;
	  double* subpar;

	  switch(mode) {
	  case 0:
	       returnValue = par[nParameters - 1]*CB(x,par); break;
	  case 1:
	       returnValue = par[nParameters - 1]*BW(x,par); break;
	  case 2:
	       returnValue = par[nParameters - 1]*Gaus(x,par); break;
	  case 3:   
	       subpar = (double []){ par[2], par[3] };
	       returnValue = par[nParameters - 1]*CB(x,par)*BW(x,subpar); break;
	  case 4:
	       subpar = (double []){ par[2], par[3] };
	       returnValue = par[nParameters - 1]*(Gaus(x, par)+Landau(x,subpar)); break;
	  };

	  return returnValue;
     }

     int mode;
     int nParameters;
     std::vector<std::string> parameterNames;

 private:

     std::vector<double(*)(double* x, double* par)> funcVec = {};
   
     double CB(double* x, double* par) {

	  double absAlpha = TMath::Abs(par[0]);
	  double nDivAAlpha = par[1]/absAlpha;
	  double z = (x[0]-par[2])/par[3];
 
	  if(par[0] >= 0) {
	       if( z > -par[0] ) { return TMath::Exp(-0.5*z*z); }
	       else
		    {
			 double A = TMath::Power(nDivAAlpha,par[1])*TMath::Exp(-0.5*absAlpha*absAlpha);
			 double B = nDivAAlpha - absAlpha;
			 
			 return A*TMath::Power(B - z, -par[1]);
		    }
	  }
	  else {	 
	       if( z < -par[0] ) { return TMath::Exp(-0.5*z*z); }
	       else
		    {
			 double A = TMath::Power(nDivAAlpha,par[1])*TMath::Exp(-0.5*absAlpha*absAlpha);
			 double B = nDivAAlpha - absAlpha;
			 
			 return A*TMath::Power(B + z, -par[1]);
		    }	 
	  }
     }

     double BW(double* x, double* par) {
	  return TMath::BreitWigner(x[0], par[0], par[1]);
     }

     double Gaus(double* x, double* par) {
	  return TMath::Gaus(x[0], par[0], par[1]);
     }


     double Landau(double *x, double* par) {
	  return TMath::Landau(x[0], par[0], par[1]);
     }

     Double_t langaufun(Double_t *x, Double_t *par) {
	  //Fit parameters:
	  //par[0]=Width (scale) parameter of Landau density
	  //par[1]=Most Probable (MP, location) parameter of Landau density
	  //par[2]=Total area (integral -inf to inf, normalization constant)
	  //par[3]=Width (sigma) of convoluted Gaussian function
	  //
	  //In the Landau distribution (represented by the CERNLIB approximation), 
	  //the maximum is located at x=-0.22278298 with the location parameter=0.
	  //This shift is corrected within this function, so that the actual
	  //maximum is identical to the MP parameter.
  
	  // Numeric constants
	  Double_t invsq2pi = 0.3989422804014;   // (2 pi)^(-1/2)
	  Double_t mpshift  = -0.22278298;       // Landau maximum location
  
	  // Control constants
	  Double_t np = 100.0;      // number of convolution steps
	  Double_t sc =   5.0;      // convolution extends to +-sc Gaussian sigmas
  
	  // Variables
	  Double_t xx;
	  Double_t mpc;
	  Double_t fland;
	  Double_t sum = 0.0;
	  Double_t xlow,xupp;
	  Double_t step;
	  Double_t i;
  
  
	  // MP shift correction
	  mpc = par[1] - mpshift * par[0]; 
  
	  // Range of convolution integral
	  xlow = x[0] - sc * par[3];
	  xupp = x[0] + sc * par[3];
  
	  step = (xupp-xlow) / np;
  
	  // Convolution integral of Landau and Gaussian by sum
	  for(i=1.0; i<=np/2; i++) {
	       xx = xlow + (i-.5) * step;
	       fland = TMath::Landau(xx,mpc,par[0]) / par[0];
	       sum += fland * TMath::Gaus(x[0],xx,par[3]);
    
	       xx = xupp - (i-.5) * step;
	       fland = TMath::Landau(xx,mpc,par[0]) / par[0];
	       sum += fland * TMath::Gaus(x[0],xx,par[3]);
	  }
  
	  return (par[2] * step * sum * invsq2pi / par[3]);
     }


};

class CF_Handler
{

 public:

     CF_Handler(int CFmode = 0) { changeMode(CFmode); }
     ~CF_Handler() { delete ComplexFunc; }

     void changeMode(int CFmode) {
	  complexObj.setMode(CFmode);
	  if(ComplexFunc != nullptr) delete ComplexFunc;
	  ComplexFunc = new TF1("Complex", complexObj, 0., 20000., complexObj.nParameters);
	  for(auto iParName = 0; iParName < complexObj.parameterNames.size(); iParName++) {
	       ComplexFunc->SetParName(iParName, complexObj.parameterNames.at(iParName).c_str());
	  }
     }

     ComplexFunctions complexObj;
     TF1* ComplexFunc = nullptr;

     void setParsInitialValues(double* vals) {
	  if(ComplexFunc == nullptr) return;

	  switch(complexObj.mode) {
	  case 0: case 3: 
	       // Pars: #alpha", "n", "#mu", "#sigma", "N" 
	       ComplexFunc->SetParameter(0, vals[0]);  
	       ComplexFunc->SetParameter(1, vals[1]);
	       ComplexFunc->SetParameter(2, vals[2]);
	       ComplexFunc->SetParameter(3, vals[3]);
	       ComplexFunc->SetParameter(4, vals[4]);	  
	       break;
	  case 1: case 2:
	       // Pars: "#mu", "#sigma", "N" 
	       ComplexFunc->SetParameter(0, vals[0]);	  
	       ComplexFunc->SetParameter(1, vals[1]);	  
	       ComplexFunc->SetParameter(2, vals[2]);	  
	       break;
	  case 4:
	       // Pars: "#mu", "#sigma", "MPV", "Width", "N"
	       ComplexFunc->SetParameter(0, vals[0]);  
	       ComplexFunc->SetParameter(1, vals[1]);
	       ComplexFunc->SetParameter(2, vals[2]);
	       ComplexFunc->SetParameter(3, vals[3]);
	       ComplexFunc->SetParameter(4, vals[4]);
	       break;
	  }
     }

};

#endif
 
