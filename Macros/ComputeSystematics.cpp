void ComputeSystematics(std::string inputFiles= "", std::string manualLastCut = "", std::string outFileName = "SystematicErrors")
{

  // --- Computing systematics for the number of hits hisstograms
  
  // --- Histogram parameters

  int nBins = 250;
  int maxNHits = 2000;
  
  std::vector<std::string> files = {};

  std::string fileName = "";
  for(int iChar = 0; iChar <= inputFiles.size(); iChar++) {

    if(iChar == inputFiles.size() || inputFiles.at(iChar) == ' ') {
      files.push_back(fileName);
      fileName.clear();
      continue;
    }

    fileName += inputFiles.at(iChar);
    
  }
  
  ifstream inputFile;

  std::map<std::string, TH1F*[4]> cutsMap;
  std::map<std::string, TGraphAsymmErrors*> graphsMap;

  std::string lastCut = manualLastCut;
  
  for(auto fileIt = files.begin(); fileIt != files.end(); fileIt++) {

    inputFile.open(*fileIt);
    if(!inputFile.is_open()) {
      std::cout << "Could not open file: " << *fileIt << std::endl;
      exit(0);
    }
    
    std::string left, center, right, currentCut;
    
    while(inputFile) {

      inputFile >> left >> center >> right;

      if(left == "NEW") {
	if(cutsMap.find(right) != cutsMap.end()) {
	  std::cout << "Already existing cut: " << right << " from file: " << *fileIt << std::endl;
	  exit(0);
	}

	currentCut = right;
	cutsMap[right][0] = new TH1F(("NHits_" + right).c_str(), ";NHits;EnTries", nBins, 0., maxNHits);
	cutsMap[right][1] = new TH1F(("NHits_" + right + "_Up").c_str(), ";NHits;EnTries", nBins, 0., maxNHits);
	cutsMap[right][2] = new TH1F(("NHits_" + right + "_Down").c_str(), ";NHits;EnTries", nBins, 0., maxNHits);

	
	inputFile >> left >> center >> right;
	continue;
      }
      
      cutsMap.at(currentCut)[0]->Fill(std::stof(left));
      cutsMap.at(currentCut)[1]->Fill(std::stof(center));
      cutsMap.at(currentCut)[2]->Fill(std::stof(right));
      
    }

    if(fileIt == --files.end() && (lastCut == "" || cutsMap.find(lastCut) == cutsMap.end())) lastCut = currentCut; 
    
    inputFile.close();
    
  }
  
  TFile* outTFile = new TFile((outFileName + ".root").c_str(), "RECREATE");
  
  ofstream outputFile;
  outputFile.open((outFileName + ".txt").c_str(), std::ofstream::trunc);
  
  std::vector<std::pair<double,double>> fullSystematicErrors = std::vector<std::pair<double,double>>(nBins, std::pair<double,double>(1.,1.));

  for(auto cutIt = cutsMap.begin(); cutIt != cutsMap.end(); cutIt++) {

    outTFile->mkdir(cutIt->first.c_str());
    outTFile->cd(cutIt->first.c_str());
    
    cutIt->second[0]->Write();
    cutIt->second[1]->Write();
    cutIt->second[2]->Write();
    
    std::vector<double> x, y, exu, exd, eyu, eyd;

    int nPoints = 0;
    for(int iBin = 1; iBin <= nBins; iBin++) {

      int binContent = (int)cutIt->second[0]->GetBinContent(iBin);
      int binContentUp = (int)cutIt->second[1]->GetBinContent(iBin);
      int binContentDown = (int)cutIt->second[2]->GetBinContent(iBin);
      
      if(binContent == 0) continue;

      nPoints++;

      x.push_back((iBin-1)*maxNHits/nBins + maxNHits/(2*nBins));
      y.push_back(binContent);
      exu.push_back(maxNHits/(double)(2*nBins));
      exd.push_back(maxNHits/(double)(2*nBins));

      double errorUp, errorDown, systUp, systDown;
      
      if(binContent > 0) {
	errorUp = (binContentUp - binContent)/(double)binContent;
	errorDown = (binContentDown - binContent)/(double)binContent;	
      }
      else {
	errorUp = 1;
	errorDown = -1;
	binContent = 1;
      }
      
      if(errorUp > 0 && errorDown > 0) {
	systUp = TMath::Max(errorUp,errorDown);
	systDown = 0.;
      }

      if(errorUp < 0 && errorDown < 0) {
	systUp = 0.;	
        systDown = TMath::Abs(TMath::Min(errorUp,errorDown));
      }	
      else{
	systUp = TMath::Max(errorUp,errorDown);
	systDown = TMath::Abs(TMath::Min(errorUp,errorDown));
      }
      
      eyu.push_back(systUp*binContent);
      eyd.push_back(systDown*binContent);

      fullSystematicErrors.at(iBin - 1).first *= (1 + systUp);
      fullSystematicErrors.at(iBin - 1).second *= (1 - systDown);
      
    } //End loop over bins

    if(cutIt->first != lastCut) delete cutIt->second[0];
    delete cutIt->second[1];
    delete cutIt->second[2];

    TGraphAsymmErrors* errorsGraph = new TGraphAsymmErrors(nPoints, x.data(), y.data(), exd.data(), exu.data(), eyd.data(), eyu.data());
    errorsGraph->Write();
    delete errorsGraph;
    
  } // End loop over files

  outputFile << "BIN SYSTUP(%) SYSTDOWN(%)" << std::endl;

  int bin = 0;
  std::vector<double> x, y, exu, exd, eyu, eyd;

  int nPoints = 0;
 std:cout << "LastCut: " << lastCut << std::endl;
 
  
  for(auto fullSysIt = fullSystematicErrors.begin(); fullSysIt != fullSystematicErrors.end(); fullSysIt++) {

    bin++;
    int binContent = (int)cutsMap.at(lastCut)[0]->GetBinContent(bin);
    
    if(binContent == 0) continue;
    nPoints++;

    x.push_back((bin-1)*maxNHits/nBins + maxNHits/(2*nBins));
    y.push_back(binContent);
    exu.push_back(maxNHits/(double)(2*nBins));
    exd.push_back(maxNHits/(double)(2*nBins));

    eyu.push_back((fullSysIt->first - 1)*binContent);
    eyd.push_back((1 - fullSysIt->second)*binContent);

    outputFile << bin << " " << fullSysIt->first - 1 << " " << 1 - fullSysIt->second << std::endl;
 
  }

  delete cutsMap.at(lastCut)[0];
  
  outTFile->cd();
  
  TGraphAsymmErrors* errorsGraph = new TGraphAsymmErrors(nPoints, x.data(), y.data(), exd.data(), exu.data(), eyd.data(), eyu.data());
  errorsGraph->Write();
  delete errorsGraph;
  
  outputFile.close();
  outTFile->Close();

  exit(0);
  
}


