
void All_LinearityResolution()
{

     std::string outFileBaseName = "All_LinearityResolution";
     TFile* outputFile = new TFile((outFileBaseName + ".root").c_str(),"RECREATE");

     TFile* inputFile_0Deg = new TFile("/pool/calice/hectorgc/sdhcal-angleanalysis/Results/TB/0Deg/LinearityResolution.root", "READ");
     TGraphErrors* linearity_0Deg = (TGraphErrors*)inputFile_0Deg->Get("Linearity");
     linearity_0Deg->SetMarkerStyle(8);
     TF1* fit = linearity_0Deg->GetFunction("pol1");
     fit->SetLineColor(4);
     fit->SetFillColor(4);
     //fit->SetFillStyle(3001);
     linearity_0Deg->SetMarkerColor(4);
     linearity_0Deg->SetLineColor(4);
     linearity_0Deg->SetMaximum(90);
     linearity_0Deg->SetMinimum(0);
     linearity_0Deg->GetXaxis()->SetLimits(0,90);
     TGraphErrors* deviations_0Deg = (TGraphErrors*)inputFile_0Deg->Get("Deviations");
     deviations_0Deg->SetMarkerStyle(8);
     deviations_0Deg->SetMarkerColor(4);
     deviations_0Deg->SetLineColor(4);
     deviations_0Deg->SetMaximum(0.5);
     deviations_0Deg->SetMinimum(-0.5);
     deviations_0Deg->GetXaxis()->SetLimits(0,90);
     TGraphErrors* resolution_0Deg = (TGraphErrors*)inputFile_0Deg->Get("Resolution");
     resolution_0Deg->SetMarkerStyle(8);
     resolution_0Deg->SetMarkerColor(4);
     resolution_0Deg->SetLineColor(4);
     resolution_0Deg->SetMaximum(0.4);
     resolution_0Deg->SetMinimum(0);    
     resolution_0Deg->GetXaxis()->SetLimits(0,90); 

     TFile* inputFile_20Deg = new TFile("/pool/calice/hectorgc/sdhcal-angleanalysis/Results/TB/20Deg/LinearityResolution.root", "READ");
     TGraphErrors* linearity_20Deg = (TGraphErrors*)inputFile_20Deg->Get("Linearity");
     linearity_20Deg->SetMarkerStyle(21);
     linearity_20Deg->SetMarkerColor(2);
     linearity_20Deg->SetLineColor(2);
     TGraphErrors* deviations_20Deg = (TGraphErrors*)inputFile_20Deg->Get("Deviations");
     deviations_20Deg->SetMarkerStyle(21);
     deviations_20Deg->SetMarkerColor(2);
     deviations_20Deg->SetLineColor(2);
     TGraphErrors* resolution_20Deg = (TGraphErrors*)inputFile_20Deg->Get("Resolution");
     resolution_20Deg->SetMarkerStyle(21);
     resolution_20Deg->SetMarkerColor(2);
     resolution_20Deg->SetLineColor(2);
     
     TFile* inputFile_26Deg = new TFile("/pool/calice/hectorgc/sdhcal-angleanalysis/Results/TB/22.46Deg/LinearityResolution.root", "READ");
     TGraphErrors* linearity_26Deg = (TGraphErrors*)inputFile_26Deg->Get("Linearity");
     linearity_26Deg->SetMarkerStyle(3);
     linearity_26Deg->GetFunction("pol1")->SetLineColor(1);
     linearity_26Deg->SetMarkerColor(1);
     linearity_26Deg->SetLineColor(1);
     TGraphErrors* deviations_26Deg = (TGraphErrors*)inputFile_26Deg->Get("Deviations");
     deviations_26Deg->SetMarkerStyle(3);
     deviations_26Deg->SetMarkerColor(1);
     deviations_26Deg->SetLineColor(1);
     TGraphErrors* resolution_26Deg = (TGraphErrors*)inputFile_26Deg->Get("Resolution");
     resolution_26Deg->SetMarkerStyle(3);
     resolution_26Deg->SetMarkerColor(1);
     resolution_26Deg->SetLineColor(1);
     
     TLegend* linearityLegend = new TLegend(0.11,0.8,0.2,0.89);
     linearityLegend->AddEntry(linearity_0Deg, "0Deg");
     linearityLegend->AddEntry(linearity_20Deg, "20Deg");
     linearityLegend->AddEntry(linearity_26Deg, "26Deg");

     TLegend* deviationsLegend = new TLegend(0.8,0.8,0.89,0.89);
     deviationsLegend->AddEntry(deviations_0Deg, "0Deg");
     deviationsLegend->AddEntry(deviations_20Deg, "20Deg");
     deviationsLegend->AddEntry(deviations_26Deg, "26Deg");

     TLegend* resolutionLegend = new TLegend(0.8,0.8,0.89,0.89);
     resolutionLegend->AddEntry(resolution_0Deg, "0Deg");
     resolutionLegend->AddEntry(resolution_20Deg, "20Deg");
     resolutionLegend->AddEntry(resolution_26Deg, "26Deg");

     outputFile->cd();

     TCanvas* linearity = new TCanvas("Linearity");
     linearity_0Deg->Draw("APE");
     linearity_20Deg->Draw("SamePE");
     linearity_26Deg->Draw("SamePE");
     linearityLegend->Draw("Same");
     fit->Draw("sameE3");

     linearity->Write();
     delete linearity;

     TLine* hLine = new TLine(0,0,90,0);
     hLine->SetLineColor(12);
     hLine->SetLineWidth(1);
     hLine->SetLineStyle(10);

     TCanvas* deviations = new TCanvas("Deviations");
     deviations_0Deg->Draw("APE");
     deviations_20Deg->Draw("SamePE");
     deviations_26Deg->Draw("SamePE");
     hLine->Draw("same");
     deviationsLegend->Draw("same");

     deviations->Write();
     delete deviations;

     TCanvas* resolution = new TCanvas("Resolution");
     resolution_0Deg->Draw("APE");
     resolution_20Deg->Draw("SamePE");
     resolution_26Deg->Draw("SamePE");
     resolutionLegend->Draw("same");

     resolution->Write();
     delete resolution;

}


